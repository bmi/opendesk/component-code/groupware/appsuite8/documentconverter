/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.apache.commons.io.FileUtils;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.impl.api.ICacheHashFileMapper;

/**
 * {@link CacheEntryReference}
 *
 * A lightweight representation of a cache entry object, having  only the
 * parent cache as well as the hash itself as members. *
 * Method access is unsynchronized and needs external synchronization.
 *
 * @author  <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.5
 */
public class CacheEntryReference implements Comparable<CacheEntryReference> {

    final private static Charset ASCII_CHARSET = Charset.forName("US-ASCII");

    /**
     * Initializes a new {@link CacheEntryReference}.
     * @param cache
     * @param hash
     */
    public CacheEntryReference(@NonNull final String asciiHash) {
        super();

        m_asciiHashBytes = asciiHash.getBytes(ASCII_CHARSET);
    }

    /**
     * Initializes a new {@link CacheEntryReference}.
     * @param cache
     * @param hash
     */
    public CacheEntryReference(@NonNull final ICacheHashFileMapper hashFileMapper, @NonNull final String asciiHash, final long lastAccessTime) {
        this(asciiHash);

        final File persistentDir = getPersistentDir(hashFileMapper);

        m_lastAccessTimeMillis = lastAccessTime;
        m_persistentSize = (int) Cache.calculateDirectorySize(persistentDir);
    }

    // - Overrides -------------------------------------------------------------

    /**
     *
     */
    @Override
    public int hashCode() {
        return Arrays.hashCode(m_asciiHashBytes);
    }

    /**
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof CacheEntryReference)) {
            return false;
        }
        CacheEntryReference other = (CacheEntryReference) obj;
        return Arrays.equals(m_asciiHashBytes, other.m_asciiHashBytes);
    }

    /**
     *
     */
    @Override
    public int compareTo(CacheEntryReference compareCacheHashEntry) {
        // only the timestamps are compared in natural sort order (olderDate < newerData),
        return Long.compare(m_lastAccessTimeMillis, compareCacheHashEntry.m_lastAccessTimeMillis);
    }

    // - Public API ------------------------------------------------------------

    /**
     * @return
     */
    public String getHash() {
        return new String(m_asciiHashBytes, ASCII_CHARSET);
    }

    /**
     * @param rootDir
     * @return
     */
    public boolean isValid(@NonNull final ICacheHashFileMapper hashFileMapper) {
        return CacheEntry.isValid(hashFileMapper, getHash());
    }

    /**
     * @param curTimeMillis
     * @return
     */
    public boolean isValidTimestamp(final long curTimeMillis, final long timeoutMillis) {
        return ((curTimeMillis - m_lastAccessTimeMillis) < timeoutMillis);
    }

    /**
     * @return  The full path to this objects cache entry directory
     */
    public File getPersistentDir(@NonNull final ICacheHashFileMapper hashFileMapper) {
        return hashFileMapper.mapHashToFile(getHash());
    }

    /**
     * @return
     */
    public long getLastAccessTimeMillis() {
        return m_lastAccessTimeMillis;
    }

    /**
     * @param rootDir
     * @return
     */
    public long getPersistentSize() {
        return m_persistentSize;
    }

    /**
     * @param rootDir
     */
    public void clear(@NonNull final ICacheHashFileMapper hashFileMapper) {
        FileUtils.deleteQuietly(getPersistentDir(hashFileMapper));
    }

    /**
     * @param rootDir
     */
    public void touch(@NonNull final ICacheHashFileMapper hashFileMapper) {
        getPersistentDir(hashFileMapper).setLastModified(m_lastAccessTimeMillis = System.currentTimeMillis());
    }

    // - Members ---------------------------------------------------------------

    final private byte[] m_asciiHashBytes;

    private long m_lastAccessTimeMillis = 0;

    private int m_persistentSize = 0;
}
