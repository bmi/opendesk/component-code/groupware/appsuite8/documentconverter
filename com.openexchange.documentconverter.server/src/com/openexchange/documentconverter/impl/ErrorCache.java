/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.util.HashMap;
import java.util.Map;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.JobErrorEx.StatusErrorCache;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.Watchdog.WatchdogMode;
import com.openexchange.documentconverter.impl.api.JobEntry;

/**
 * {@link ErrorCache}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.6.0
 */
public class ErrorCache {

    /**
     * Initializes a new {@link ErrorCache}.
     */
    public ErrorCache(long errorCacheTimeoutMilliseconds, int errorCacheMaxCycleCount) {
        super();

        final boolean errorCacheEnabled = (errorCacheTimeoutMilliseconds > 0) && (errorCacheMaxCycleCount > 0);

        if (errorCacheEnabled) {
            m_errorHashMap = new HashMap<>();
            m_errorCacheMaxCycleCount = errorCacheMaxCycleCount;

            m_errorCacheWatchdog = new Watchdog("DC ErrorCache watchdog", errorCacheTimeoutMilliseconds, WatchdogMode.TIMEOUT);
            m_errorCacheWatchdog.start();

            if (ServerManager.isLogInfo()) {
                ServerManager.logInfo("DC ErrorCache enabled", new LogData(
                    "timeout",
                    (errorCacheTimeoutMilliseconds / 1000L) + "s"), new LogData("cycles", Integer.toString(errorCacheMaxCycleCount)));
            }
        } else if (ServerManager.isLogInfo()) {
            JobErrorEx.setErrorCacheDisabled();
            ServerManager.logInfo("DC ErrorCache disabled");
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return true, if this cache is configured to collect job errors based on their hash value
     */
    public boolean isEnabled() {
        return (null != m_errorCacheWatchdog);
    }

    /**
     *
     */
    public void terminate() {
        if (isEnabled()) {
            m_errorCacheWatchdog.terminate();

            try {
                m_errorCacheWatchdog.join();
            } catch (@SuppressWarnings("unused") InterruptedException e) {
                // ok
            }
        }
    }

    /**
     * @param hash
     * @param jobError
     * @return The number of errorcache activations for this hash
     */
    public int notifyJobFinish(@NonNull final JobEntry jobEntry) {
        final String hash = jobEntry.getHash();
        final JobErrorEx jobErrorEx = jobEntry.getJobErrorEx();
        int activationCount = 0;

        if (isEnabled() && Cache.isValidHash(hash) && (null != jobErrorEx)) {
            final String fileName = (String) jobEntry.getJobProperties().get(Properties.PROP_INFO_FILENAME);

            synchronized (m_errorHashMap) {
                ErrorCacheEntry errorCacheEntry = m_errorHashMap.get(hash);

                if (null != errorCacheEntry) {
                    if (jobErrorEx.hasNoError()) {
                        m_errorCacheWatchdog.removeWatchdogHandler(errorCacheEntry);
                        m_errorHashMap.remove(hash);

                        errorCacheEntry = null;

                        if (ServerManager.isLogTrace()) {
                            ServerManager.logTrace("DC ErrorCache removed entry after successful conversion",
                                new LogData("hash", hash),
                                new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));
                        }
                    } else if (errorCacheEntry.isActive()) {
                        errorCacheEntry = null;
                    } else {
                        jobErrorEx.setStatusErrorCache(StatusErrorCache.NEW);
                    }
                } else if (jobErrorEx.hasError()) {
                    jobErrorEx.setStatusErrorCache(StatusErrorCache.NEW);
                    m_errorHashMap.put(hash, errorCacheEntry = new ErrorCacheEntry(jobEntry));
                }

                if (null != errorCacheEntry) {
                    activationCount = errorCacheEntry.activate();

                    m_errorCacheWatchdog.addWatchdogHandler(errorCacheEntry);

                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC ErrorCache activated entry",
                            new LogData("hash", hash),
                            new LogData("joberror", jobErrorEx.getJobError().getErrorText()),
                            new LogData("activations", Integer.toString(errorCacheEntry.getActivationCount())),
                            new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));
                    }
                }
            }
        }

        return activationCount;
    }

    /**
     * @param hash
     * @return The error for the job with the given hash or <code>null</code>, if the corresponding job is not flagged as an error job at
     *         all
     */
    public JobErrorEx getJobErrorExForHash(String hash) {
        final boolean isEnabled = isEnabled();

        if (isEnabled && Cache.isValidHash(hash)) {
            synchronized (m_errorHashMap) {
                final ErrorCacheEntry foundEntry = m_errorHashMap.get(hash);

                if ((null != foundEntry) && foundEntry.isActive()) {
                    final JobErrorEx ret = new JobErrorEx(JobError.fromErrorCode(foundEntry.getJobErrorCode()));

                    ret.setStatusErrorCache((foundEntry.getActivationCount() < getMaxCycleCount()) ? StatusErrorCache.ACTIVE : StatusErrorCache.LOCKED);

                    return ret;
                }
            }
        }

        return null;
    }

    /**
     * @return
     */
    protected int getMaxCycleCount() {
        return m_errorCacheMaxCycleCount;
    }

    // - Members ----------------------------------------------------------------

    protected Map<String, ErrorCacheEntry> m_errorHashMap = null;

    protected Watchdog m_errorCacheWatchdog = null;

    protected int m_errorCacheMaxCycleCount = 0;

    // - Inner class ErrorCacheEntry --------------------------------------------

    /**
     * {@link ErrorCacheEntry}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.6.0
     */
    protected class ErrorCacheEntry implements WatchdogHandler {

        /**
         * Initializes a new {@link ErrorCacheEntry}.
         *
         * @param hash
         */
        protected ErrorCacheEntry(@NonNull final JobEntry jobEntry) {
            super();

            final String fileName = (String) jobEntry.getJobProperties().get(Properties.PROP_INFO_FILENAME);

            m_hash = jobEntry.getHash();
            m_jobErrorCode = jobEntry.getJobErrorEx().getErrorCode();
            m_fileName = (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN;
        }

        /* (non-Javadoc)
         * @see com.openexchange.documentconverter.server.impl.WatchdogHandler#notify(long, com.openexchange.documentconverter.JobError)
         */
        @Override
        public void handleWatchdogNotification(final long curTimeMillis, final long startTimeMillis, final JobErrorEx notificationJobErrorEx) {
            synchronized (m_errorHashMap) {
                if (m_activationCount < getMaxCycleCount()) {
                    m_isActive = false;

                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace(
                            "DC ErrorCache deactivated entry after timeout",
                            new LogData("hash", m_hash),
                            new LogData("joberror", JobError.fromErrorCode(m_jobErrorCode).toString()),
                            new LogData("activations", Integer.toString(m_activationCount)),
                            new LogData("filename", m_fileName));
                    }
                } else {
                    ServerManager.logInfo(
                        "DC ErrorCache permanently added entry after " + Integer.toString(m_activationCount) + " activations",
                        new LogData("hash", m_hash),
                        new LogData("joberror", JobError.fromErrorCode(m_jobErrorCode).toString()),
                        new LogData("filename", m_fileName));
                }
            }
        }

        // - Implementation ----------------------------------------------------

        /**
         * @return
         */
        public String getHash() {
            return m_hash;
        }

        /**
         * @return
         */
        public int getJobErrorCode() {
            return m_jobErrorCode;
        }

        /**
         * @param set
         * @return
         */
        public int activate() {
            m_isActive = true;

            return (++m_activationCount);
        }

        /**
         * @return
         */
        public boolean isActive() {
            return m_isActive;
        }

        /**
         * @return
         */
        public int getActivationCount() {
            return m_activationCount;
        }

        // - Members -----------------------------------------------------------

        final private String m_hash;

        final private int m_jobErrorCode;

        final private String m_fileName;

        private boolean m_isActive = false;

        private int m_activationCount = 0;
    }
}

