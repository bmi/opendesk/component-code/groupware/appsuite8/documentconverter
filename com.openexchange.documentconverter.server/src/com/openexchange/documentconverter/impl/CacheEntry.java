/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import com.google.common.base.Throwables;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.api.ICacheEntry;
import com.openexchange.documentconverter.impl.api.ICacheHashFileMapper;
import com.openexchange.documentconverter.impl.api.IdLocker;

/**
 * {@link CacheEntry}
 *
 * The CacheEntry class has no internal synchronization and does not need to have one
 * as one entry is created on demanded and not shared between different threads.
 * Instead, each access should be synchronized prior to usage via the given hash,
 * an instance is created with.
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class CacheEntry implements ICacheEntry {

    // - Statics ---------------------------------------------------------------

    /**
     * The serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    // - Public API ------------------------------------------------------------

    /**
     * @param cacheEntryDir
     * @param hash
     * @return
     */
    public static @Nullable CacheEntry createFrom(
        @NonNull final ICacheHashFileMapper hashFileMapper,
        @NonNull final String hash) {

        if (Cache.isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                // read result properties first
                final File cacheEntryDir = getCacheEntryDir(hashFileMapper, hash);
                final File resultPropertiesFile = new File(cacheEntryDir, Properties.CACHE_PROPERTIES_FILENAME);
                final File resultContentFile = new File(cacheEntryDir, Properties.CACHE_RESULT_FILENAME);

                if (resultPropertiesFile.canRead() && resultContentFile.canRead()) {
                    // try to read properties first and remove possibly
                    // contained result buffer in every case since
                    // content is/should be available via content file
                    try (ObjectInputStream objInputStm = new ObjectInputStream(new BufferedInputStream(new FileInputStream(resultPropertiesFile)))) {
                        final Object readObject = objInputStm.readObject();

                        if (readObject instanceof Map<?, ?>) {
                            return new CacheEntry(hashFileMapper, hash, (Map<String, Object>) readObject, null);
                        }
                    }
                }
            } catch (final Exception e) {
                ServerManager.logExcp(e);
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return null;
    }

    /**
     * @param cacheEntryDir
     * @param hash
     * @param properties
     * @param content
     * @return
     */
    public static @Nullable CacheEntry createAndMakePersistent(
        @NonNull final ICacheHashFileMapper hashFileMapper,
        @NonNull final String hash,
        @NonNull final Map<String, Object> properties,
        @NonNull final byte[] content) {

        if (Cache.isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                final File cacheEntryDir = getCacheEntryDir(hashFileMapper, hash);

                if (ServerManager.validateOrMkdir(cacheEntryDir)) {
                    final CacheEntry cacheEntry = new CacheEntry(hashFileMapper, hash, properties, content);

                    if (cacheEntry.implMakePersistent()) {
                        return cacheEntry;
                    }
                }

                // cacheEntryDir is removed when previous calls
                // did not return a new CacheEntry up to now
                FileUtils.deleteQuietly(cacheEntryDir);
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return null;
    }

    /**
     * @param rootDir
     * @param hash
     * @return
     */
    public static boolean isValid(@NonNull final ICacheHashFileMapper hashFileMapper, @NonNull final String hash) {
        if (Cache.isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                // verify if result properties are reabable and a result buffer can be read
                final File cacheEntryDir = getCacheEntryDir(hashFileMapper, hash);
                final File resultPropertiesFile = new File(cacheEntryDir, Properties.CACHE_PROPERTIES_FILENAME);
                final File resultContentFile = new File(cacheEntryDir, Properties.CACHE_RESULT_FILENAME);

                return resultPropertiesFile.canRead() && (resultPropertiesFile.length() > 0) && resultContentFile.canRead();
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return false;
    }

    /**
     * @param rootDir
     * @param hash
     * @return
     */
    public static File getCacheEntryDir(@NonNull final ICacheHashFileMapper hashFileMapper, @NonNull final String hash) {
        return hashFileMapper.mapHashToFile(hash);
    }

    // - Implementation --------------------------------------------------------

    private CacheEntry() {
        super();

        implInitTransients();
    }

    /**
     * Unused
     *
     * Initializes a new {@link CacheEntry}.
     */
    /**
     * Initializes a new {@link CacheEntry}.
     * @param cacheEntryDir
     * @param hash
     * @param properties
     * @param content
     */
    private CacheEntry(
        @NonNull final ICacheHashFileMapper hashFileMapper,
        @NonNull final String hash,
        @NonNull final Map<String, Object> properties,
        @Nullable final byte[] content) {

        this();

        m_entryDir = getCacheEntryDir(hashFileMapper, hash);
        m_hash = hash;

        m_resultProperties.clear();
        m_resultProperties.putAll(properties);

        m_resultContent = content;
        m_accessTime = new Date(m_entryDir.lastModified());
        m_persistentSize = Cache.calculateDirectorySize(m_entryDir);
    }

    // - Overrides -------------------------------------------------------------

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((m_entryDir == null) ? 0 : m_entryDir.hashCode());
        result = prime * result + ((m_hash == null) ? 0 : m_hash.hashCode());
        return result;
    }
    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CacheEntry other = (CacheEntry) obj;
        if (m_entryDir == null) {
            if (other.m_entryDir != null) {
                return false;
            }
        } else if (!m_entryDir.equals(other.m_entryDir)) {
            return false;
        }
        if (m_hash == null) {
            if (other.m_hash != null) {
                return false;
            }
        } else if (!m_hash.equals(other.m_hash)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.ICacheEntry#isValid()
     */
    @Override
    public boolean isValid() {
        return Cache.isValidHash(m_hash);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.ICacheEntry#clear()
     */
    @Override
    public void clear() {
        m_hash = null;
        m_persistentSize = 0;
        m_resultProperties.clear();

        FileUtils.deleteQuietly(m_entryDir);
        m_entryDir = null;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.ICacheEntry#getHash()
     */
    @Override
    public String getHash() {
        return m_hash;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.ICacheEntry#getResultProperties()
     */
    @Override
    public HashMap<String, Object> getResultProperties() {
        return m_resultProperties;
    }

    /**
     *
     */
    @Override
    public byte[] getResultContent() {
        try {
            return isValid() ?
                (null != m_resultContent) ? m_resultContent :
                    FileUtils.readFileToByteArray(getResultFile()) :
                        null;
        } catch (IOException e) {
            if (ServerManager.isLogWarn()) {
                ServerManager.logWarn(new StringBuilder(1024).
                    append("DC cache entry result content could not be read: ").
                    append(getHash()).
                    append('(').
                    append(Throwables.getRootCause(e)).
                    append(')').toString());
            }
        }

        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.ICacheEntry#getResultFile()
     */
    @Override
    public File getResultFile() {
        return new File(getPersistentDirectory(), Properties.CACHE_RESULT_FILENAME);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.ICacheEntry#getPersistentEntryDirectory()
     */
    @Override
    public File getPersistentDirectory() {
        return m_entryDir;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.ICacheEntry#getPersistentSize()
     */
    @Override
    public long getPersistentSize() {
        return  m_persistentSize;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconvertemakePersistentr.ICacheEntry#getAccessTime()
     */
    @Override
    public Date getAccessTime() {
        return m_accessTime;
    }

    @Override
    public boolean updateAndMakePersistent(final HashMap<String, Object> resultProperties) {
        if ((null != resultProperties) &&
            (null != m_entryDir) &&
            ServerManager.validateOrMkdir(m_entryDir)) {

            m_resultProperties.clear();
            m_resultProperties.putAll(resultProperties);

            return implMakePersistent();
        }

        return false;
    }

    // - Public interface ------------------------------------------------------

    /**
     * @param cacheEntry
     * @param job
     * @return true If result properties could be retrieved from cacheEntry and set at the current serverJob
     */
    public Map<String, Object> getJobResult() {
        Map<String, Object> ret = null;

        if (isValid()) {
            final File resultFile = getResultFile();

            if (resultFile.canRead() && (resultFile.length() > 0)) {
                (ret = new HashMap<>()).putAll(getResultProperties());

                try {
                    ret.put(Properties.PROP_RESULT_BUFFER, FileUtils.readFileToByteArray(resultFile));
                } catch (IOException e) {
                    ServerManager.logExcp(e);
                    ret = null;
                }
            }
        }

        return ret;
    }

    // - Implementation --------------------------------------------------------

    /**
     * Initialize the transient members
     */
    private void implInitTransients() {
        m_entryDir = null;
    }

    /**
     * @return
     */
    private boolean implMakePersistent() {
        if (isValid() && (null != m_entryDir)) {
            // prepare the property set for persistence
            m_resultProperties.remove(Properties.PROP_RESULT_BUFFER);

            // take name of the result directory as the entries persistent name
            m_resultProperties.put(Properties.PROP_CACHE_PERSIST_ENTRY_NAME, m_entryDir.getName());

            // write adjusted properties map
            try (ObjectOutputStream objStm = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File(m_entryDir, Properties.CACHE_PROPERTIES_FILENAME))))) {
                objStm.writeObject(m_resultProperties);
                objStm.flush();

                // write content, if available at runtime
                // and set to null afterwards since it can
                // be restored from persistent data
                if (null != m_resultContent) {
                    FileUtils.writeByteArrayToFile(new File(m_entryDir, Properties.CACHE_RESULT_FILENAME), m_resultContent);
                    m_resultContent = null;
                }

                // update persistent members
                m_accessTime = new Date(m_entryDir.lastModified());
                m_persistentSize = Cache.calculateDirectorySize(m_entryDir);

                return true;
            } catch (final Exception e) {
                ServerManager.logExcp(e);
            }
        }

        return false;
    }

    /**
     * @param objInputStm
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void readObject(ObjectInputStream objInputStm) throws IOException, ClassNotFoundException {
        if (null != objInputStm) {
            try {
                objInputStm.defaultReadObject();
            } finally {
                implInitTransients();
            }
        }
    }

    /**
     * @param objOutputStm
     * @throws IOException
     */
    private void writeObject(ObjectOutputStream objOutputStm) throws IOException {
        if (null != objOutputStm) {
            final boolean contentInMemory = (null != m_resultContent);

            try {
                // load buffer content into memory to be transfered
                if (!contentInMemory) {
                    m_resultContent = getResultContent();
                }

                objOutputStm.defaultWriteObject();
                objOutputStm.flush();
            } finally {
                // restore old content in memory status
                if (!contentInMemory) {
                    m_resultContent = null;
                }
            }
        }
    }


    // - Members ----------------------------------------------------------

    // !!!  DO NOT CHANGE TYPE, ORDER OR NAME OF THIS VARIABLE FOR STREAMING COMPATIBILITY !!!
    private String m_hash = null;

    // !!!  DO NOT CHANGE TYPE, ORDER OR NAME OF THIS VARIABLE FOR STREAMING COMPATIBILITY !!!
    private HashMap<String, Object> m_resultProperties = new HashMap<>(8);

    // !!!  DO NOT CHANGE TYPE, ORDER OR NAME OF THIS VARIABLE FOR STREAMING COMPATIBILITY !!!
    private byte[] m_resultContent = null;

    // !!!  DO NOT CHANGE TYPE, ORDER OR NAME OF THIS VARIABLE FOR STREAMING COMPATIBILITY !!!
    private Date m_accessTime = new Date();

    // !!!  DO NOT CHANGE TYPE, ORDER OR NAME OF THIS VARIABLE FOR STREAMING COMPATIBILITY !!!
    private long m_persistentSize = 0;

    private transient File m_entryDir = null;
}
