/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.JobEntry;
import com.sun.star.beans.XPropertySet;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;

//------------------------
//- class InstanceThread -
//------------------------

/**
 * {@link REThread}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
/**
 * @author ka
 *
 */
class REThread extends JobThread {

    /**
     * Initializes a new {@link REThread}.
     *
     * @param jobProcessor
     * @param backendDescriptor
     * @param cache
     * @param watchdog
     */
    REThread(
        @NonNull final ServerManager serverManager,
        @NonNull JobProcessor jobProcessor,
        @NonNull REInstanceProvider instanceProvider,
        @NonNull Cache cache,
        @NonNull ErrorCache errorCache,
        @NonNull Watchdog watchdog) {

        super(serverManager, jobProcessor, BackendType.READERENGINE, cache, errorCache, watchdog);

        setName(getLogBuilder().append("#").append(m_reThreadNumber).toString());

        m_instanceProvider = instanceProvider;

        synchronized (RE_ERROR_RUN_COUNT_MAP) {
            RE_ERROR_RUN_COUNT_MAP.put(JobError.TIMEOUT,
                Integer.valueOf(m_serverManager.getServerConfig().JOB_RESTART_AFTER_TIMEOUT ? 2 : 1));
        }
    }

    // - JobThread -------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.server.impl.JobThread#getSupportedBackendRuns()
     */
    @Override
    Map<JobError, Integer> getSupportedBackendRuns() {
        return RE_ERROR_RUN_COUNT_MAP;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.server.impl.JobThread#isDisposed()
     */
    @Override
    public boolean isDisposed() {
        final REInstance currentInstance = implGetCurrentInstance();

        return (null == currentInstance) || currentInstance.isDisposed();
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#createCopy()
     */
    @Override
    protected JobThread createCopy() {
        return new REThread(m_serverManager, m_jobProcessor, m_instanceProvider, m_cache, m_errorCache, m_watchdog);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#shutdownCurrentInstance()
     */
    @Override
    protected synchronized void shutdownCurrentInstance() {
        if (null != m_instance) {
            m_instance.kill();
            m_instance = null;
        }

        super.shutdownCurrentInstance();
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#prepareProcessingJob()
     */
    @Override
    protected boolean prepareProcessingJob() {
        final REInstance currentInstance = implGetOrCreateInstance();
        boolean ret = (null != currentInstance);

        if (ret) {
            // reset the set instance local to null (=> use default locale)
            m_curLocale = null;

            // set readerEngine instance as execution data,
            // if we have a valid readerEngine instance
            setJobExecutionData(currentInstance);
        }

        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#handlePrepareProcessingJobFailed()
     */
    @Override
    protected void handlePrepareProcessingJobFailed() {
        if (isRunning()) {
            if (ServerManager.isLogError()) {
                ServerManager.logError(getLogBuilder().
                    append("Engine#").append(m_reThreadNumber).append(" could not be launched => trying again in ").
                    append(REInstance.CONNECT_TIMEOUT_MS / 1000).append('s').toString());
            }

            ServerManager.sleepThread(REInstance.CONNECT_TIMEOUT_MS, true);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#setLocale(java.lang.String)
     */
    @Override
    protected String setLocale(String jobLocale) {
        final String curLocale = super.setLocale(jobLocale);

        if ((null == m_curLocale) || !m_curLocale.equalsIgnoreCase(curLocale)) {
            final REInstance currentInstance = implGetCurrentInstance();

            if (null != currentInstance) {
                final XMultiComponentFactory serviceFactory = currentInstance.getComponentFactory();
                final XComponentContext serviceContext = currentInstance.getDefaultContext();

                if ((null != serviceFactory) && (null != serviceContext)) {
                    try {
                        final XPropertySet xLocaleService = UnoRuntime.queryInterface(
                            XPropertySet.class,
                            serviceFactory.createInstanceWithContext(
                                "com.sun.star.config.OxLocaleService", serviceContext));

                        if (null != xLocaleService) {
                            xLocaleService.setPropertyValue("OxLocale", curLocale.replace('_', '-'));
                            m_curLocale = curLocale;
                        }
                    } catch (final Exception e) {
                        ServerManager.logExcp(e);
                    }
                }
            }

            if (null == m_curLocale) {
                m_curLocale = "en_US";
            }
        }

        return m_curLocale;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#endProcessingJob(boolean)
     */
    @Override
    protected synchronized void endProcessingJob(JobEntry curJobEntry) {
        if (null != curJobEntry) {
            final REInstance currentInstance = implGetCurrentInstance();

            if (null != currentInstance) {

                // always restart instance in error case or
                // if max. number of conversions are finished
                if (curJobEntry.getJobErrorEx().hasError() ||
                    (currentInstance.getRestartCount() < 2) ||
                    (0 == (getInstanceJobsProcessed() % currentInstance.getRestartCount()))) {

                    shutdownCurrentInstance();
                }
            }
        }

        setJobExecutionData(null);

        super.endProcessingJob(curJobEntry);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.server.impl.JobThread#getRestartCount()
     */
    @Override
    protected int getRestartCount() {
        final REInstance currentInstance = implGetCurrentInstance();

        return (null != currentInstance) ?
            currentInstance.getRestartCount() :
                super.getRestartCount();

    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    @Nullable private synchronized REInstance implGetOrCreateInstance() {
        return isRunning() ?
            (null != m_instance) ? m_instance : (m_instance = m_instanceProvider.getREInstance()) :
                null;
    }

    /**
     * @return
     */
    @Nullable private synchronized REInstance implGetCurrentInstance() {
        return m_instance;
    }

    // - Static members ---------------------------------------------------------

    final private static AtomicInteger RE_THREAD_NUMBER = new AtomicInteger(0);

    final private static Map<JobError, Integer> RE_ERROR_RUN_COUNT_MAP = new HashMap<>();

    static {
        // initialize with default runs of 1
        RE_ERROR_RUN_COUNT_MAP.putAll(ServerManager.getDefaultSupportedBackendRuns());

        // set run count to 2 for JobError.GENERAL, JobError.DISPOSED
        RE_ERROR_RUN_COUNT_MAP.put(JobError.GENERAL, Integer.valueOf(2));
        RE_ERROR_RUN_COUNT_MAP.put(JobError.DISPOSED, Integer.valueOf(2));
    }

    // - Members ---------------------------------------------------------------

    private final int m_reThreadNumber = RE_THREAD_NUMBER.incrementAndGet();

    private final REInstanceProvider m_instanceProvider;

    private REInstance m_instance = null;

    private String m_curLocale = null;
}
