/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.ConverterStatus;
import com.openexchange.exception.ExceptionUtils;
import com.sun.star.beans.PropertyState;
import com.sun.star.beans.PropertyValue;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;

//-------------------------
//- class PDFConverterJob -
//-------------------------

/**
 * {@link DocumentConverterJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
class DocumentConverterJob extends ConverterJob {

    /**
     * Initializes a new {@link DocumentConverterJob}.
     *
     * @param inputStream
     * @param outputStream
     */
    DocumentConverterJob(HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties, String jobType) {
        super(jobProperties, resultProperties);
        this.jobType = jobType;
        applyJobProperties();
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#getHash()
     */
    @Override
    public String getHash() {
        if (null == m_cacheHash) {
            final StringBuilder hashBuilder = super.getInputFileHashBuilder();

            if (null != hashBuilder) {
                if (m_resultZipArchive) {
                    hashBuilder.append('z');
                }
                hashBuilder.append("pdfa".equals(jobType) ? "pdfa" : m_filterShortName);

                if (null != m_pageRange) {
                    hashBuilder.append('#').append(m_pageRange);
                }

                setHash(hashBuilder);
            }
        }
        return m_cacheHash;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.IJob#needsInstance()
     */
    @Override
    public BackendType backendTypeNeeded() {
        return BackendType.READERENGINE;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#doExecute(com.openexchange.documentconverter.IInstance)
     */
    @Override
    protected ConverterStatus doExecute(Object jobExecutionData) throws com.sun.star.lang.DisposedException, InterruptedException, IOException, MalformedURLException, Exception {
        final REInstance instance = (jobExecutionData instanceof REInstance) ? (REInstance) jobExecutionData : null;
        final XComponent component = super.loadComponent(instance, m_pageRange);
        final XStorable storable = (null != component) ? UnoRuntime.queryInterface(XStorable.class, component) : null;
        String appType = "writer";
        ConverterStatus converterStatus = ConverterStatus.ERROR;
        final boolean trace = ServerManager.isLogTrace();
        long traceStartTimeMillis = 0;

        if (null != component) {
            final XServiceInfo info = UnoRuntime.queryInterface(XServiceInfo.class, component);

            if (null != info) {
                if (info.supportsService("com.sun.star.text.TextDocument")) {
                    appType = "writer";
                } else if (info.supportsService("com.sun.star.sheet.SpreadsheetDocument")) {
                    appType = "calc";
                } else if (info.supportsService("com.sun.star.presentation.PresentationDocument")) {
                    appType = "impress";
                } else if (info.supportsService("com.sun.star.drawing.DrawingDocument")) {
                    appType = "draw";
                }
            }
        }

        if (null != storable) {
            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace("DC ReaderEngine started storing document");
            }

            // set filter name, mimetype and extension according to
            // filter shortname and detected input document type
            setFilterParams(component, appType);

            // always store document to the m_outputFile, since LIBO
            // doesn't handle storeToURL very well regarding non-seekable output
            // streams with all kinds of export filters
            final boolean isPDFExport = "pdf".equals(m_resultExtension);
            final String outputFileUrlString = ServerManager.getAdjustedFileURL(m_outputFile);

            // preparing FilterData
            final List<PropertyValue> filterData = new ArrayList<>(6);

            filterData.add(new PropertyValue("PixelWidth", -1, Integer.valueOf(m_pixelWidth), PropertyState.DIRECT_VALUE));
            filterData.add(new PropertyValue("PixelHeight", -1, Integer.valueOf(m_pixelHeight), PropertyState.DIRECT_VALUE));
            filterData.add(new PropertyValue("UseNativeTextDecoration", -1, Boolean.FALSE, PropertyState.DIRECT_VALUE));

            if (m_resultZipArchive) {
                filterData.add(new PropertyValue("ContainerType", -1, "ZipArchive", PropertyState.DIRECT_VALUE));
            }

            if (isPDFExport) {
                filterData.add(new PropertyValue("Quality", -1, Integer.valueOf(ServerManager.get().getServerConfig().PDF_JPEG_QUALITY_PERCENTAGE), PropertyState.DIRECT_VALUE));

                if("pdfa".equals(jobType)) {
                    filterData.add(new PropertyValue("SelectPdfVersion", -1, Integer.valueOf(1), PropertyState.DIRECT_VALUE));
                }
            }

            // initialize media descriptor
            final List<PropertyValue> mediaDesc = new ArrayList<>(4);

            mediaDesc.add(new PropertyValue("URL", -1, outputFileUrlString, PropertyState.DIRECT_VALUE));
            mediaDesc.add(new PropertyValue("FilterName", -1, m_filterName, PropertyState.DIRECT_VALUE));
            mediaDesc.add(new PropertyValue("Overwrite", -1, Boolean.TRUE, PropertyState.DIRECT_VALUE));
            mediaDesc.add(new PropertyValue("FilterData", -1, filterData.toArray(new PropertyValue[filterData.size()]), PropertyState.DIRECT_VALUE));

            try {
                // store loaded component content with correctly setup filter
                storable.storeToURL(outputFileUrlString, mediaDesc.toArray(new PropertyValue[mediaDesc.size()]));
                m_resultJobErrorEx = new JobErrorEx((m_outputFile.length() > 0) ? JobError.NONE : JobError.GENERAL);

                if (trace) {
                    ServerManager.logTrace(new StringBuilder(256).
                        append("DC ReaderEngine finished storing document: ").
                        append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").toString());
                }
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);

                if (m_resultJobErrorEx.hasNoError()) {
                    m_resultJobErrorEx = new JobErrorEx(JobError.GENERAL);
                }

                if (trace) {
                    final String excpMessage = e.getMessage();

                    ServerManager.logTrace(new StringBuilder(256).
                        append((null != excpMessage) ?
                            ("DC ReaderEngine error storing document with exception: " + excpMessage + ":") :
                                "DC ReaderEngine killed while storing document.").
                        append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").toString());
                }

                throw e;
            }
        } else if (m_resultJobErrorEx.hasNoError()) {
            m_resultJobErrorEx = new JobErrorEx(JobError.GENERAL);
        }

        if (null != component) {
            try {
                closeComponent(component);
                converterStatus = ConverterStatus.OK;
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);

                if (m_resultJobErrorEx.hasNoError()) {
                    converterStatus = ConverterStatus.OK_WITH_ENGINE_ERROR;
                } else {
                    throw e;
                }
            }
        }

        return converterStatus;
    }

    /**
     * @param component The component which contains the document to export.
     * @param filterShortName The short name of the filter: html, pdf, odf or ooxml.
     * @return
     */
    private void setFilterParams(XComponent component, String appType) {
        boolean writer = false, calc = false, impress = false, draw = false;

        if ((null == m_filterShortName) || !(m_filterShortName.equalsIgnoreCase("pdf") || m_filterShortName.equalsIgnoreCase("odf") || m_filterShortName.equalsIgnoreCase("ooxml"))) {
            m_filterShortName = "html";
        } else {
            m_filterShortName = m_filterShortName.toLowerCase();
        }

        // get type of loaded component
        if (null != component) {
            // standad application detection did not work
            // => try it via the detected application type
            if (!writer && !calc && !impress && !draw && (null != appType)) {
                if ((appType.indexOf("calc") != -1) || (appType.indexOf("excel") != -1) || (appType.indexOf("spreadsheet") != -1)) {
                    calc = true;
                } else if ((appType.indexOf("impress") != -1) || (appType.indexOf("powerpoint") != -1) || (appType.indexOf("presentation")) != -1) {
                    impress = true;
                } else if ((appType.indexOf("draw") != -1) || (appType.indexOf("image") != -1) || (appType.indexOf("svg") != -1)) {
                    draw = true;
                } else {
                    writer = true;
                }
            }
        }

        // get corresponding filtername, mimetype and extension for export filter in conjunction with application
        if ("html".equals(m_filterShortName)) {
            m_filterName = "writer_ox_multi_exporter_filter";
            m_resultMimeType = "text/html;charset=UTF-8";
            m_resultExtension = "html";
        } else if ("pdf".equals(m_filterShortName)) {
            m_filterName = "writer_pdf_Export";
            m_resultMimeType = "application/pdf";
            m_resultExtension = "pdf";
        } else if ("odf".equals(m_filterShortName)) {
            if (calc) {
                m_filterName = "calc8";
                m_resultMimeType = "application/vnd.oasis.opendocument.spreadsheet";
                m_resultExtension = "ods";
            } else if (impress) {
                m_filterName = "impress8";
                m_resultMimeType = "application/vnd.oasis.opendocument.presentation";
                m_resultExtension = "odp";
            } else if (draw) {
                m_filterName = "draw8";
                m_resultMimeType = "application/vnd.oasis.opendocument.graphics";
                m_resultExtension = "odg";
            } else {
                m_filterName = "writer8";
                m_resultMimeType = "application/vnd.oasis.opendocument.text";
                m_resultExtension = "odt";
            }
        } else if ("ooxml".equals(m_filterShortName)) {
            if (calc) {
                m_filterName = "Calc MS Excel 2007 XML";
                m_resultMimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                m_resultExtension = "xlsx";
            } else if (impress || draw) {
                m_filterName = "Impress MS PowerPoint 2007 XML";
                m_resultMimeType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                m_resultExtension = "pptx";
            } else {
                m_filterName = "MS Word 2007 XML";
                m_resultMimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                m_resultExtension = "docx";
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#applyJobProperties(java.util.HashMap)
     */
    @Override
    protected void applyJobProperties() {
        super.applyJobProperties();

        Object obj = null;

        if (null != (obj = m_jobProperties.get(Properties.PROP_FILTER_SHORT_NAME))) {
            m_filterShortName = (String) obj;
        }
    }

    // - Members ---------------------------------------------------------------

    /**
     * The short name of the export filter to be used (html, pdf, odf or ooxml)
     */
    protected String m_filterShortName = null;

    /**
     * The UNO filtername to be used for the export
     */
    protected String m_filterName = null;

    final String jobType;
}
