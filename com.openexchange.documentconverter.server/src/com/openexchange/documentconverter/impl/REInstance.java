/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.impl.api.REDescriptor;
import com.sun.star.beans.XPropertySet;
import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.frame.XDesktop;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.task.XInteractionContinuation;
import com.sun.star.task.XInteractionHandler;
import com.sun.star.task.XInteractionRequest;
import com.sun.star.task.XInteractionURLResolver;
import com.sun.star.uno.AnyConverter;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;

// ------------------
// - class Instance -
// ------------------

/**
 * {@link REInstance}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
/**
 * {@link REInstance}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.4
 */
final class REInstance {

    final static int BEFORE_CONNECT_DELAY_MS = 250;

    final static int RETRY_CONNECT_TIMEOUT_MS = 500;

    final static int CONNECT_TIMEOUT_MS = 60000;

    /**
     * {@link ProcessReaderRunnable}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     */
    private static class ProcessReaderRunnable implements Runnable {

        /**
         * Initializes a new {@link ProcessReaderRunnable}.
         *
         * @param processInputStream
         */
        ProcessReaderRunnable(REInstance instance, Process process, InputStream processInputStream) {
            super();

            m_instance = instance;
            m_instanceProcess = process;
            m_processInputStream = processInputStream;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            if (null != m_processInputStream) {
                final boolean trace = ServerManager.isLogTrace();

                try (final BufferedReader  bufferedReader = new BufferedReader(new InputStreamReader(m_processInputStream))) {
                    String outputStr = null;

                    while (null != (outputStr = bufferedReader.readLine())) {
                        if (trace && StringUtils.isNotEmpty(outputStr = implFilterOutput(outputStr))) {
                            ServerManager.logTrace("DC ReaderEngine output: " + outputStr);
                        }
                    }
                } catch (final Exception e) {
                    ServerManager.logExcp(e);
                }

                ServerManager.logTrace("DC ReaderEngine instance thread finished");

                m_instance.implProcessTerminated(m_instanceProcess);
                m_instance = null;
                m_instanceProcess = null;

                ServerManager.close(m_processInputStream);
            }
        }

        // - Implementation ----------------------------------------------------

        /**
         * @param outputStr
         * @return
         */
        private String implFilterOutput(final String outputStr) {
            String ret = null;

            if (StringUtils.isNotEmpty(outputStr)) {
                if ((-1 == outputStr.indexOf("warn:")) &&
                    (-1 == outputStr.indexOf("Can't open storage"))) {

                    ret = outputStr;
                }
            }

            return ret;
        }

        // - Members -----------------------------------------------------------

        private REInstance m_instance = null;

        private Process m_instanceProcess = null;

        private InputStream m_processInputStream = null;
    }

    /**
     * {@link URLResolveHandler}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    private static class URLResolveHandler implements XInteractionHandler {

        /**
         * Initializes a new {@link URLResolveHandler}.
         */
        URLResolveHandler(@NonNull final ServerManager serverManager, final int tempDirId) {
            super();

            m_tempDir = serverManager.createTempDir("oxurl-" + tempDirId + "-");
        }

        /* (non-Javadoc)
         * @see com.sun.star.task.XInteractionHandler#handle(com.sun.star.task.XInteractionRequest)
         */
        @Override
        public void handle(XInteractionRequest xRequest) {
            if (null != xRequest) {
                final XInteractionContinuation[] xContinuations = xRequest.getContinuations();

                if (null != xContinuations) {
                    for (int i = 0; i < xContinuations.length; ++i) {
                        final XInteractionContinuation curContinuation = xContinuations[i];

                        if (null != curContinuation) {
                            try {
                                final XInteractionURLResolver xURLResolver = UnoRuntime.queryInterface(XInteractionURLResolver.class, curContinuation);

                                if (null != xURLResolver) {
                                    final String sourceURL = xURLResolver.getSourceURL();
                                    final File outputFile = new File(m_tempDir, Long.toString(TEMPFILE_ID.incrementAndGet()));
                                    final MutableWrapper<String> extensionWrapper = new MutableWrapper<>(null);
                                    final File ret = ServerManager.resolveURL(sourceURL, outputFile, extensionWrapper);

                                    if (null == ret) {
                                        FileUtils.deleteQuietly(outputFile);
                                    } else {
                                        final String extension = extensionWrapper.get();
                                        final File outputFileWithExtension = (StringUtils.isNotBlank(extension)) ?
                                            new File(outputFile.getAbsolutePath() + "." + extension) :
                                                outputFile;

                                        if (outputFile != outputFileWithExtension) {
                                            outputFile.renameTo(outputFileWithExtension);
                                        }

                                        xURLResolver.setTargetURL("file://" + outputFileWithExtension.getAbsolutePath());
                                    }
                                }
                            } catch (final Exception e) {
                                ServerManager.logExcp(e);
                            }
                        }
                    }
                }
            }
        }

        // - Implementation ----------------------------------------------------

        /**
         *
         */
        public void kill() {
            FileUtils.deleteQuietly(m_tempDir);
        }

        // - Static Members ----------------------------------------------------

        final private static AtomicLong TEMPFILE_ID = new AtomicLong(0);

        // - Members -----------------------------------------------------------

        final private File m_tempDir;
    }

    /**
     * Initializes a new {@link REInstance}.
     */
    public REInstance(
        @NonNull final ServerManager serverManager,
        @NonNull final JobProcessor jobProcessor,
        @NonNull final REDescriptor reDescriptor) {

        super();

        m_serverManager = serverManager;
        m_jobProcessor = jobProcessor;
        m_descriptor = reDescriptor;
    }

    /**
     * @return
     */
    public boolean launch() {
        boolean ret = false;

        // kill any existing instance
        kill();

        // launch new instance
        final String installPath = m_descriptor.installPath;
        final String userPath = m_descriptor.userPath;

        if (implIsValidInstallation(installPath, userPath)) {
            final long launchStartTime = System.currentTimeMillis();

            try {
                String uuid = null;

                do {
                    // create unique user directory name to be used for this server instance
                    // remove all dashes from the UUID, since the MD5 algorithm will produce
                    // different results than the LO MD5 algorithm with these characters
                    uuid = UUID.randomUUID().toString().replaceAll("-", "");
                    m_userDirFile = new File(userPath, m_pipeName = ("ooxserver_" + uuid));
                } while (m_userDirFile.exists());

                if (ServerManager.validateOrMkdir(m_userDirFile)) {
                    final File programDirFile = new File(installPath, "program").getCanonicalFile();
                    final String userDirURL = "file://" + m_userDirFile.getCanonicalPath();
                    String executableName = "soffice";

                    // add exe extension on Windows
                    if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
                        executableName += ".exe";
                    }

                    m_programFile = new File(programDirFile, executableName);

                    final Vector<String> cmdVector = new Vector<>();

                    cmdVector.add(m_programFile.getCanonicalPath());

                    // max vmem
                    if (m_descriptor.maxVMemMB > 0) {
                        cmdVector.add("-maxvmem");
                        cmdVector.add(Long.toString(m_descriptor.maxVMemMB));
                    }

                    // set tmp dir for readerEngine processes
                    cmdVector.add("-tmpdir");
                    cmdVector.add(m_userDirFile.getCanonicalPath());

                    // set blacklist URL pattern file
                    if (null != m_descriptor.blacklistFile) {
                        cmdVector.add("-blacklist");
                        cmdVector.add(m_descriptor.blacklistFile);
                    }

                    // set whitelist URL pattern file
                    if (null != m_descriptor.whitelistFile) {
                        cmdVector.add("-whitelist");
                        cmdVector.add(m_descriptor.whitelistFile);
                    }

                    // set maximum count of links to be resolved
                    cmdVector.add("-urllinklimit");
                    cmdVector.add(Integer.toString(m_descriptor.urlLinkLimit));

                    // set proxy server to resolve external URLs
                    if (null != m_descriptor.urlLinkProxy) {
                        cmdVector.add("-urllinkproxy");
                        cmdVector.add(m_descriptor.urlLinkProxy);
                    }

                    cmdVector.add("--accept=pipe,name=" + m_pipeName + ";urp;StarOffice.ServiceManager");
                    cmdVector.add("--headless");
                    cmdVector.add("--invisible");
                    cmdVector.add("--minimized");
                    cmdVector.add("--nocrashreport");
                    cmdVector.add("--nodefault");
                    cmdVector.add("--nofirststartwizard");
                    cmdVector.add("--nologo");
                    cmdVector.add("--norestore");
                    cmdVector.add("-env:UserInstallation=" + userDirURL);

                    final ProcessBuilder procBuilder = new ProcessBuilder(cmdVector);

                    procBuilder.directory(m_userDirFile);
                    procBuilder.redirectErrorStream(true);

                    if (null != (m_process = procBuilder.start())) {
                        @SuppressWarnings("resource") // ok, since procInputStream will be deleted when thread finishes
                        final InputStream procInputStream = m_process.getInputStream();

                        if (null != procInputStream) {
                            final ProcessReaderRunnable processReaderRunnable = new ProcessReaderRunnable(this, m_process, procInputStream);
                            final ExecutorService readerEngineExecutorService = m_jobProcessor.getReaderEngineExecutorService();

                            if (null != readerEngineExecutorService) {
                                readerEngineExecutorService.execute(processReaderRunnable);
                            } else {
                                (new Thread(processReaderRunnable)).start();
                            }
                        }
                    }
                }
            } catch (final java.lang.Exception e) {
                ServerManager.logExcp(e);
                m_process = null;
            }

            // try to get a connection with a defined delay time, retry timeout and final timeout
            if (null != m_process) {
                boolean isAlive = false;

                // check if process has already been exited;
                // ugly test via Exception, but Java 1.8 impl.
                // for Process#isAlive obviously does the same internally
                try {
                    m_process.exitValue();
                } catch (@SuppressWarnings("unused") final IllegalThreadStateException e) {
                    isAlive = true;
                }

                if (isAlive) {
                    final long startTime = System.currentTimeMillis();

                    ServerManager.sleepThread(BEFORE_CONNECT_DELAY_MS, true);

                    while (!implInitConnection() && (System.currentTimeMillis() < (startTime + CONNECT_TIMEOUT_MS))) {
                        ServerManager.sleepThread(RETRY_CONNECT_TIMEOUT_MS, true);
                    }

                    implPrepareInstance();
                }
            }

            // force process destroy and cleanup in case we really got no connection;
            if (!isConnected()) {
                kill();
            } else if (ServerManager.isLogInfo()) {
                ServerManager.logInfo("DC ReaderEngine connected",
                    new LogData("launchtime", Long.valueOf(System.currentTimeMillis() - launchStartTime).toString() + "ms"));
            }
        } else if (ServerManager.isLogError()) {
            if ((null == installPath) || (installPath.length() < 1) || !(new File(installPath).canRead())) {
                ServerManager.logError("DC installation: install dir is not readable: " + installPath);
            } else if ((null == userPath) || (userPath.length() < 1) || !(new File(userPath).canWrite())) {
                ServerManager.logError("DC installation: user dir is not writable: " + userPath);
            }
        }

        // set the lauch flag to indicate a failure of the last launch try
        if (isConnected()) {
            implInitializeReaderEngineServices();
            ret = true;

            ServerManager.logInfo("DC ReaderEngine launched", new LogData("max.VMem MB", Long.toString(m_descriptor.maxVMemMB)));
        } else {
            ServerManager.logError("DC ReaderEngine could not be launched", new LogData("installpath", m_descriptor.installPath));
        }

        return ret;
    }

    /**
     *
     */
    public synchronized void kill() {
        Process curProcess = m_process;
        final int curPid = m_instancePid;

        m_process = null;
        m_desktop = null;
        m_serviceFactory = null;
        m_componentFactory = null;
        m_defaultContext = null;
        m_instancePid = 0;

        // force a hard destroy of the process
        if (null != curProcess) {
            try {
                // if we do have a PID, use SIGKILL (kill -9) to make
                // sure the process is really killed in time first
                if (0 != curPid) {
                    final String pidStr = Integer.toString(curPid);
                    final ProcessBuilder killProcessBuilder = new ProcessBuilder("kill", "-9", pidStr);
                    killProcessBuilder.redirectErrorStream(true);
                    final Process killProcess = killProcessBuilder.start();

                    if (null != killProcess) {
                        killProcess.waitFor();
                    }
                }
            } catch (final Exception e) {
                ServerManager.logExcp(e);
            } finally {
                // finally call destroyForcibly (most possibly a SIGTERM (kill -15)
                // internally) to destroy the Java Process object in a correct manner
                curProcess.destroyForcibly();
                curProcess = null;
            }
        }

        // remove user directory and URL content directory for this server instance
        FileUtils.deleteQuietly(m_userDirFile);

        // kill URL resolve handler for this instance
        if (null != m_urlResolveHandler) {
            m_urlResolveHandler.kill();
            m_urlResolveHandler = null;
        }

        // get rid of OSL_PIPE_* files located in LO/AOO hard coded /tmp or /var/tmp directories
        if ((null != m_userDirFile) && (null != m_pipeName)) {
            File tmpDir = new File("/tmp");

            if (!tmpDir.canWrite()) {
                tmpDir = new File("/var/tmp");
            }

            if (tmpDir.canWrite()) {
                final String tmpDirEntries[] = tmpDir.list();

                if (null != tmpDirEntries) {
                    // LO/AOO uses a sal_Unicode md5 hash and not a character md5 hash,
                    // so translate each character into a pair of bytes; each byte is
                    // afterwards translated into its corresponding hex value, omitting
                    // possible leading zeros for each byte to hex conversion
                    String md5String = null;
                    String userDirURL = null;

                    try {
                        userDirURL = "file://" + m_userDirFile.getCanonicalPath();

                        final char[] charArray = userDirURL.toCharArray();

                        if (charArray.length > 0) {
                            final byte[] byteArray = new byte[charArray.length << 1];

                            // translate char array to byte array
                            for (int i = 0, k = 0; i < charArray.length;) {
                                final char curChar = charArray[i++];

                                byteArray[k++] = (byte) (curChar & 0xFF);
                                byteArray[k++] = (byte) ((curChar >> 8) & 0xFF);
                            }

                            // get the 16 byte MD5 Digest
                            final byte[] md5 = DigestUtils.md5(byteArray);

                            // translate the 16 bytes array into the corresponding, LO conform hex string
                            if ((null != md5) && (md5.length == 16)) {
                                try (final StringWriter md5StringWriter = new StringWriter(32);
                                    final PrintWriter md5PrintWriter = new PrintWriter(md5StringWriter)) {
                                    for (int n = 0; n < 16;) {
                                        md5PrintWriter.printf("%x", Byte.valueOf(md5[n++]));
                                    }

                                    md5PrintWriter.flush();
                                    md5String = md5StringWriter.toString();
                                }
                            }
                        }
                    } catch (final Exception e) {
                        ServerManager.logExcp(e);
                    }

                    for (final String curEntry : tmpDirEntries) {
                        if (curEntry.startsWith("OSL_PIPE_") && ((curEntry.endsWith(m_pipeName) || ((null != md5String) && curEntry.endsWith(md5String))))) {
                            FileUtils.deleteQuietly(new File(tmpDir, curEntry));
                        }
                    }
                }
            }
        }

        m_programFile = m_userDirFile = null;
        m_pipeName = null;
    }

    /**
     * @return
     */
    public boolean isDisposed() {
        Process curProcess = null;
        int curPid = 0;
        boolean isAlive = false;

        synchronized (this) {
            curProcess = m_process;
            curPid = m_instancePid;
        }

        // in case of an obviously running process, deeply check
        // if the system process with instance Pid is really running;
        // return true for a disposed instance process
        if ((null != curProcess) && (0 != curPid)) {
            final String pidStr = "" + curPid;

                if (HAS_PROCSFS) {
                isAlive = (new File("/proc/" + pidStr, "status")).canRead();
            } else {
                try {
                    final ProcessBuilder psProcessBuilder = new ProcessBuilder("ps", "-p", pidStr);

                    psProcessBuilder.redirectErrorStream(true);

                    final Process psProcess = psProcessBuilder.start();

                    if (null != psProcess) {
                        try (BufferedReader psProcessReader = new BufferedReader(new InputStreamReader(psProcess.getInputStream()))) {
                            String psProcessLine = null;

                            while (!isAlive && (null != (psProcessLine = psProcessReader.readLine()))) {
                                if (StringUtils.isNotEmpty(psProcessLine) && (psProcessLine.indexOf(pidStr) != -1)) {
                                    isAlive = true;
                                }
                            }
                        }
                    }
                } catch (final IOException e) {
                    ServerManager.logExcp(e);
                }
            }
        }

        return !isAlive;
    }

    /**
     * @return
     */
    public boolean isConnected() {
        return ((null != m_process) && (null != m_componentFactory) && (null != m_defaultContext) && (null != m_serviceFactory) && (null != m_desktop));
    }

    /**
     * @return
     */
    public int getRestartCount() {
        return m_descriptor.restartCount;
    }

    /**
     * @return
     */
    public XMultiComponentFactory getComponentFactory() {
        return m_componentFactory;
    }

    /**
     * @return
     */
    public XComponentContext getDefaultContext() {
        return m_defaultContext;
    }

    /**
     * @return
     */
    public com.sun.star.lang.XMultiServiceFactory getFilterFactory() {
        return m_serviceFactory;
    }

    /**
     * @return
     */
    public com.sun.star.frame.XDesktop getDesktop() {
        return m_desktop;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return true if a connection has been established (new or already connected)
     */
    private boolean implInitConnection() {
        boolean ret = isConnected();

        if (!ret) {
            try {
                final XComponentContext xComponentContext = Bootstrap.createInitialComponentContext(null);
                final XMultiComponentFactory xMultiComponentFactory = xComponentContext.getServiceManager();
                final Object aObjectUrlResolver = xMultiComponentFactory.createInstanceWithContext(
                    "com.sun.star.bridge.UnoUrlResolver",
                    xComponentContext);

                m_componentFactory = UnoRuntime.queryInterface(
                    XMultiComponentFactory.class,
                    UnoRuntime.queryInterface(XUnoUrlResolver.class, aObjectUrlResolver).resolve(
                        "uno:pipe,name=" + m_pipeName + ";urp;StarOffice.ServiceManager"));

                if (null != m_componentFactory) {
                    m_defaultContext = UnoRuntime.queryInterface(
                        XComponentContext.class,
                        UnoRuntime.queryInterface(XPropertySet.class, m_componentFactory).getPropertyValue("DefaultContext"));

                    m_desktop = UnoRuntime.queryInterface(
                        XDesktop.class,
                        m_componentFactory.createInstanceWithContext("com.sun.star.frame.Desktop", m_defaultContext));

                    m_serviceFactory = UnoRuntime.queryInterface(
                        XMultiServiceFactory.class,
                        m_componentFactory.createInstanceWithContext("com.sun.star.document.FilterFactory", m_defaultContext));
                }
            } catch (@SuppressWarnings("unused") final com.sun.star.connection.NoConnectException e) {
                // try again later
            } catch (final com.sun.star.uno.Exception e) {
                ServerManager.logExcp(e);
            } catch (final java.lang.Exception e) {
                ServerManager.logExcp(e);
            } finally {
                if (!(ret = isConnected())) {
                    m_serviceFactory = null;
                    m_desktop = null;
                    m_componentFactory = null;
                    m_defaultContext = null;
                }
            }
        }

        return ret;
    }

    /**
     *
     */
    private void implPrepareInstance() {
        try {
            Thread.sleep(BEFORE_CONNECT_DELAY_MS, 0);
        } catch (@SuppressWarnings("unused") final InterruptedException e) {
            // Ok
        }

        synchronized (this) {
            if (isConnected()) {
                try {
                    final XMultiComponentFactory serviceFactory = getComponentFactory();
                    final XComponentContext serviceContext = getDefaultContext();

                    if ((null != serviceFactory) && (null != serviceContext)) {
                        try {
                            final XPropertySet oxAliveProperties = UnoRuntime.queryInterface(
                                XPropertySet.class,
                                serviceFactory.createInstanceWithContext(UNO_SERVICENAME_ALIVESERVICE, serviceContext));

                            if (null != oxAliveProperties) {
                                m_instancePid = Integer.parseInt(AnyConverter.toString(
                                    oxAliveProperties.getPropertyValue(UNO_PROPERYTY_GETPID)));
                            }
                        } catch (final Exception e) {
                            ServerManager.logExcp(e);
                        }
                    }
                } catch (final Exception e) {
                    ServerManager.logExcp(e);
                }
            }
        }
    }

    /**
    *
    */
   private void implInitializeReaderEngineServices() {
       final XMultiComponentFactory serviceFactory = getComponentFactory();
       final XComponentContext serviceContext = getDefaultContext();
       XPropertySet xRemoteProxyService = null;

       if ((null != serviceFactory) && (null != serviceContext)) {
           try {
               xRemoteProxyService = UnoRuntime.queryInterface(XPropertySet.class,
                   serviceFactory.createInstanceWithContext(UNO_SERVICENAME_REMOTEPROXY, serviceContext));

               if (null != xRemoteProxyService) {
                   // set Remote proxy at ReaderEngine
                   final String proxy = m_descriptor.urlLinkProxy;

                   if ((null != proxy) && (proxy.length() > 0)) {
                       try {
                           xRemoteProxyService.setPropertyValue(UNO_PROPERTY_REMOTEPROXY, proxy);
                       } catch (final Exception e) {
                           ServerManager.logExcp(e);
                       }
                   }

                   // set URL resolver at ReaderEngine
                   if (null != m_urlResolveHandler) {
                       m_urlResolveHandler.kill();
                       m_urlResolveHandler = null;
                   }

                   try {
                       m_urlResolveHandler = new URLResolveHandler(m_serverManager, m_instancePid);
                       xRemoteProxyService.setPropertyValue(UNO_PROPERTY_URLRESOLVER_HANDLER, m_urlResolveHandler);
                   } catch (final Exception e) {
                       ServerManager.logExcp(e);
                   }
               }
           } catch (final Exception e) {
               ServerManager.logExcp(e);
           }
       }
   }

    /**
     * @param aInstallDir
     * @param aUserBaseDir
     * @return true in case the given files specify a valid readerEngine installation
     */
   private synchronized boolean implIsValidInstallation(String installDir, String userBaseDir) {
        if (null == VALID_INSTALLATION) {
            final File instDir = ((null != installDir) && (installDir.length() > 0)) ? new File(installDir) : null;
            final File userDir = ((null != userBaseDir) && (userBaseDir.length() > 0)) ? new File(userBaseDir) : null;

            VALID_INSTALLATION = new AtomicBoolean(
                (null != instDir) && instDir.canRead() &&
                (null != userDir) &&  ServerManager.validateOrMkdir(userDir));
        }

        return VALID_INSTALLATION.get();
    }

    /**
     * @param process
     */
    protected void implProcessTerminated(Process process) {
        if ((null != process) && process.equals(m_process)) {
            ServerManager.logTrace("DC ReaderEngine instance thread finished unexpectedly => forcing Process destruction of terminated instance");
            kill();
        }
    }

    // - Static members -------------------------------------------------------

    final private static String UNO_SERVICENAME_ALIVESERVICE = "com.sun.star.config.OxAliveService";

    final private static String UNO_SERVICENAME_REMOTEPROXY = "com.sun.star.config.OxRemoteProxyService";

    final private static String UNO_PROPERYTY_GETPID = "OxGetPID";

    final private static String UNO_PROPERTY_REMOTEPROXY = "OxRemoteProxy";

    final private static String UNO_PROPERTY_URLRESOLVER_HANDLER = "OxURLResolverHandler";

    private static AtomicBoolean VALID_INSTALLATION = null;

    private static boolean HAS_PROCSFS = false;

    // get ProcFS status:
    // /proc/cpuinfo needs to be readable and last modification must not be older than 1 hour
    static {
        final File procCheckFile = new File("/proc/cpuinfo");
        HAS_PROCSFS = (procCheckFile.canRead() && ((System.currentTimeMillis() - procCheckFile.lastModified()) <= (60*60*1000)));
    }

    // - Members ---------------------------------------------------------------

    final private ServerManager m_serverManager;

    final private JobProcessor m_jobProcessor;

    final private REDescriptor m_descriptor;

    private volatile Process m_process = null;

    private volatile int m_instancePid = 0;

    private volatile XDesktop m_desktop = null;

    private volatile XMultiComponentFactory m_componentFactory = null;

    private volatile XMultiServiceFactory m_serviceFactory = null;

    private volatile XComponentContext m_defaultContext = null;

    private File m_programFile = null;

    private File m_userDirFile = null;

    private String m_pipeName = null;

    private URLResolveHandler m_urlResolveHandler = null;
}
