/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

/**
 * {@link ConverterMeasurement}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.6.0
 */
public enum ConverterMeasurement {

    NONE("Unknown"),

    MEASURE_CONVERT_CYCLE("Measuring Manager#convert cycle"),

    MEASURE_IMAGESERER_CYCLE("Measuring Manager#ImageServer cycle"),

    MEASURE_BEGIN_GET_END_CYCLE("Measuring Manager#beginConversion/getPage/endConversion cycle"),

    SCHEDULE_JOB("Scheduling job in queue"),

    EXECUTE_LOCAL_CONVERT("Executing local #convert"),

    EXECUTE_REMOTE_CONVERT("Executing remote #convert"),

    EXECUTE_LOCAL_BEGIN_CONVERT("Executing local #beginConvert"),

    EXECUTE_REMOTE_BEGIN_CONVERT("Executing remote #beginConvert"),

    EXECUTE_LOCAL_GET_PAGE("Executing local #getPage"),

    EXECUTE_REMOTE_GET_PAGE("Executing remote #getPage"),

    EXECUTE_LOCAL_END_CONVERT("Executing local #endConvert"),

    EXECUTE_REMOTE_END_CONVERT("Executing remote #endConvert"),

    EXECUTE_CONVERTER_JOB("Executing DCJob"),

    LOAD_SOURCE_DOCUMENT("Loading source document"),

    GENERIC_CONVERSION("Generic conversion"),

    GRAPHIC_CONVERSION("Graphic conversion"),

    IMAGESERVER_TRANSFORMATION("ImageServer Transformation"),

    CONVERT_DOC_TO_GRAPHIC("Document to Graphic conversion"),

    CONVERT_DOC_TO_ODF("Document to ODF conversion"),

    CONVERT_DOC_TO_OOXML("Document to OOXML conversion"),

    CONVERT_DOC_TO_PDF("Document to PDF conversion"),

    CONVERT_PDF_TO_SVG("PDF to SVG conversion"),

    CONVERT_PDF_TO_GRAPHIC("PDF to Graphic conversion"),

    PROCESS_REMOTE_RESPONE("Processing remote response"),

    GET_FALLBACK_RESULT("Getting fallback result from resource"),

    READ_CACHE_ENTRY("Reading cache entry"),

    EXECUTE_NON_CACHED_JOB("Executing non cached job"),

    WRITING_RESULT_TO_CACHE("Writing result to cache"),

    PREPARING_JOB_INPUT_FILE("Preparing job input file");

    /**
     * Initializes a new {@link TitleMapping}.
     * @param title
     */
    ConverterMeasurement(String title) {
        m_title = title;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return m_title;
    }

    // - string mapped enums -----------------------------------------------

    private String m_title = null;
}
