/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.IJob;
import com.openexchange.documentconverter.impl.api.IJobStatusListener;
import com.openexchange.documentconverter.impl.api.IQueueMonitor;
import com.openexchange.documentconverter.impl.api.JobEntry;
import com.openexchange.documentconverter.impl.api.JobStatus;
import com.openexchange.exception.ExceptionUtils;

/**
 * {@link ManagedJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
class ManagedJob implements IJobStatusListener {

    final private static long MAX_RUNNING_TO_JOBEXEC_TIME_PERCENTAGE = 225;
    final private static long MAX_QUEUETIME_TO_JOBEXEC_TIME_PERCENTAGE = MAX_RUNNING_TO_JOBEXEC_TIME_PERCENTAGE * 10;

    /**
     * Initializes a new {@link ManagedJob}.
     */
    ManagedJob(@NonNull final ServerManager serverManager, @NonNull final JobProcessor jobProcessor) {
        this(serverManager, jobProcessor, null);
    }

    /**
     * Initializes a new {@link ManagedJob}.
     */
    ManagedJob(
        @NonNull final ServerManager serverManager,
        @NonNull final JobProcessor jobProcessor,
        @Nullable final IQueueMonitor queueMonitor) {

        super();

        m_serverManager = serverManager;
        m_jobProcessor = jobProcessor;
        m_queueMonitor = queueMonitor;
    }

    /**
     * @param jobType
     * @param jobProperties
     * @param resultProperties
     * @return
     */
    protected InputStream process(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        String usedJobType = StringUtils.isBlank(jobType) ? null : jobType.toLowerCase();
        InputStream resultStm = null;

        if ((null != usedJobType) && m_jobProcessor.isRunning()) {
            final HashMap<String, Object> curJobProperties = new HashMap<>(12);
            final HashMap<String, Object> curResultProperties = new HashMap<>(8);

            ServerManager.ensureInputTypeSet(jobProperties);

            // create a temporary properties map
            curJobProperties.putAll(jobProperties);

            final MutableWrapper<Boolean> deleteInputFile = new MutableWrapper<>(Boolean.FALSE);
            final File jobInputFile = prepareJob(m_jobProcessor, usedJobType, curJobProperties, curResultProperties, deleteInputFile);

            if (null != jobInputFile) {
                final String inputType = (String) curJobProperties.get(Properties.PROP_INPUT_TYPE);
                final boolean isPDFInput = STR_PDF.equals(inputType);

                if ((STR_PDF.equals(usedJobType) || STR_PDF_A.equals(usedJobType)) && isPDFInput) {
                    // just return the given input document content,
                    // if a PDF conversion of a PDF file is requested
                    try {
                        final byte[] resultBuffer = FileUtils.readFileToByteArray(jobInputFile);

                        resultStm = new ByteArrayInputStream(resultBuffer);
                        resultProperties.put(Properties.PROP_RESULT_BUFFER, resultBuffer);
                        resultProperties.put(Properties.PROP_CACHE_HASH, curResultProperties.get(Properties.PROP_CACHE_HASH));
                    } catch (final IOException e) {
                        ServerManager.logExcp(e);
                    }
                } else {
                    String jobDescription = null;
                    final boolean pdfEncTest = "pdfenctest".equals(usedJobType) && isPDFInput;

                    // previews and thumbnails are mapped to image conversions
                    // with the first page only
                    if ("preview".equals(usedJobType) || "thumbnail".equals(usedJobType) || pdfEncTest) {
                        usedJobType = "image";
                        curJobProperties.put(Properties.PROP_PAGE_RANGE, "1");

                        // For PDF encryption test, create a dummy 4x4 pixel PNG graphic
                        if (pdfEncTest) {
                            final Integer dummy4PixelExtent = Integer.valueOf(4);

                            curJobProperties.put(Properties.PROP_RESULT_MIME_TYPE, "image/png");
                            curJobProperties.put(Properties.PROP_PIXEL_WIDTH, dummy4PixelExtent);
                            curJobProperties.put(Properties.PROP_PIXEL_HEIGHT, dummy4PixelExtent);
                        }
                    }

                    if ("image".equals(usedJobType)) {
                        // set jobm_lock type to pdf2svg in case we have a PDF input
                        if (isPDFInput) {
                            usedJobType = "pdf2grf";
                            jobDescription = PDF_TO_GRAPHIC_JOB;
                        } else {
                            jobDescription = STANDARD_GRAPHIC_JOB;
                        }
                    } else if (usedJobType.startsWith("html")) {
                        usedJobType = "pdf2html";
                        jobDescription = PDF_TO_HTML_JOB;
                    } else {
                        jobDescription = GENERIC_JOB;
                    }

                    // do the conversion
                    final IJob job = m_jobProcessor.createServerJob(usedJobType, curJobProperties, resultProperties);

                    if (null != job) {
                        resultStm = processJob(m_jobProcessor, job, curJobProperties, resultProperties, jobDescription);
                    } else {
                        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
                    }

                    // For a PDF encryption test, we're only interested in the
                    // ERROR_CODE, so that we can immediately close the resulting
                    // dummy stream and delete a possibly created result output file
                    if (pdfEncTest) {
                        final File resultOutputFile = (File) curJobProperties.get(Properties.PROP_OUTPUT_FILE);

                        ServerManager.close(resultStm);

                        if ((null != resultOutputFile) && resultOutputFile.exists()) {
                            FileUtils.deleteQuietly(resultOutputFile);
                        }
                    }
                }
            } else if (curResultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, curResultProperties.get(Properties.PROP_RESULT_ERROR_CODE));

                if (curResultProperties.containsKey(Properties.PROP_RESULT_ERROR_DATA)) {
                    resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, curResultProperties.get(Properties.PROP_RESULT_ERROR_DATA));
                }
            }

            // cleanup input file, if flagged
            implDeleteFileIfFlagged((File) curJobProperties.remove(Properties.PROP_INPUT_FILE), deleteInputFile);
        } else  {
            // set GENERAL error in case of a missing job processor or job type
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        }

        if ((null != resultStm) && !resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.NONE.getErrorCode()));
        }

        return resultStm;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.IJobStatusListener#statusChanged(String )
     */
    @Override
    public void statusChanged(String jobId, Object data) {
        if (data instanceof JobEntry) {
            final JobStatus oldJobStatus = m_jobStatus;
            final JobEntry changedEntry = (JobEntry) data;

            try {
                m_lock.lock();

                m_jobStatus = changedEntry.getJobStatus();

                // scheduled entry has been removed from queue =>
                // change JobStatus to JobStatus.ERROR and signal waiting thread for this job
                if ((JobStatus.SCHEDULED == oldJobStatus) && (JobStatus.REMOVED == m_jobStatus)) {
                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC job was removed from queue => setting QUEUE_TIMEOUT error at job and signaling startedExecutionCondition",
                            new LogData("jobid", jobId));
                    }

                    m_jobErrorEx = new JobErrorEx(JobError.QUEUE_TIMEOUT);

                    changedEntry.setJobStatus(m_jobStatus = JobStatus.ERROR);
                    changedEntry.setJobErrorEx(m_jobErrorEx);
                    changedEntry.getResultProperties().put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(m_jobErrorEx.getErrorCode()));
                    changedEntry.getResultProperties().put(Properties.PROP_RESULT_ERROR_DATA, m_jobErrorEx.getErrorData().toString());

                    // signal started condition, followed by signaling finished condition
                    m_startedExecutionCondition.signal();
                } else if ((JobStatus.FINISHED == m_jobStatus) || (JobStatus.ERROR == m_jobStatus)) {
                    m_jobErrorEx = changedEntry.getJobErrorEx();

                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC job finished RUNNING state => signaling finishedExecutionCondition",
                            new LogData("jobid", jobId));
                    }

                    // notify waiting thread that this job has just finished
                    m_finishedExecutionCondition.signal();
                } else if (JobStatus.RUNNING == m_jobStatus) {
                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC job reached RUNNING state => signaling startedExecutionCondition",
                            new LogData("jobid", jobId));
                    }

                    // notify waiting thread that this job has just begun running
                    m_startedExecutionCondition.signal();
                }
            } finally {
                m_lock.unlock();
            }
        }
    }

    /**
     * @param jobProcessor
     * @param jobType
     * @param jobProperties
     * @param deleteInputFile
     * @return
     */
    protected File prepareJob(@NonNull JobProcessor jobProcessor, @NonNull String jobType, @NonNull HashMap<String, Object> jobProperties, @NonNull HashMap<String, Object> resultProperties, @NonNull MutableWrapper<Boolean> deleteInputFile) {
        File jobInputFile = m_serverManager.getJobInputFile(jobProperties, deleteInputFile);

        if (null != jobInputFile) {
            final String cacheHash = (String) jobProperties.get(Properties.PROP_CACHE_HASH);
            final String inputFileHash = (String) jobProperties.get(Properties.PROP_INPUTFILE_HASH);
            final String jobLocale = (String) jobProperties.get(Properties.PROP_LOCALE);
            final Integer jobFeaturesId = (Integer) jobProperties.get(Properties.PROP_FEATURES_ID);
            final JobPriority jobPriority = (JobPriority) jobProperties.get(Properties.PROP_PRIORITY);
            final String inputType = (String) jobProperties.get(Properties.PROP_INPUT_TYPE);
            final String inputURL = (String) jobProperties.get(Properties.PROP_INPUT_URL);
            final String fileName = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);

            if (ServerManager.isLogTrace() && (null == inputType)) {
                ServerManager.logTrace("DC JobProperties has no valid input type for the source document set => results may be inconsistent");
            }

            final boolean imageTransformation = "imagetransformation".equals(jobType);
            final boolean documentConversion = !imageTransformation && ("odf".equals(jobType) || "ooxml".equals(jobType) || "shape2png".equals(jobType));
            final boolean graphicConversion = !imageTransformation && "graphic".equals(jobType);
            final boolean pdfConversion = !imageTransformation && !documentConversion && !graphicConversion && !STR_PDF.equals(inputType);

            // check different ways to convert the current input file to PDF,
            // based on set user capabilites etc.
            if (pdfConversion) {
                final HashMap<String, Object> pdfJobProperties = new HashMap<>(12);
                final HashMap<String, Object> pdfResultProperties = new HashMap<>(8);

                pdfJobProperties.put(Properties.PROP_INPUT_FILE, jobInputFile);

                if (null != inputType) {
                    pdfJobProperties.put(Properties.PROP_INPUT_TYPE, inputType);
                }

                if (null != inputURL) {
                    pdfJobProperties.put(Properties.PROP_INPUT_URL, inputURL);
                }

                if (null != cacheHash) {
                    pdfJobProperties.put(Properties.PROP_CACHE_HASH, cacheHash);
                }

                if (null != inputFileHash) {
                    pdfJobProperties.put(Properties.PROP_INPUTFILE_HASH, inputFileHash);
                }

                if (null != jobLocale) {
                    pdfJobProperties.put(Properties.PROP_LOCALE, jobLocale);
                }

                if (null != jobFeaturesId) {
                    pdfJobProperties.put(Properties.PROP_FEATURES_ID, jobFeaturesId);
                }

                if (null != jobPriority) {
                    pdfJobProperties.put(Properties.PROP_PRIORITY, jobPriority);
                }

                if (null != fileName) {
                    pdfJobProperties.put(Properties.PROP_INFO_FILENAME, fileName);
                }


                pdfJobProperties.put(Properties.PROP_FILTER_SHORT_NAME, STR_PDF);
                pdfJobProperties.put(Properties.PROP_READERENGINE_ROOT, m_serverManager.getServerConfig().RE_INSTALLDIR);

                final IJob pdfJob = new DocumentConverterJob(pdfJobProperties, pdfResultProperties, STR_PDF_A.equalsIgnoreCase(jobType) ? STR_PDF_A : STR_PDF);

                try (final InputStream pdfResultStm = processJob(jobProcessor, pdfJob, pdfJobProperties, pdfResultProperties, STANDARD_PDF_JOB)) {
                    if (null != pdfResultStm) {
                        // remove invalid entries from the original job
                        jobProperties.remove(Properties.PROP_INPUT_FILE);
                        jobProperties.remove(Properties.PROP_INPUTFILE_HASH);
                        jobProperties.remove(Properties.PROP_INPUT_STREAM);

                        if (null != cacheHash) {
                            if (Cache.isValidHash(cacheHash)) {
                                // remove previously set cache hash value, to
                                // force creation of a new unique hash
                                jobProperties.remove(Properties.PROP_CACHE_HASH);
                            } else {
                                // set to invalid hash to indicate a non cached
                                // conversion request
                                jobProperties.put(Properties.PROP_CACHE_HASH, Properties.CACHE_NO_CACHEHASH);
                            }
                        }

                        // set adjusted job properties wrt. the new PDF input buffer
                        jobProperties.put(Properties.PROP_INPUT_STREAM, pdfResultStm);
                        jobProperties.put(Properties.PROP_INPUT_TYPE, STR_PDF);

                        if (null != inputURL) {
                            jobProperties.put(Properties.PROP_INPUT_URL, inputURL);
                        }

                        if (null != fileName) {
                            jobProperties.put(Properties.PROP_INFO_FILENAME, fileName);
                        }

                        // remove old input file, if flagged
                        implDeleteFileIfFlagged(jobInputFile, deleteInputFile);
                        jobInputFile = m_serverManager.getJobInputFile(jobProperties, deleteInputFile);
                    } else {
                        if (pdfResultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
                            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, pdfResultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
                        }

                        if (pdfResultProperties.containsKey(Properties.PROP_RESULT_ERROR_DATA)) {
                            resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, pdfResultProperties.get(Properties.PROP_RESULT_ERROR_DATA));
                        }

                        if (pdfResultProperties.containsKey(Properties.PROP_RESULT_CACHE_HASH)) {
                            resultProperties.put(Properties.PROP_RESULT_CACHE_HASH, pdfResultProperties.get(Properties.PROP_RESULT_CACHE_HASH));
                        }

                        if (ServerManager.isLogTrace() && jobProcessor.isRunning()) {
                            ServerManager.logTrace("DC failed " + STANDARD_PDF_JOB + " conversion ");
                        }

                        // remove old input file, if flagged
                        implDeleteFileIfFlagged(jobInputFile, deleteInputFile);
                        jobInputFile = null;
                    }
                } catch (final Exception e) {
                    // remove old input file, if flagged
                    implDeleteFileIfFlagged(jobInputFile, deleteInputFile);
                    ServerManager.logExcp(e);
                }
            }
        }

        return jobInputFile;
    }

    /**
     * @param jobProcessor
     * @param job
     * @param jobProperties
     * @param resultProperties
     */
    protected InputStream processJob(@NonNull JobProcessor jobProcessor, @NonNull IJob job, @NonNull HashMap<String, Object> jobProperties, @NonNull HashMap<String, Object> resultProperties, String jobDescription) {
        final String hash = job.getHash();
        final long jobStartTimeMillis = System.currentTimeMillis();
        final String filename = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);
        final JobErrorEx cachedJobErrorEx = jobProcessor.getErrorCache().getJobErrorExForHash(hash);
        final boolean trace = ServerManager.isLogTrace();
        final boolean debug = ServerManager.isLogDebug();
        InputStream ret = null;

        if (null == cachedJobErrorEx) {
            final Boolean cacheOnlyObj = (Boolean) jobProperties.get(Properties.PROP_CACHE_ONLY);
            final boolean cacheOnly = (null != cacheOnlyObj) && cacheOnlyObj.booleanValue();

            // create a job id in every case
            String jobId = UUID.randomUUID().toString();

            // lookup into result cache => if not found and no cache only flag is set => schedule job and process
            if ((null == (ret = ServerManager.getCachedResult(m_serverManager.getCache(), hash, filename, resultProperties, true))) && !cacheOnly) {
                final BackendType jobBackendType = job.backendTypeNeeded();

                // First check if configured max. queue count has been reached for Readerengine
                // jobs only => return immediately with appropriate error code set
                if ((null != m_queueMonitor) && m_queueMonitor.isQueueLimitReached() && (BackendType.READERENGINE == jobBackendType)) {
                    m_jobErrorEx = new JobErrorEx(JobError.MAX_QUEUE_COUNT);

                    resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(m_jobErrorEx.getErrorCode()));
                    resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, m_jobErrorEx.getErrorData().toString());

                    if (debug) {
                        ServerManager.logDebug("DC job convert error",
                                jobProperties,
                                new LogData("jobid", jobId),
                                new LogData("hash", hash),
                                new LogData("joberror", m_jobErrorEx.getJobError().getErrorText()));
                    }

                    return null;
                }

                try {
                    // process job via queue scheduling
                    jobId = jobProcessor.scheduleJob(job, (JobPriority) jobProperties.get(Properties.PROP_PRIORITY), this, jobDescription);

                    if (null != jobId) {
                        try {
                            // waiting, until the job is really executed or processed in any way
                            m_lock.lock();

                            if (m_jobStatus.ordinal() < JOBSTATUS_ORDINAL_RUNNING) {
                                m_startedExecutionCondition.await(
                                    (m_serverManager.getServerConfig().JOB_EXECUTION_TIMEOUT_MILLISECONDS * MAX_QUEUETIME_TO_JOBEXEC_TIME_PERCENTAGE) / 100,
                                    TimeUnit.MILLISECONDS);
                            }
                        } finally {
                            m_lock.unlock();
                        }

                        final long jobStartExecTimeMillis = System.currentTimeMillis();

                        try {
                            // waiting, until the job is finished, but no more than upper time limit for execution (beside watchdog)
                            m_lock.lock();

                            if (m_jobStatus.ordinal() <= JOBSTATUS_ORDINAL_RUNNING) {
                                m_finishedExecutionCondition.await(
                                    (m_serverManager.getServerConfig().JOB_EXECUTION_TIMEOUT_MILLISECONDS * MAX_RUNNING_TO_JOBEXEC_TIME_PERCENTAGE) / 100,
                                    TimeUnit.MILLISECONDS);
                            }
                        } finally {
                            m_lock.unlock();
                        }

                        // If job has been started and is currently still executing,
                        // check the current total exec time and force a kill of the c
                        // current job via the JobProcessor.
                        // In general, the Watchdog should have killed
                        // the job already and the job status should be > RUNNING;
                        // for yet unknown reasons, we don't get notified for some
                        // rare jobs once in a while.
                        // So, this is our last chance to really finish the job the
                        // hard way and cleanup resources accordingly
                        final long jobEndExecTimeMillis = System.currentTimeMillis();

                        if (m_jobStatus.ordinal() <= JOBSTATUS_ORDINAL_RUNNING) {
                            if (ServerManager.isLogWarn()) {
                                ServerManager.logWarn("DC job will be aborted due to final TIMEOUT detection without job being finished up to now",
                                    new LogData("jobid", jobId),
                                    new LogData("hash", hash),
                                    new LogData("exectime", Long.toString(jobEndExecTimeMillis - jobStartExecTimeMillis) + "ms"));
                            }

                            final JobErrorEx notificationErrorEx = new JobErrorEx(JobError.TIMEOUT);

                            jobProcessor.killJob(jobId, notificationErrorEx);
                            m_jobErrorEx = notificationErrorEx;
                        }

                        // error and success handling
                        if (JobStatus.FINISHED == m_jobStatus) {
                            final byte[] resultBuffer = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER);

                            if (null != resultBuffer) {
                                ret = new ByteArrayInputStream(resultBuffer);
                                m_jobErrorEx = new JobErrorEx();
                            } else {
                                m_jobErrorEx = new JobErrorEx(JobError.NO_CONTENT);
                            }
                        }

                        // set final jobError at result properties
                        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(m_jobErrorEx.getErrorCode()));
                        resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, m_jobErrorEx.getErrorData().toString());

                        if (trace && (null != jobDescription)) {
                            ServerManager.logTrace(
                                "DC finished " + jobDescription,
                                new LogData("jobid", jobId),
                                new LogData("hash", hash),
                                new LogData("doctype", (String) jobProperties.get(Properties.PROP_INPUT_TYPE)),
                                new LogData("jobtime", Long.toString(jobEndExecTimeMillis - jobStartTimeMillis) + "ms"),
                                new LogData("queuetime", Long.toString(jobStartExecTimeMillis - jobStartTimeMillis) + "ms"),
                                new LogData("exectime", Long.toString(jobEndExecTimeMillis - jobStartExecTimeMillis) + "ms"));
                        }
                    } else if (trace && jobProcessor.isRunning()) {
                        ServerManager.logTrace("DC job not added to queue => please check ReaderEngine installation!",
                            jobProperties,
                            new LogData("jobid", jobId));
                    }
                } catch (Throwable e) {
                    ExceptionUtils.handleThrowable(e);
                    ServerManager.logExcp(new ConverterException(e));
                } finally {
                    if (null != jobId) {
                        // remove job from JobProcessor
                        jobProcessor.removeJob(jobId);
                    }
                }
            }

            // add jobId to result properties
            resultProperties.put(Properties.PROP_RESULT_JOBID, jobId);

            // Logging
            if (null == ret) {
                if (jobProcessor.isRunning()) {
                    if (cacheOnly) {
                        if (trace) {
                            ServerManager.logTrace(
                                "DC did not find a cached result for a 'cache only' job => returning null as valid result",
                                jobProperties);
                        }
                    } else {
                        final String errorMsg = "DC job convert error";
                        final JobErrorEx jobErrorEx = JobErrorEx.fromResultProperties(resultProperties);
                        final LogData[] logDatas = {
                            new LogData("jobid", jobId),
                            new LogData("hash", hash),
                            new LogData("joberror", jobErrorEx.getJobError().toString())
                        };

                        // special treatment for queue limit related errors => log
                        // on DEBUG level only, not on WARN level since the current
                        // qeue limit status has already been logged as a WARNING
                        if ((JobError.MAX_QUEUE_COUNT == jobErrorEx.getJobError()) || (JobError.QUEUE_TIMEOUT == jobErrorEx.getJobError())) {
                            if (debug) {
                                ServerManager.logDebug(errorMsg, jobProperties, logDatas);
                            }
                        } else {
                            ServerManager.logWarn(errorMsg, jobProperties, logDatas);
                        }
                    }
                }
            } else if (trace) {
                ServerManager.logTrace("DC job converted",
                    new LogData("jobid", jobId),
                    new LogData("hash", hash),
                    new LogData(
                        "mimetype",
                        resultProperties.containsKey(Properties.PROP_RESULT_MIME_TYPE) ? (String) resultProperties.get(Properties.PROP_RESULT_MIME_TYPE) : ""),
                    new LogData(
                        "extension",
                        resultProperties.containsKey(Properties.PROP_RESULT_EXTENSION) ? (String) resultProperties.get(Properties.PROP_RESULT_EXTENSION) : ""),
                    new LogData(
                        "pagecount",
                        resultProperties.containsKey(Properties.PROP_RESULT_PAGE_COUNT) ? ((Integer) resultProperties.get(Properties.PROP_RESULT_PAGE_COUNT)).toString() : "-1"),
                    new LogData(
                        "zip",
                        resultProperties.containsKey(Properties.PROP_RESULT_ZIP_ARCHIVE) ? ((Boolean) resultProperties.get(Properties.PROP_RESULT_ZIP_ARCHIVE)).toString() : "false"),
                    resultProperties.containsKey(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT) ? new LogData(
                        "originalpagecount",
                        ((Integer) resultProperties.get(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT)).toString()) : null);
            }
        } else {
            if (!resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(cachedJobErrorEx.getErrorCode()));
                resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, cachedJobErrorEx.getErrorData().toString());
            }

            final LTStatistics statistics = m_serverManager.getStatistics();

            // update the statistics errorcache hit counter
            if (null != statistics) {
                statistics.incrementProcessedJobCount(cachedJobErrorEx, true);
            }

            if (trace && jobProcessor.isRunning()) {
                ServerManager.logTrace("DC error returned from errorcache",
                    jobProperties,
                    new LogData("hash", hash),
                    new LogData("joberror", cachedJobErrorEx.getJobError().toString()));
            }
        }

        return ret;
    }

    /**
     * @param file
     * @param deleteFile
     * @return
     */
    private void implDeleteFileIfFlagged(@NonNull File file, @NonNull MutableWrapper<Boolean> deleteFile) {
        if (deleteFile.get().booleanValue()) {
            deleteFile.set(Boolean.valueOf(!FileUtils.deleteQuietly(file)));
        }
    }

    // - Static members --------------------------------------------------------

    final private static int JOBSTATUS_ORDINAL_RUNNING = JobStatus.RUNNING.ordinal();

    final private static String GENERIC_JOB = "Generic job";

    final private static String STANDARD_PDF_JOB = "ReaderEngine PDF job";

    final private static String STANDARD_GRAPHIC_JOB = "ReaderEngine Graphic job";

    final private static String PDF_TO_GRAPHIC_JOB = "PDFTool Graphic job";

    final private static String PDF_TO_HTML_JOB = "PDFTool HTML job";

    final private static String STR_PDF = "pdf";

    final private static String STR_PDF_A = "pdfa";

    // - Members ---------------------------------------------------------------

    final private ServerManager m_serverManager;

    final private JobProcessor m_jobProcessor;

    final private IQueueMonitor m_queueMonitor;

    final private ReentrantLock m_lock = new ReentrantLock(true);

    final private Condition m_startedExecutionCondition = m_lock.newCondition();

    final private Condition m_finishedExecutionCondition = m_lock.newCondition();

    volatile private JobStatus m_jobStatus = JobStatus.NEW;

    volatile private JobErrorEx m_jobErrorEx = new JobErrorEx();
}
