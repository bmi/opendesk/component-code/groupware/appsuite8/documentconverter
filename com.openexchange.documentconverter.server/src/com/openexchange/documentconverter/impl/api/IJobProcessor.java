/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

import com.openexchange.documentconverter.JobPriority;

//------------------
//-  IJobProcessor -
//------------------

/**
 * {@link IJobProcessor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public interface IJobProcessor {

    /**
     * Adds a new Job to the processor
     *
     * @param serverJob The server job to be added to the queue
     * @param jobStatusListener The listener associated with the given server job
     * @return The unique id string of the added job or null in case of failure
     */
    String scheduleJob(IJob serverJob, JobPriority jobPriority, IJobStatusListener jobStatusListener, String jobDescription);

    /**
     * Removes the Job with the given id from the queue of queued and already running jobs
     *
     * @param jobId
     */
    boolean removeJob(String jobId);

    /**
     * Removes the Job with the given id from the queue of scheduled jobs
     *
     * @param jobId
     */
    boolean removeScheduledJob(String jobId);

    /**
     * Adds a listener for global JobStatus change events
     *
     * @param jobStatusListener
     */
    void addJobStatusChangeListener(IJobStatusListener jobStatusListener);

    /**
     * Removes a listener for global JobStatus change events
     *
     * @param jobStatusListener
     */
    void removeJobStatusChangeListener(IJobStatusListener jobStatusListener);
}
