/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

import java.util.Objects;

/**
 * {@link MutableLong} Fast counting class that is NOT synchronized at all!
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class MutableLong {

    /**
     * Initializes a new {@link MutableLong}.
     */
    public MutableLong() {
        super();
        m_value = 0;
    }

    /**
     * Initializes a new {@link MutableLong}.
     * @param startValue
     */
    public MutableLong(final long startValue) {
        super();
        m_value = startValue;
    }

    // - Object ----------------------------------------------------------------

    @Override
    public int hashCode() {
        return Objects.hash(m_value);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        return (obj instanceof MutableLong) ?
            (m_value == ((MutableLong) obj).m_value) :
                false;
    }

    // - Public API ------------------------------------------------------------

    /**
     * @return
     */
    public long get() {
        return m_value;
    }

    /**
     * @param addValue
     */
    public void add(final long addValue) {
        m_value += addValue;
    }

    /**
     * @param addValue
     * @return
     */
    public long addAndGet(final long addValue) {
        return (m_value += addValue);
    }

    /**
     * @param adadValue
     * @return
     */
    public long getAndAdd(final long addValue) {
        final long originalValue = m_value;

        m_value += addValue;

        return originalValue;
    }

    /**
     * @param subtractValue
     */
    public void subtract(final long subtractValue) {
        m_value -= subtractValue;
    }

    /**
     * @param subtractValue
     * @return
     */
    public long subtractAndGet(final long subtractValue) {
        return (m_value -= subtractValue);
    }

    /**
     * @param subtractValue
     * @return
     */
    public long getAndSubtract(final long subtractValue) {
        final long originalValue = m_value;

        m_value -= subtractValue;

        return originalValue;
    }

    /**
     *
     */
    public void increment() {
        ++m_value;
    }

    /**
     *
     */
    public long incrementAndGet() {
        return ++m_value;
    }

    /**
     *
     */
    public long getAndIncrement() {
        return m_value++;
    }

    /**
     *
     */
    public void decrement() {
        --m_value;
    }

    /**
     *
     */
    public long decrementAndGet() {
        return --m_value;
    }

    /**
     *
     */
    public long getAndDecrement() {
        return m_value--;
    }

    // - Members ---------------------------------------------------------------

    long m_value;
}
