/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

//---------------
//- BackendType -
//---------------

/**
 * {@link BackendType}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public enum BackendType {

    /**
     * AOO or LIBO
     */
    READERENGINE,

    /**
     * PDFTool
     */
    PDFTOOL;

    /**
     * @return The user friendly name of this enum value
     */
    @Override
    public String toString() {
        return (READERENGINE == this) ?
            "ReaderEngine" :
                "PDFTool";
    }

    // - Public API ------------------------------------------------------------

    /**
     * @param backendType
     * @return The string shortname representation for the given
     *         <code>BackendType</code>
     */
    static public String getShortName(BackendType backendType) {
        String ret = null;

        switch (backendType) {
            case READERENGINE: {
                ret = "re";
                break;
            }

            case PDFTOOL: {
                ret = "pt";
                break;
            }

            default: {
                ret = "no";
                break;
            }
        }

        return ret;
    }
}
