/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

import com.openexchange.documentconverter.JobError;
import com.openexchange.management.MBeanMethodAnnotation;

public interface IDocumentConverterInformationMBean {

    /**
     * @return The total number of jobs that have been processed
     */
    @MBeanMethodAnnotation (description="The total number of jobs that have been processed", parameterDescriptions = {}, parameters = {})
    long getJobsProcessed();

    /**
     * * @return The current count of entries, stored within the remote cache
     */
    @MBeanMethodAnnotation (description="The current count of entries, stored within the remote cache", parameterDescriptions = {}, parameters = {})
    long getCacheEntryCount();

    /**
     * * @return The current count of entries, stored within the persistent local cache
     */
    @MBeanMethodAnnotation (description="The current count of entries, stored within the local persistent cache", parameterDescriptions = {}, parameters = {})
    long getLocalCacheEntryCount();

    /**
     * * @return The current size in bytes of all entries, stored within the persistent local cache directory
     */
    @MBeanMethodAnnotation (description="The current size in bytes of all persistent entries, stored within the remote cache. The size is calculated on the raw sum of bytes of each entry without any padding.", parameterDescriptions = {}, parameters = {})
    long getCachePersistentSize();

    /**
     * * @return The current size in bytes of all entries, stored within the persistent local cache directory
     */
    @MBeanMethodAnnotation (description="The current size in bytes of all persistent entries, stored within the local persistent cache directory. The size is calculated on a volume block size basis to reflect the correct summed up size.", parameterDescriptions = {}, parameters = {})
    long getLocalCachePersistentSize();

    /**
     * * @return The current free size in bytes of the local volume, the cache root directory is located on
     */
    @MBeanMethodAnnotation (description="The current free size in bytes of the local volume, the local cache root directory is located on. The size is calculated on a volume block size basis to reflect the correct size of the free volume.", parameterDescriptions = {}, parameters = {})
    long getCacheFreeVolumeSize();

    /**
     * @return The age of the oldest entry within the remote cache in seconds
     */
    @MBeanMethodAnnotation (description="The age of the oldest entry within remote cache in seconds", parameterDescriptions = {}, parameters = {})
    long getCacheOldestEntrySeconds();

    /**
     * @return The age of the oldest entry within the local cache in seconds
     */
    @MBeanMethodAnnotation (description="The age of the oldest entry within the local cache in seconds", parameterDescriptions = {}, parameters = {})
    long getLocalCacheOldestEntrySeconds();

    /**
     * @return The ratio of cache hits versus processed job as simple quotient
     */
    @MBeanMethodAnnotation (description="The ratio of successful job results that are retrieved from the local cache against the total number of processed jobs as simple quotient", parameterDescriptions = {}, parameters = {})
    double getCacheHitRatio();

    /**
     * @return The ratio of error cache hits versus processed jobs with errors as simple quotient
     */
    @MBeanMethodAnnotation (description="The ratio of job errors that are retrieved from the local errror cache against the total number of job errors as simple quotient", parameterDescriptions = {}, parameters = {})
    double getErrorCacheHitRatio();

    // -------------------------------------------------------------------------

    /**
     * @return The current number of all scheduled, synchronous jobs within the queue, waiting to be processed
     */
    @MBeanMethodAnnotation (description="The current number of all scheduled, synchronous jobs within the queue, waiting to be processed", parameterDescriptions = {}, parameters = {})
    int getScheduledJobCountInQueue();

    /**
     * @return The peak number of jobs in queue for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The peak number of background priority jobs in queue, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    int getPeakJobCountInQueue_Background();

    /**
     * @return The peak number of jobs in queue for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The peak number of low priority jobs in queue, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    int getPeakJobCountInQueue_Low();

    /**
     * @return The peak number of jobs in queue for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The peak number of medium priority jobs in queue, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    int getPeakJobCountInQueue_Medium();

    /**
     * @return The peak number of jobs in queue for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The peak number of high priority jobs in queue, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    int getPeakJobCountInQueue_High();

    /**
     * @return The peak number of jobs in queue for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The peak number of instant priority jobs in queue, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    int getPeakJobCountInQueue_Instant();

    /**
     * @return The peak total number of jobs in queue for measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The peak number of jobs in queue, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    int getPeakJobCountInQueue_Total();

    // -------------------------------------------------------------------------

    /**
     * @return The current number of async jobs scheduled in queue
     */
    @MBeanMethodAnnotation (description="The current number of jobs scheduled in asynchronous queue", parameterDescriptions = {}, parameters = {})
    int getAsyncJobCountScheduled_Total();

    /**
     * @return The summed up number of async jobs processed so far
     */
    @MBeanMethodAnnotation (description="The summed up number of jobs processed from asynchronous queue", parameterDescriptions = {}, parameters = {})
    long getAsyncJobCountProcessed_Total();

    /**
     * @return The summed up number of async jobs dropped so far
     */
    @MBeanMethodAnnotation (description="The summed up number async jobs dropped from asynchronous queue", parameterDescriptions = {}, parameters = {})
    long getAsyncJobCountDropped_Total();

    /**
     * @return The peak total number of async jobs in queue measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The peak number of jobs in the asynchronous queue, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    int getPeakAsyncJobCount_Total();

    // -------------------------------------------------------------------------

    /**
     * @return The toal number of jobs that returned with a timeout error
     */
    @MBeanMethodAnnotation (description="The total number of jobs, that returned with a timeout error", parameterDescriptions = {}, parameters = {})
    long getJobErrorsTimeout();

    /**
     * @return The total number of jobs that returned with an error in general
     */
    @MBeanMethodAnnotation (description="The total number of jobs, that returned with an error", parameterDescriptions = {}, parameters = {})
    long getJobErrorsTotal();


    // -------------------------------------------------------------------------

    /**
     * @return The total number of physical conversions
     */
    @MBeanMethodAnnotation (description="The total number of all physical ReaderEngine and PDFTool conversions", parameterDescriptions = {}, parameters = {})
    long getConversionCount();

    /**
     * @return The total number of physical conversions
     */
    @MBeanMethodAnnotation (description="The total number of all physical ReaderEngine conversions", parameterDescriptions = {}, parameters = {})
    long getConversionCountReaderEngine();

    /**
     * @return The total number of physical conversions
     */
    @MBeanMethodAnnotation (description="The total number of all physical PDFTool conversions", parameterDescriptions = {}, parameters = {})
    long getConversionCountPDFTool();

    /**
     * @return The total number of conversions that used more than one run to perform a conversion
     */
    @MBeanMethodAnnotation (description="The total number of physical ReaderEngine conversions, that needed a second, new ReaderEngine instance", parameterDescriptions = {}, parameters = {})
    long getConversionCountWithMultipleRuns();

    /**
     * @return The total number of conversions that returned with a {@link JobError.GENERAL}
     */
    @MBeanMethodAnnotation (description="The total number of physical ReaderEngine conversions, that resulted in a general error", parameterDescriptions = {}, parameters = {})
    long getConversionErrorsGeneral();

    /**
     * @return The total number of conversions that returned with a {@link JobError.TIMEOUT}
     */
    @MBeanMethodAnnotation (description="The total number of physical ReaderEngine conversions, that resulted in a tiemout error", parameterDescriptions = {}, parameters = {})
    long getConversionErrorsTimeout();

    /**
     * @return The total number of conversions that returned with a {@link JobError.DISPOSED}
     */
    @MBeanMethodAnnotation (description="The total number of physical ReaderEngine conversions, that resulted in a disposed error (ReaderEngine instance vanished during conversion)", parameterDescriptions = {}, parameters = {})
    long getConversionErrorsDisposed();

    /**
     * @return The total number of conversions that returned with a {@link JobError.PASSWORD}
     */
    @MBeanMethodAnnotation (description="The total number of physical ReaderEngine conversions, that resulted in a password error", parameterDescriptions = {}, parameters = {})
    long getConversionErrorsPassword();

    /**
     * @return The total number of conversions that returned with a {@link JobError.NO_CONTENT}
     */
    @MBeanMethodAnnotation (description="The total number of physical ReaderEngine conversions, that resulted in a no content error (e.g. source document of 0 bytes length)", parameterDescriptions = {}, parameters = {})
    long getConversionErrorsNoContent();

    /**
     * @return The total number of conversions that returned with a {@link JobError.OUT_OF_MEMORY}
     */
    @MBeanMethodAnnotation (description="The total number of physical ReaderEngine conversions, that resulted in an out of memory error", parameterDescriptions = {}, parameters = {})
    long getConversionErrorsOutOfMemory();

    /**
     * @return The total number of conversions that returned with a {@link JobError.OUT_OF_MEMORY}
     */
    @MBeanMethodAnnotation (description="The total number of physical PDFTool conversions, that resulted in an error", parameterDescriptions = {}, parameters = {})
    long getConversionErrorsPDFTool();

    // -------------------------------------------------------------------------

    /**
     * @return The median of the jobs time in queue for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a background job waited in the queue for processing, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianQueueTimeMillis_Background();

    /**
     * @return The median of the jobs time in queue for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a low priority job waited in the queue for processing, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianQueueTimeMillis_Low();

    /**
     * @return The median of the jobs time in queue for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a medium priority job waited in the queue for processing, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianQueueTimeMillis_Medium();

    /**
     * @return The median of the jobs time in queue for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a high priority job waited in the queue for processing, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianQueueTimeMillis_High();

    /**
     * @return The median of the jobs time in queue for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, an instant priority job waited in the queue for processing, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianQueueTimeMillis_Instant();

    /**
     * @return The median of the jobs time in queue for all jobs measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a job waited in the queue for processing, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianQueueTimeMillis_Total();

    // -------------------------------------------------------------------------

    /**
     * @return The median of the jobs conversion time for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a physical ReaderEngine conversion with background priority lasted, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianConversionTimeMillis_Background();

    /**
     * @return The median of the jobs conversion time for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a physical ReaderEngine conversion with low priority lasted, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianConversionTimeMillis_Low();

    /**
     * @return The median of the jobs conversion time for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a physical ReaderEngine conversion with medium priority lasted, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianConversionTimeMillis_Medium();

    /**
     * @return The median of the jobs conversion time for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a physical ReaderEngine conversion with high priority lasted, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianConversionTimeMillis_High();

    /**
     * @return The median of the jobs conversion time for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a physical ReaderEngine conversion with instant priority lasted, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianConversionTimeMillis_Instant();

    /**
     * @return The median of the jobs conversion time for all jobs measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a physical ReaderEngine conversion lasted, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianConversionTimeMillis_Total();

    // -------------------------------------------------------------------------

    /**
     * @return The median of the jobs total time for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a job with background priority needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianJobTimeMillis_Background();

    /**
     * @return The median of the jobs total time for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a job with low priority needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianJobTimeMillis_Low();

    /**
     * @return The median of the jobs total time for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a job with medium priority needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianJobTimeMillis_Medium();

    /**
     * @return The median of the jobs total time for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a job with high priority needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianJobTimeMillis_High();

    /**
     * @return The median of the jobs total time for the given priority measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a job with instant priority needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianJobTimeMillis_Instant();

    /**
     * @return The median of the jobs total time for all jobs measured from the beginning of the last reset/initialization on
     */
    @MBeanMethodAnnotation (description="The median time in milliseconds, a job needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes)", parameterDescriptions = {}, parameters = {})
    long getMedianJobTimeMillis_Total();

    /**
     * @return The number of stateful job conversions for that no endConvert has been called up to now
     */
    @MBeanMethodAnnotation (description="The number of stateful job conversions for that no endConvert has been called up to now", parameterDescriptions = {}, parameters = {})
    long getPendingStatefulJobCount();
}