/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import com.openexchange.documentconverter.AsyncExecutor;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.LogData;


/**
 * {@link AsyncServerExecutor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.3
 */
public class AsyncServerExecutor extends AsyncExecutor {

    final private static long LOG_CONNECTION_STATUS_PERIOD_MILLIS = 60 * 1000L;

    final private static String LOG_CONNECTION_STATUS_PERIOD_SECONDS_STR = Long.toString(LOG_CONNECTION_STATUS_PERIOD_MILLIS / 1000L) + "s";

    /**
     * Initializes a new {@link AsyncServerExecutor}.
     * @param converter
     * @param manager
     * @param maxThreadCount
     */
    public AsyncServerExecutor(IDocumentConverter converter, ServerManager manager, int maxThreadCount, int asyncQueueLength, Statistics statistics) {
        super(converter, manager, maxThreadCount, asyncQueueLength);

        m_statistics = statistics;

        m_logTimerExecutor.scheduleWithFixedDelay(() -> implLogAsyncDroppedStatus(),
            LOG_CONNECTION_STATUS_PERIOD_MILLIS, LOG_CONNECTION_STATUS_PERIOD_MILLIS, TimeUnit.MILLISECONDS);
    }

    // - ICounter --------------------------------------------------------------

    @Override
    public void increment() {
        super.increment();

        if (null != m_statistics) {
            m_statistics.incrementAsyncQueueCount();
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.ICounter#decrement()
     */
    @Override
    public void decrement() {
        super.decrement();

        if (null != m_statistics) {
            m_statistics.decrementAsyncQueueCount();
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     *
     */
    private synchronized void implLogAsyncDroppedStatus() {
        final long droppedAsyncTotal = getDroppedCount();
        final long droppedAsyncPeriod = droppedAsyncTotal - m_lastDroppedAsyncCount;

        if (droppedAsyncPeriod > 0) {
            ServerManager.logWarn("DC JobMonitor dropped asynchronous jobs during last observation period of " + LOG_CONNECTION_STATUS_PERIOD_SECONDS_STR,
                new LogData("dropped_total", Long.toString(droppedAsyncTotal)),
                new LogData("dropped_period", Long.toString(droppedAsyncPeriod)));
        }

        m_lastDroppedAsyncCount = droppedAsyncTotal;
    }

    // - Members ---------------------------------------------------------------

    final private ScheduledExecutorService m_logTimerExecutor = Executors.newScheduledThreadPool(1);

    private Statistics m_statistics = null;

    private long m_lastDroppedAsyncCount = 0;
}
