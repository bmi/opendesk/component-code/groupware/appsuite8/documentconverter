/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.zip.ZipFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.util.ByteArrayBuffer;
import org.mozilla.universalchardet.Constants;
import org.mozilla.universalchardet.UniversalDetector;
import com.google.common.base.Throwables;
import com.openexchange.documentconverter.DocumentInformation;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.api.ConverterStatus;
import com.openexchange.documentconverter.impl.api.IJob;
import com.openexchange.exception.ExceptionUtils;
import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.XNameContainer;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XModel;
import com.sun.star.lang.DisposedException;
import com.sun.star.lang.XComponent;
import com.sun.star.style.XStyleFamiliesSupplier;
import com.sun.star.task.PDFExportException;
import com.sun.star.task.XInteractionContinuation;
import com.sun.star.task.XInteractionHandler;
import com.sun.star.task.XInteractionPassword;
import com.sun.star.task.XInteractionPassword2;
import com.sun.star.ucb.XInteractionSupplyAuthentication;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.util.XCloseable;

//----------------------
//- class ConverterJob -
//----------------------

/**
 * {@link ConverterJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
abstract public class ConverterJob implements IJob {

    final private Charset CHARSET_UTF8 = Charset.forName("UTF-8");

    final private String INPUT_TYPE_TXT = "txt";

    final private String INPUT_TYPE_TEXT = "text";

    final private int TEXT_DECODER_BUFFER_SIZE = 4096;

    final public static String ALL_PAGES = "1-99999";

    /**
     * {@link JobInteractionHandler}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @param <XInteractionRequest>
     */
    private static class ConverterJobInteractionHandler<XInteractionRequest> implements XInteractionHandler {

        /**
         * Initializes a new {@link JobInteractionHandler}.
         */
        ConverterJobInteractionHandler(ConverterJob converterJob) {
            super();
            m_converterJob = converterJob;
        }

        /* (non-Javadoc)
         * @see com.sun.star.task.XInteractionHandler#handle(com.sun.star.task.XInteractionRequest)
         */
        @Override
        public void handle(com.sun.star.task.XInteractionRequest xRequest) {
            if ((null != xRequest) && (null != m_converterJob)) {
                final Object interactionRequest = xRequest.getRequest();
                final XInteractionContinuation[] iaContinuations = xRequest.getContinuations();

                if (interactionRequest instanceof PDFExportException) {
                    final int[] errorCodes = ((PDFExportException) interactionRequest).ErrorCodes;

                    if (null != errorCodes) {
                        for (final int errorCode : errorCodes) {
                            if (PDF_ERRORCODE_NO_DOCUMENT_CONTENT == errorCode) {
                                m_converterJob.setJobErrorEx(new JobErrorEx(JobError.NO_CONTENT));
                            }
                        }
                    }
                } else if (null != iaContinuations) {
                    for (int i = 0; i < iaContinuations.length; ++i) {
                        final XInteractionContinuation curContinuation = iaContinuations[i];

                        if (null != curContinuation) {
                            final XInteractionPassword xPassword = UnoRuntime.queryInterface(XInteractionPassword.class, curContinuation);
                            final XInteractionPassword2 xPassword2 = UnoRuntime.queryInterface(XInteractionPassword2.class, curContinuation);
                            final XInteractionSupplyAuthentication xSupplyAuth = UnoRuntime.queryInterface(XInteractionSupplyAuthentication.class, curContinuation);

                            if ((null != xPassword) || (null != xPassword2) || (null != xSupplyAuth)) {
                                m_converterJob.setJobErrorEx(new JobErrorEx(JobError.PASSWORD));
                            }
                        }
                    }
                }
            }
        }

        // - Members ---------------------------------------------------------------

        private static final int PDF_ERRORCODE_NO_DOCUMENT_CONTENT = 0x01000001;

        private ConverterJob m_converterJob = null;
    }

    /**
     * Initializes a new {@link ConverterJob}.
     *
     * @param aInputStream
     */
    ConverterJob(HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        super();

        m_jobProperties = (null == jobProperties) ? new HashMap<>(12) : jobProperties;
        m_resultProperties = (null == resultProperties) ? new HashMap<>(8) : resultProperties;
    }

    // - IJob ------------------------------------------------------------------

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.ICachable#getHash()
     */
    @Override
    public String getHash() {
        return m_cacheHash;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJob#addJobProperties(java.util.HashMap )
     */
    @Override
    public void addJobProperties(HashMap<String, Object> additionalJobProperties) {
        m_jobProperties.putAll(additionalJobProperties);
        applyJobProperties();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJob#getJobProperties()
     */
    @Override
    public HashMap<String, Object> getJobProperties() {
        return m_jobProperties;
    }

    /**
     * @return The current job properties
     */
    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJob#getJobResultProperties()
     */
    @Override
    public HashMap<String, Object> getResultProperties() {
        return m_resultProperties;
    }

    /**
     * @return
     * @throws DisposedException
     * @throws InterruptedExceptionosl_createTempFile+0x29a
     * @throws IOException
     * @throws MalformedURLException
     * @throws Exception
     */
    @Override
    public ConverterStatus execute(Object jobExecutionData) throws InterruptedException, IOException, MalformedURLException, Exception {
        ConverterStatus ret = ConverterStatus.ERROR;

        m_resultJobErrorEx = new JobErrorEx();

        if (beginExecute(jobExecutionData)) {
            doExecute(jobExecutionData);
        }

        ret = endExecute();

        return ret;
    }

    @Override
    public void kill() {
        // default implementation does nothing
        if (ServerManager.isLogTrace()) {
            ServerManager.logTrace("DC ConverterJob#kill called but implementation missing");
        }
    }

    // - instance methods ------------------------------------------------------

    /**
     * @return
     */
    @SuppressWarnings("static-method")
    ConverterMeasurement getMeasurementType() {
        return ConverterMeasurement.GENERIC_CONVERSION;
    }

    /**
     * @param jobExecutionData TODO
     * @param instance
     * @return
     * @throws com.sun.star.lang.DisposedException
     * @throws Exception
     * @throws IOException
     */
    protected boolean beginExecute(Object jobExecutionData) throws com.sun.star.lang.DisposedException, Exception, IOException {
        m_resultPageCount = -1;
        m_originalPageCount = -1;
        m_resultMimeType = "";
        m_resultExtension = "";
        m_resultZipArchive = false;
        m_pixelWidth = -1;
        m_pixelHeight = -1;

        applyJobProperties();

        if ((null == m_inputFile) || !m_inputFile.canRead() || (null == m_outputFile)) {
            m_resultJobErrorEx = new JobErrorEx(JobError.GENERAL);
        }

        return m_resultJobErrorEx.hasNoError();
    }

    /**
     * @param jobExecutionData TODO
     * @param instance
     * @return
     * @throws com.sun.star.lang.DisposedException
     * @throws InterruptedException
     * @throws IOException
     * @throws MalformedURLException
     * @throws Exception
     */
    protected abstract ConverterStatus doExecute(Object jobExecutionData) throws com.sun.star.lang.DisposedException, InterruptedException, IOException, MalformedURLException, Exception;

    /**
     * @param jobExecutionData TODO
     * @param instance
     * @return
     * @throws com.sun.star.lang.DisposedException
     * @throws IOException
     * @throws MalformedURLException
     * @throws Exception
     */
    protected ConverterStatus endExecute() throws com.sun.star.lang.DisposedException, IOException, MalformedURLException, Exception {
        if (m_resultJobErrorEx.hasNoError() && (m_outputFile.length() > 0) && !m_resultProperties.containsKey(Properties.PROP_RESULT_BUFFER)) {
            if (null == m_resultProperties.get(Properties.PROP_RESULT_BUFFER)) {
                m_resultProperties.put(Properties.PROP_RESULT_BUFFER, FileUtils.readFileToByteArray(m_outputFile));
            }

            // multiple result pages are stored within a zip archive =>
            // if page count has not been set previously, calculate it
            // from the number of contained zip entries;
            if ((m_resultPageCount <= 0) && m_resultZipArchive) {
                try (final ZipFile zipFile = new ZipFile(m_outputFile)) {
                    m_resultPageCount = zipFile.size();
                }
            }
        }

        // set result parameters at job result property set
        m_resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(m_resultJobErrorEx.getErrorCode()));
        m_resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, m_resultJobErrorEx.getErrorData().toString());

        if (m_resultJobErrorEx.hasNoError()) {
            if (null != m_cacheHash) {
                m_resultProperties.put(Properties.PROP_RESULT_CACHE_HASH, m_cacheHash);
            }

            if (null != m_inputFileHash) {
                m_resultProperties.put(Properties.PROP_RESULT_INPUTFILE_HASH, m_inputFileHash);
            }

            if (null != m_locale) {
                m_resultProperties.put(Properties.PROP_RESULT_LOCALE, m_locale);
            }

            m_resultProperties.put(Properties.PROP_RESULT_PAGE_COUNT, Integer.valueOf(m_resultPageCount));

            // in case of a possible single page conversion, set the
            // 'OriginalPageCount' result property
            if (m_originalPageCount > 1) {
                m_resultProperties.put(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT, Integer.valueOf(m_originalPageCount));
            }

            if (null != m_resultMimeType) {
                m_resultProperties.put(Properties.PROP_RESULT_MIME_TYPE, m_resultMimeType);
            } else {
                m_resultProperties.remove(Properties.PROP_RESULT_MIME_TYPE);
            }

            if (null != m_resultExtension) {
                m_resultProperties.put(Properties.PROP_RESULT_EXTENSION, m_resultExtension);
            } else {
                m_resultProperties.remove(Properties.PROP_RESULT_EXTENSION);
            }

            m_resultProperties.put(Properties.PROP_RESULT_ZIP_ARCHIVE, Boolean.valueOf(m_resultZipArchive));
        }

        return m_resultJobErrorEx.hasNoError() ? ConverterStatus.OK : ConverterStatus.ERROR;
    }

    /**
     * @param instance
     * @param type
     * @param PageCountObj [output] If set, this output parameter will contain the number of pages for loaded document. May be null.
     * @return
     */
    protected XComponent loadComponent(@NonNull REInstance instance, String pageRange) throws com.sun.star.lang.DisposedException, Exception {
        final boolean trace = ServerManager.isLogTrace();
        XComponent ret = null;

        // always read document from a local (temp) file URL, since LIBO
        // doesn't handle loadComponentFromURL very well due to
        // reading just 4 Byte chunks per each InputStream read call;
        // AOO doesn't have such problems at all, but for consistency reasons,
        // this behaviour is valid for all types of backend

        if ((null != m_inputFile) && (null != instance) && instance.isConnected()) {
            implEnsureUTF8TextInputFile();


            final String inputFileUrlString = ServerManager.getAdjustedFileURL(m_inputFile);
            final PropertyValue mediaDesc[][] = { getMediaDescriptor(inputFileUrlString, pageRange) };
            final XComponentLoader loader = UnoRuntime.queryInterface(XComponentLoader.class, instance.getDesktop());

            long loadStartTime = 0;

            if (trace) {
                loadStartTime = System.currentTimeMillis();
                ServerManager.logTrace("DC ReaderEngine started loading source document");
            }

            if (FileUtils.sizeOf(m_inputFile) < 1) {
                setJobErrorEx(new JobErrorEx(JobError.NO_CONTENT));
            }

            if (m_resultJobErrorEx.hasNoError() && (null != loader)) {
                ret = loader.loadComponentFromURL(inputFileUrlString, "_blank", 0, mediaDesc[0]);

                if (trace) {
                    ServerManager.logTrace("DC ReaderEngine finished loading source document: " + (System.currentTimeMillis() - loadStartTime) + "ms");
                }

                if (m_resultJobErrorEx.hasError()) {
                    ret = null;
                } else {
                    final XModel xModel = (null != ret) ? UnoRuntime.queryInterface(XModel.class, ret) : null;

                    if (xModel != null) {
                        final PropertyValue[] mediaDescriptor = xModel.getArgs();

                        if (mediaDescriptor != null) {
                            for (final PropertyValue mediaValue : mediaDescriptor) {
                                if ("FilterData".equals(mediaValue.Name)) {
                                    final PropertyValue[] filterDataValues = (PropertyValue[]) mediaValue.Value;
                                    for (final PropertyValue filterDataValue : filterDataValues) {
                                        if (filterDataValue.Name.equals(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT)) {
                                            m_originalPageCount = ((Integer) filterDataValue.Value).intValue();
                                        }
                                    }
                                }
                            }
                        }

                        doXModelSetup(xModel);
                    }
                }
            } else if (trace) {
                ServerManager.logTrace("DC ReaderEngine error loading source document: " + (System.currentTimeMillis() - loadStartTime) + "ms");
            }
        }

        return ret;
    }

    /**
     * @param inputFileUrlString
     * @param pageRange
     * @return The properly filled media descriptor
     */
    protected PropertyValue[] getMediaDescriptor(String inputFileUrlString, String pageRange) {
        int propertyCount = 4;
        int curPropertyPos = 0;
        String filterName = null;

        // add page range, if necessary
        final boolean pageRangeUsed = (pageRange != null) && (pageRange.length() > 0) && !pageRange.equals(ALL_PAGES);

        if (pageRangeUsed) {
            ++propertyCount;
        }

        // check, if a special filter needs to be set
        if (null != m_inputType) {
            DocumentInformation documentInformation= DocumentInformation.createFromExtension(m_inputType);

            if (null != documentInformation) {
                filterName = documentInformation.getFilterName();
                ++propertyCount;
            }

            if ("csv".equals(m_inputType)) {
                m_isCSVImport = true;
            }
        }

        // setup media descriptor
        final PropertyValue[] mediaDesc = new PropertyValue[propertyCount];

        // URL
        mediaDesc[curPropertyPos] = new PropertyValue();
        mediaDesc[curPropertyPos].Name = "URL";
        mediaDesc[curPropertyPos++].Value = inputFileUrlString;

        // InteractionHandler
        mediaDesc[curPropertyPos] = new PropertyValue();
        mediaDesc[curPropertyPos].Name = "InteractionHandler";
        mediaDesc[curPropertyPos++].Value = new ConverterJobInteractionHandler<>(this);

        // Hidden
        mediaDesc[curPropertyPos] = new PropertyValue();
        mediaDesc[curPropertyPos].Name = "Hidden";
        mediaDesc[curPropertyPos++].Value = Boolean.TRUE;

        // UserFileName
        mediaDesc[curPropertyPos] = new PropertyValue();
        mediaDesc[curPropertyPos].Name = "UserFileName";
        mediaDesc[curPropertyPos++].Value = implGetUserFileURL();

        // PageRange
        if (pageRangeUsed) {
            final PropertyValue[] filterData = new PropertyValue[1];
            filterData[0] = new PropertyValue();
            filterData[0].Name = "PageRange";
            filterData[0].Value = pageRange;

            // FilterData
            mediaDesc[curPropertyPos] = new PropertyValue();
            mediaDesc[curPropertyPos].Name = "FilterData";
            mediaDesc[curPropertyPos++].Value = filterData;
        }

        // FilterName
        if (null != filterName) {
            mediaDesc[curPropertyPos] = new PropertyValue();
            mediaDesc[curPropertyPos].Name = "FilterName";
            mediaDesc[curPropertyPos++].Value = filterName;
        }

        return mediaDesc;
    }

    /**
     * @param xModel
     */
    protected void doXModelSetup(@NonNull XModel xModel) throws DisposedException {
        final boolean trace = ServerManager.isLogTrace();
        final long setupStartTime = System.currentTimeMillis();

        if (trace) {
            ServerManager.logTrace("DC ReaderEngine started setting up model");
        }

        try {
            if (m_hideChanges || m_hideComments) {
                final XPropertySet xPropSet = UnoRuntime.queryInterface(XPropertySet.class, xModel);

                if (null != xPropSet) {
                    if (xPropSet.getPropertySetInfo().hasPropertyByName("RedlineDisplayType")) {
                        xPropSet.setPropertyValue("RedlineDisplayType", Short.valueOf((short) 0));
                    }
                }
            }

            if (m_isCSVImport) {
                // set HeaderIsOn/HeaderOn to false for CSV imports
                final XNameContainer xPageStyles = UnoRuntime.queryInterface(XNameContainer.class, UnoRuntime.queryInterface(XStyleFamiliesSupplier.class, xModel).getStyleFamilies().getByName("PageStyles"));

                if ((null != xPageStyles) && xPageStyles.hasByName("Default")) {
                    final XPropertySet xPagePropSet = UnoRuntime.queryInterface(XPropertySet.class, xPageStyles.getByName("Default"));

                    if (null != xPagePropSet) {
                        if (xPagePropSet.getPropertySetInfo().hasPropertyByName("HeaderIsOn")) {
                            xPagePropSet.setPropertyValue("HeaderIsOn", Boolean.FALSE);
                        }

                        if (xPagePropSet.getPropertySetInfo().hasPropertyByName("HeaderOn")) {
                            xPagePropSet.setPropertyValue("HeaderOn", Boolean.FALSE);
                        }
                    }
                }
            }
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            throw new DisposedException(e);
        }

        if (trace) {
            ServerManager.logTrace("DC ReaderEngine finished setting up model: " + (System.currentTimeMillis() - setupStartTime) + "ms");
        }
    }

    /**
     * @param component
     * @throws com.sun.star.lang.DisposedException
     * @throws Exception
     */
    protected void closeComponent(XComponent component) throws com.sun.star.lang.DisposedException, Exception {
        final boolean trace = ServerManager.isLogTrace();

        if (m_closeDocument) {
            long closeStartTime = 0;

            if (trace) {
                closeStartTime = System.currentTimeMillis();
                ServerManager.logTrace("DC ReaderEngine started closing document");
            }

            if (null != component) {
                final XCloseable closeable = UnoRuntime.queryInterface(XCloseable.class, component);

                if (null != closeable) {
                    closeable.close(true);
                }
            }

            if (trace) {
                ServerManager.logTrace("DC ReaderEngine finished closing document: " + (System.currentTimeMillis() - closeStartTime) + "ms");
            }
        } else if (trace) {
            ServerManager.logTrace("DC ReaderEngine is not closing document (as requested)");
        }
    }

    /**
     * @return The hash value for the input file
     */
    protected StringBuilder getInputFileHashBuilder() {
        StringBuilder ret = null;

        if (null == m_inputFileHash) {
            ret = ServerManager.getFileHashBuilder(m_inputFile, m_inputURL, m_locale);

            if (null != ret) {
                m_inputFileHash = ret.toString();
            }
        } else {
            ret = new StringBuilder(256).append(m_inputFileHash);
        }

        return ret;
    }

    /**
     * @param hashBuilder
     */
    protected void setHash(@NonNull StringBuilder hashBuilder) {
        m_cacheHash = hashBuilder.toString();
    }

    /**
     *
     */
    protected void applyJobProperties() {
        Object obj = null;

        if (null != (obj = m_jobProperties.get(Properties.PROP_CACHE_HASH))) {
            m_cacheHash = (String) obj;
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_INPUTFILE_HASH))) {
            m_inputFileHash = (String) obj;
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_LOCALE))) {
            m_locale = (String) obj;
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_INPUT_FILE))) {
            m_inputFile = (File) obj;
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_INPUT_TYPE))) {
            m_inputType = ((String) obj).toLowerCase();
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_INPUT_URL))) {
            m_inputURL = (String) obj;
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_OUTPUT_FILE))) {
            m_outputFile = (File) obj;
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_ZIP_ARCHIVE))) {
            m_resultZipArchive = ((Boolean) obj).booleanValue();
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_PIXEL_WIDTH))) {
            m_pixelWidth = ((Integer) obj).intValue();
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_PIXEL_HEIGHT))) {
            m_pixelHeight = ((Integer) obj).intValue();
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_PAGE_RANGE))) {
            m_pageRange = (String) obj;
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_HIDE_CHANGES))) {
            m_hideChanges = ((Boolean) obj).booleanValue();
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_HIDE_COMMENTS))) {
            m_hideComments = ((Boolean) obj).booleanValue();
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_CLOSE_DOCUMENT))) {
            m_closeDocument = ((Boolean) obj).booleanValue();
        }
    }

    /**
     *
     */
    protected void setJobErrorEx(@NonNull JobErrorEx jobErrorEx) {
        if ((JobError.PASSWORD == jobErrorEx.getJobError()) && (null != m_inputType) && m_inputType.endsWith(Properties.OX_RESCUEDOCUMENT_EXTENSION_APPENDIX)) {
            m_resultJobErrorEx = new JobErrorEx(JobError.GENERAL);
        } else {
            m_resultJobErrorEx = jobErrorEx;
        }

        m_resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(m_resultJobErrorEx.getErrorCode()));
        m_resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, m_resultJobErrorEx.getErrorData().toString());
    }

    /**
     * @return The user file name as URI, constructed with file://localhost/${INFO_FILENAME} schema.
     *  If the file name ${INFO_FILENAME} is not given, the default file name 'unknown.document' is used instead.
     */
    private String implGetUserFileURL() {
        // try to use 'InputUrl' property first
        String userFileURL = m_inputURL;

        // if no 'InputUrl' property is set, try to use
        // the info filename property to construct the URL
        if (StringUtils.isBlank(userFileURL)) {
            final String fileName = (String) m_jobProperties.get(Properties.PROP_INFO_FILENAME);

            // if none user filename is set up to now, create a default one
            if (StringUtils.isBlank(fileName)) {
                userFileURL = "unknown." + (StringUtils.isBlank(m_inputType) ? "document" : m_inputType);
            }

            userFileURL = "file:///" + fileName;
        }

        return userFileURL;
    }

    private void implEnsureUTF8TextInputFile() {
        if (INPUT_TYPE_TXT.equalsIgnoreCase(m_inputType) ||
            INPUT_TYPE_TEXT.equalsIgnoreCase(m_inputType)) {

            final ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(TEXT_DECODER_BUFFER_SIZE);
            final UniversalDetector charsetDetector = new UniversalDetector(null);
            String detectedCharset = null;

            try (final InputStream inputStm = FileUtils.openInputStream(m_inputFile)) {
                final byte[] readBuffer = new byte[TEXT_DECODER_BUFFER_SIZE];

                for (int curReadCount = 0; 0 < (curReadCount = inputStm.read(readBuffer)) && !charsetDetector.isDone();) {
                    charsetDetector.handleData(readBuffer, 0, curReadCount);
                    byteArrayBuffer.append(readBuffer, 0, curReadCount);
                }
            } catch (Throwable e) {
                ServerManager.logTrace("DC server not able to detect charset from text file, assuming UTF-8: " +
                    Throwables.getRootCause(e).getMessage());
            } finally {
                charsetDetector.dataEnd();
                detectedCharset = charsetDetector.getDetectedCharset();
            }

            if ((null != detectedCharset) && (Constants.CHARSET_UTF_8 != detectedCharset)) {
                try {
                    final Charset sourceCharSet = Charset.forName(detectedCharset);

                    if (null != sourceCharSet) {
                        // convert byte array with detected source file encoding into an
                        // UTF-8 encoded byte array to be used by ReaderEngine in every case
                        FileUtils.writeByteArrayToFile(m_inputFile, new String(byteArrayBuffer.toByteArray(), sourceCharSet).getBytes(CHARSET_UTF8));
                    }
                } catch (IOException e) {
                    ServerManager.logTrace("DC server not able to write UTF-8 converted source file: " +
                        Throwables.getRootCause(e).getMessage());
                }
            }
        }
    }

    // - Members ---------------------------------------------------------------

    protected HashMap<String, Object> m_jobProperties = null;

    protected HashMap<String, Object> m_resultProperties = null;

    protected String m_cacheHash = null;

    protected String m_inputFileHash = null;

    protected String m_locale = null;

    protected File m_inputFile = null;

    protected String m_inputType = null;

    protected String m_inputURL = null;

    protected File m_outputFile = null;

    protected JobErrorEx m_resultJobErrorEx = new JobErrorEx();

    protected String m_resultMimeType = "";

    protected String m_resultExtension = "";

    protected int m_resultPageCount = -1;

    protected int m_originalPageCount = -1;

    protected int m_pixelWidth = -1;

    protected int m_pixelHeight = -1;

    protected boolean m_resultZipArchive = false;

    protected String m_pageRange = ALL_PAGES;

    protected boolean m_hideChanges = true;

    protected boolean m_hideComments = true;

    protected boolean m_isCSVImport = false;

    protected boolean m_closeDocument = true;
}
