/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.impl.api.JobEntry;
import com.openexchange.documentconverter.impl.api.JobStatus;

/**
 * {@link JobEntryQueue}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.6.0
 */
final class JobEntryQueue {

    /**
     * Initializes a new {@link JobEntryQueue}.
     */
    JobEntryQueue(@NonNull JobProcessor jobProcessor) {
        super();

        m_jobProcessor = jobProcessor;
    }

    // - Public interface ------------------------------------------------------

    /**
    *
    */
    public void terminate() {
        if (m_isRunning.compareAndSet(true, false)) {
            try {
                m_lock.lock();
                m_notEmptyCondition.signal();
            } finally {
                m_lock.unlock();
            }
        }
    }

    /**
     * @return
     */
    public boolean isRunning() {
        return m_isRunning.get();
    }

    /**
     * @return
     */
    public int size() {
        try {
            m_lock.lock();
            return (m_scheduled.size() + m_processing.size());
        } finally {
            m_lock.unlock();
        }
    }

    /**
     * @param entry The job to be scheduled for execution
     * @return
     */
    public void schedule(@NonNull JobEntry jobEntry) {
        if (isRunning()) {
            final JobPriority jobPriority = jobEntry.getJobPriority();

            try {
                m_lock.lock();

                boolean entryAdded = false;

                // entries with lowest priority will always be added to end of list,
                // other entries need to be inserted at correct, priority based position
                if (JobPriority.lowest() != jobPriority) {
                    final ListIterator<JobEntry> listIter = m_scheduled.listIterator();

                    // insert entry at priority based position
                    while (listIter.hasNext()) {
                        final JobEntry curEntry = listIter.next();

                        // if given jobPriority is higher than current jobPriority,
                        // insert given entry just before the current entry. This
                        // ensures, that entries are always added in a priority based
                        // order, beginning from highest order to lowest order and FIFO
                        if (jobPriority.compareTo(curEntry.getJobPriority()) > 0) {
                            listIter.previous();
                            listIter.add(jobEntry);
                            entryAdded = true;
                            break;
                        }
                    }
                }

                // add entry to end of list, if it was not already added in a prioritized way before
                if (!entryAdded) {
                    m_scheduled.add(jobEntry);
                }
            } finally {
                m_lock.unlock();
            }

            // status update SCHEDULED needs to be called out of sync block
            m_jobProcessor.updateServerJobStatus(jobEntry, JobStatus.SCHEDULED);

            // signal notEmptyCondition for threads, waiting for an entry to become available
            try {
                m_lock.lock();
                m_notEmptyCondition.signal();
            } finally {
                m_lock.unlock();
            }

        }
    }

    /**
     * @param jobId
     * @return
     */
    public JobEntry get(@NonNull final String jobId) {
        try {
            m_lock.lock();
            return implFind(other -> jobId.compareTo(other.getJobId()), m_scheduled, m_processing);
        } finally {
            m_lock.unlock();
        }
    }

    /**
     * @param jobId
     * @return
     */
    public JobEntry remove(@NonNull String jobId, boolean scheduledOnly) {
        JobEntry ret = null;

        try {
            m_lock.lock();

            // check jobEntry process list first, since we can
            // assume, that most of all entries are removed, when they
            // will have reached the processing state and this
            // #remove method is eventually called after processing
            ret = scheduledOnly ?
                implRemove(jobId, m_scheduled) :
                    implRemove(jobId, m_processing, m_scheduled);
        } finally {
            m_lock.unlock();
        }

        // perform status update for each entry out of sync block, since
        // entries might be removed from their original lists during the update
        if (null != ret) {
            m_jobProcessor.updateServerJobStatus(ret, JobStatus.REMOVED);
        }

        return ret;
    }


    /**
     * @param jobEntry
     * @param jobStatus
     */
    public void finishedJobEntry(@NonNull final JobEntry jobEntry, @NonNull final JobStatus jobStatus) {
        m_jobProcessor.updateServerJobStatus(jobEntry, jobStatus);
    }

    /**
     * @return
     */
    public JobEntry getNextJobEntryToProcess() {
        JobEntry ret = null;

        try {
            m_lock.lock();

            while (isRunning() && (0 == m_scheduled.size())) {
                try {
                    m_notEmptyCondition.await();
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok, we got interrupted
                }
            }

            final ListIterator<JobEntry> listIter = m_scheduled.listIterator();

            while(listIter.hasNext()) {
                final JobEntry curEntry = listIter.next();

                // use entry and move entry from scheduled list to processing list,
                // if the job's hash is not currently processed otherwise;
                if ((null != curEntry) && !implIsProcessing(curEntry.getHash())) {
                    listIter.remove();
                    m_processing.add(curEntry);
                    ret = curEntry;
                    break;
                }
            }
        } finally {
            m_lock.unlock();
        }

        // perform status update for each entry out of sync block, since
        // entries might be removed from their original lists during the update
        if (null != ret) {
            m_jobProcessor.updateServerJobStatus(ret, JobStatus.RUNNING);
        }

        return ret;
    }

    // - Implementation ----------------------------------------------------

    /**
     * This private method is not synchronized, so that the caller needs
     * to ensure correct synchronization for given lists
     *
     * @param hash
     * @return
     */
    private boolean implIsProcessing(@NonNull final String hash) {
        return (Cache.isValidHash(hash) && (null != implFind(compareJobEntry -> {
            final String compareHash = compareJobEntry.getHash();
            return (Cache.isValidHash(compareHash) ? hash.compareTo(compareHash) : 1);
        }, m_processing)));
    }

    /**
     * This private method is not synchronized, so that the caller needs
     * to ensure correct synchronization for given lists
     *
     * @param findPredicate
     * @param lists
     * @return
     */
    private JobEntry implFind(@NonNull final Comparable<JobEntry> findPredicate, @NonNull final List<JobEntry>... lists) {
        for (final List<JobEntry> curList : lists) {
            for (final JobEntry curEntry : curList) {
                if (0 == findPredicate.compareTo(curEntry)) {
                    return curEntry;
                }
            }
        }

        return null;
    }

    /**
     * This private method is not synchronized, so that the caller needs
     * to ensure correct synchronization for given lists
     *
     * @param jobId
     * @param lists
     * @return
     */
    private JobEntry implRemove(@NonNull String jobId, @NonNull final List<JobEntry>... lists) {
        for (final List<JobEntry> curList : lists) {
            ListIterator<JobEntry> listIter = curList.listIterator();

            while (listIter.hasNext()) {
                final JobEntry curEntry = listIter.next();

                if (jobId.equals(curEntry.getJobId())) {
                    listIter.remove();
                    return curEntry;
                }
            }
        }

        return null;
    }

    // - Members --------------------------------------------------------------

    private JobProcessor m_jobProcessor = null;

    final private ReentrantLock m_lock = new ReentrantLock(true);

    final private Condition m_notEmptyCondition = m_lock.newCondition();

    final private List<JobEntry> m_scheduled = new ArrayList<>();

    final private List<JobEntry> m_processing = new ArrayList<>();

    final private AtomicBoolean m_isRunning = new AtomicBoolean(true);
}
