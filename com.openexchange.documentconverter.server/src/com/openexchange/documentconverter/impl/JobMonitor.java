/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.Watchdog.WatchdogMode;
import com.openexchange.documentconverter.impl.api.IQueueMonitor;
import com.openexchange.documentconverter.impl.api.JobEntry;

/**
 * {@link JobMonitor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.3
 */
public class JobMonitor implements IQueueMonitor {

    // JobMonitor timer name
    final private static String DC_JOBMONITOR_TIMER_NAME = "DC JobMonitor timer";

    // JobMonitor default timer period
    final private static long DC_MONITOR_TIMER_PERIOD_MILLIS =  60 * 1000L;

    /**
     * Initializes a new {@link JobMonitor}.
     * @param jobProcessor
     * @param queueCountLimitHigh
     * @param queueCountLimitLow
     * @param queueTimeoutSeconds
     */
    public JobMonitor(
        @NonNull final ServerManager serverManager,
        @NonNull final JobProcessor jobProcessor,
        final int queueCountLimitHigh,
        final int queueCountLimitLow,
        final int queueTimeoutSeconds) {

        super();

        m_serverManager = serverManager;
        m_jobProcessor = jobProcessor;

        m_queueCountLimitHigh = queueCountLimitHigh;
        m_queueCountLimitLow = queueCountLimitLow;

        m_queueTimeoutMillis = queueTimeoutSeconds * 1000L;

        // initialize time based jobs removed priority map
        for (final JobPriority curJobPriority : JobPriority.values()) {
            m_removedJobsPriorityMap.put(curJobPriority, new AtomicLong(0));
        }

        if (m_queueTimeoutMillis > 0) {
            m_logTimerExecutor.scheduleWithFixedDelay(() -> implLogQueueTimeoutLimitSummary(),
                DC_MONITOR_TIMER_PERIOD_MILLIS, DC_MONITOR_TIMER_PERIOD_MILLIS, TimeUnit.MILLISECONDS);
        }

        m_queueWatchdog = new Watchdog("DC JobMonitor watchdog", m_queueTimeoutMillis, WatchdogMode.TIMEOUT);
        m_queueWatchdog.start();

        implStartTimer(DC_MONITOR_TIMER_PERIOD_MILLIS);

        // log status of job monitoring at startup
        if (ServerManager.isLogInfo()) {
            // count limit
            final StringBuilder logStrBuilder = new StringBuilder(256).
                append("DC JobMonitor count based queue limiting").
                append(' ').append(implIsCountLimitEnabled() ? "enabled" : "disabled").
                append(" (").
                append("queueCountLimitHigh: ").append(queueCountLimitHigh).
                append(", ").
                append("queueCountLimitLow: ").append(queueCountLimitLow).
                append(")");

            ServerManager.logInfo(logStrBuilder.toString());

            // count limit
            logStrBuilder.setLength(0);
            logStrBuilder.append("DC JobMonitor timeout based queue limiting").
                append(' ').append(implIsTimeoutLimitEnabled() ? "enabled" : "disabled").
                append(" (").
                append("queueTimeoutSeconds: ").append(queueTimeoutSeconds).
                append(")");

            ServerManager.logInfo(logStrBuilder.toString());
        }
    }

    // - IQueueMonitor ---------------------------------------------------------

    @Override
    public synchronized void jobScheduled(@NonNull final JobEntry jobEntry) {
        final String jobId = jobEntry.getJobId();

        m_scheduledJobEntryMap.put(jobId, jobEntry);

        // remove old watchdog handler, if still set
        implRemoveWatchdogHandler(jobId);

        if (implIsCountLimitEnabled() &&
            (++m_curQueueCount == m_queueCountLimitHigh) &&
            m_queueLimitReached.compareAndSet(false, true)) {

            // update statistics
            m_serverManager.getStatistics().incrementQueueCountLimitHighReached();

            // log
            implLogIfQueueLimitReached(System.currentTimeMillis(), true);
        }

        if (implIsTimeoutLimitEnabled()) {
            // add new watchdog handler for jobId to map in order to identify the
            // handler for this jobId, e.g. if scheduling of this job has been
            // finished and the appropriate watchdogHandler needs to be removed
            final WatchdogHandler watchdogHandler = (curTimeMillis, startTimeMillis, jobError) -> {
                implRemoveScheduledJob(jobId, curTimeMillis, startTimeMillis);
            };

            m_watchdogHandlerMap.put(jobId, watchdogHandler);
            m_queueWatchdog.addWatchdogHandler(watchdogHandler);
        }
    }

    @Override
    public synchronized void jobSchedulingFinished(@NonNull final JobEntry jobEntry) {
        final String jobId = jobEntry.getJobId();

        // remove WatchdogHandler for this id from a previous scheduling
        implRemoveWatchdogHandler(jobId);

        if (implIsCountLimitEnabled() &&
            (--m_curQueueCount == m_queueCountLimitLow) &&
            m_queueLimitReached.compareAndSet(true, false)) {

            // update statistics
            m_serverManager.getStatistics().incrementQueueCountLimitLowReached();

            // log
            ServerManager.logInfo(new StringBuilder(256).
                append("DC JobMonitor JobCountInQueueLimitLow reached => requests are accepted again ").
                append('(').append(m_curQueueCount).append(" entries currently scheduled)").toString());
        }

        m_scheduledJobEntryMap.remove(jobId);
    }

    /**
     *
     */
    @Override
    public boolean isQueueLimitReached() {
        return m_queueLimitReached.get() || !m_isRunning.get();
    }

    /**
     *
     */
    @Override
    public synchronized int getScheduledCount() {
        return m_curQueueCount;
    }

    // - Public API ------------------------------------------------------------

    /**
     * terminate
     *
     */
    final public void terminate() {
        if (m_isRunning.compareAndSet(true, false)) {
            long shutdownStartTime = 0;
            final boolean trace = ServerManager.isLogTrace();

            if (trace) {
                shutdownStartTime = System.currentTimeMillis();
                ServerManager.logTrace("DC JobMonitor starting shutdown...");
            }

            m_logTimerExecutor.shutdownNow();

            synchronized (this) {
                // cancel and destroy running timer
                if (null != m_logTimer) {
                    m_logTimer.cancel();
                    m_logTimer = null;
                }

                // we only remove all leftover watchdog handlers here from the
                // watchdog since complete removal of the scheduled jobs would
                // interfere with the current iteration when the corresponding
                // #jobSchedulingFinished method is indirectly called within this loop
                m_watchdogHandlerMap.forEach((curJobId, curWatchdogHandler) -> {
                    m_queueWatchdog.removeWatchdogHandler(curWatchdogHandler);
                });

                m_watchdogHandlerMap.clear();
            }

            // terminate queue watchdog
            m_queueWatchdog.terminate();

            m_scheduledJobEntryMap.clear();

            if (trace) {
                ServerManager.logTrace(new StringBuilder(256).
                    append("DC JobMonitor finished shutdown: ").
                    append(System.currentTimeMillis() - shutdownStartTime).append("ms").
                    append('!').toString());
            }
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * implStartTimer
     *
     * @param timeoutdMillis
     */
    private void implStartTimer(final long timeoutMillis) {
        // schedule new singleshot timer with given timeout value
        final TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if (m_isRunning.get()) {
                    final long curTimeMillis = System.currentTimeMillis();

                    // log warning if queue limit is currently reached
                    implLogIfQueueLimitReached(curTimeMillis, false);

                    // restart timer with a new, calculated timeout
                    implStartTimer(Math.max(DC_MONITOR_TIMER_PERIOD_MILLIS - (curTimeMillis - m_lastLogTimestampMillis), 0));
                }
            }
        };

        synchronized (this) {
            if (null != m_logTimer) {
                m_logTimer.cancel();
            }

            m_logTimer = new Timer(DC_JOBMONITOR_TIMER_NAME, true);
            m_logTimer.schedule(timerTask, timeoutMillis);
        }
    }

    /**
     * @param jobId
     */
    private void implRemoveWatchdogHandler(@NonNull final String jobId) {
        final WatchdogHandler watchdogHandler = m_watchdogHandlerMap.remove(jobId);

        if (null != watchdogHandler) {
            m_queueWatchdog.removeWatchdogHandler(watchdogHandler);
        }
    }

    /**
     * !!! Method needs to by synchronized <=
     * !!! it is called from within a lambda expression and would give dead locks otherwise !!!
     *
     * @param jobId
     */
    private synchronized void implRemoveScheduledJob(
        @NonNull final String jobId,
        final long curTimeMillis,
        final long startTimeMillis) {

        final JobEntry jobEntry = m_scheduledJobEntryMap.get(jobId);
        final JobPriority jobPriority = (null != jobEntry) ? jobEntry.getJobPriority() : null;
        final String filename = (null != jobEntry) ? (String) jobEntry.getJob().getJobProperties().get(Properties.PROP_INFO_FILENAME) : DocumentConverterUtil.STR_NOT_AVAILABLE;

        if (null != jobPriority) {
            m_removedJobsPriorityMap.get(jobPriority).incrementAndGet();
        }

        // The watchdog handler is called from a different thread than the one
        // that scheduled the job, so that calling JobProcessor#removeScheduledJob
        // is ok and  no additional async. removal is necessary
        if (m_jobProcessor.removeScheduledJob(jobId)) {
            // update statistics
            m_serverManager.getStatistics().incrementQueueTimeoutCount();

            // log single removal
            ServerManager.logInfo("DC JobMonitor removed scheduled entry from qeue after " + ((curTimeMillis - startTimeMillis) / 1000L) + "s",
                  new LogData("jobId", jobId),
                  new LogData("filename", filename));
        }

        // remove entry from scheduled job entry map
        m_scheduledJobEntryMap.remove(jobId);
    }

    /**
     * implIsCountLimitEnabled
     *
     * @return
     */
    private boolean implIsCountLimitEnabled() {
        return (m_queueCountLimitHigh > 0);
    }

    /**
     * implIsTimeoutLimitEnabled
     *
     * @return
     */
    private boolean implIsTimeoutLimitEnabled() {
        return (m_queueTimeoutMillis > 0);
    }

    /**
     * implLogIfQueueLimitReached
     *
     * @param curTimeMillis
     * @param forceLogging
     */
    private synchronized void implLogIfQueueLimitReached(final long curTimeMillis, final boolean forceLogging) {
        if (forceLogging || ((curTimeMillis - m_lastLogTimestampMillis) >= DC_MONITOR_TIMER_PERIOD_MILLIS)) {
            if (m_queueLimitReached.get()) {
                ServerManager.logWarn(new StringBuilder(256).
                    append("DC JobMonitor JobCountInQueueLimitHigh reached => requests are rejected ").
                    append('(').append(m_curQueueCount).append(" entries currently scheduled)").toString());
            }

            m_lastLogTimestampMillis = curTimeMillis;
        }
    }

    /**
     *
     */
    private void implLogQueueTimeoutLimitSummary() {
        final List<LogData> logDataList = new ArrayList<>(m_removedJobsPriorityMap.size() + 1);
        long summedUpCount = 0;

        for (final JobPriority curJobPriority : JobPriority.values()) {
            final AtomicLong curCounter = m_removedJobsPriorityMap.get(curJobPriority);
            final long curCount = curCounter.get();

            if (curCount > 0) {
                curCounter.set(0);
                summedUpCount += curCount;
            }

            logDataList.add(new LogData(curJobPriority.toString().toLowerCase(), Long.toString(curCount)));
        }

        if (summedUpCount > 0) {
            logDataList.add(new LogData("total", Long.toString(summedUpCount)));

            ServerManager.logWarn(
                "DC JobMonitor removed scheduled entries from queue after " + (m_queueTimeoutMillis / 1000L) +
                "s during last observation period of " + (DC_MONITOR_TIMER_PERIOD_MILLIS / 1000L) + "s",
                logDataList.toArray(new LogData[logDataList.size()]));
        }
    }

    // - Members ---------------------------------------------------------------

    final protected AtomicBoolean m_isRunning = new AtomicBoolean(true);

    final protected ScheduledExecutorService m_logTimerExecutor = Executors.newScheduledThreadPool(1);

    final private ServerManager m_serverManager;

    final private JobProcessor m_jobProcessor;

    final private Watchdog m_queueWatchdog;

    final private Map<String, JobEntry> m_scheduledJobEntryMap = new ConcurrentHashMap<>();

    final private Map<String, WatchdogHandler> m_watchdogHandlerMap = new ConcurrentHashMap<>();

    final private AtomicBoolean m_queueLimitReached = new AtomicBoolean(false);

    final private Map<JobPriority, AtomicLong> m_removedJobsPriorityMap = new HashMap<>();

    final private int m_queueCountLimitHigh;

    final private int m_queueCountLimitLow;

    final private long m_queueTimeoutMillis;

    private long m_lastLogTimestampMillis = 0;

    private int m_curQueueCount = 0;

    private Timer m_logTimer = null;
}
