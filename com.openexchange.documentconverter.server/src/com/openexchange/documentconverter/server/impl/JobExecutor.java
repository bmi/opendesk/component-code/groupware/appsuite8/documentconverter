/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.impl;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import com.google.common.base.Throwables;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.Cache;
import com.openexchange.documentconverter.impl.ServerManager;

/**
 * {@link JobExecutor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
/**
 * {@link JobExecutor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class JobExecutor extends Executor {

    // RESPONSE_SIZE_WARN_THRESHOLD_BYTES set to 100MB
    final private static long RESPONSE_SIZE_WARN_THRESHOLD_BYTES = 100 * 1024 *1024;

    final private static String PAGE_1 = "1";

    /**
     * Initializes a new {@link JobExecutor}.
     *
     * @param manager
     * @param request
     * @param response
     */
    public JobExecutor(ServerManager manager, DocumentConverterServlet servlet, HttpServletRequest request, HttpServletResponse response) throws InvalidParameterException {
        super(manager, servlet, request, response);
    }

    // - Overrides -------------------------------------------------------------

    @Override
    protected Map<String, Map<String, String[]>> getParamDictionary() {
        return m_paramDictionary;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverterws.impl.Executor#doExecute(java.util.Map, java.util.Map)
     */
    @Override
    protected boolean doExecute(Map<String, String> params, HashMap<String, Object> resultProperties) {
        boolean ret = false;

        if ((null != m_manager) && (null != params) && (null != resultProperties)) {
            final String action = params.get("action");

            if ("rconvert".equals(action)) {
                ret = process(params, resultProperties);
            } else if ("convert".equals(action)) {
                ret = convert(params, resultProperties);
            } else if ("getpreview".equals(action)) {
                ret = getPreview(params, resultProperties);
            } else if ("getthumbnail".equals(action)) {
                ret = getThumbnail(params, resultProperties);
            } else if ("beginconvert".equals(action)) {
                ret = beginConvert(params, resultProperties);
            } else if ("getpage".equals(action)) {
                ret = getPage(params, resultProperties);
            } else if ("endconvert".equals(action)) {
                ret = endConvert(params, resultProperties);
            } else if ("batchconvert".equals(action)) {
                ret = batchConvert(resultProperties);
            }

        } else {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        }

        return ret;
    }

    /**
     * @param params
     * @param resultProperties
     * @return
     */
    protected boolean process(Map<String, String> params, HashMap<String, Object> resultProperties) {
        final String method = params.get("method");

        if (isNotEmpty(method)) {
            final HashMap<String, Object> jobProperties = new HashMap<>(12);
            final Cache cache = m_manager.getCache();
            final boolean logTrace = ServerManager.isLogTrace();
            byte[] resultBuffer = null;

            setJobProperties(params, jobProperties);

            final String filename = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);

            if ("convert".equals(method)) {
                // try to get the result based on the given remote hash from the cache
                final String remoteCacheHash = (null != cache) ? (String) jobProperties.get(Properties.PROP_REMOTE_CACHE_HASH) : null;

                if (isValidHash(remoteCacheHash)) {
                    try (final InputStream resultStm = ServerManager.getCachedResult(cache, remoteCacheHash, filename, resultProperties, true)) {
                        resultBuffer = ensureValidResultPropertiesAndGetBuffer(resultStm, resultProperties);

                        if (null != resultBuffer) {
                            // increment user request count, if cache request succeeded
                            m_manager.getStatistics().incrememtUserRequestCount(jobProperties, resultProperties);
                        }
                    } catch (final IOException e) {
                        ServerManager.logExcp(e);
                    }

                    if (logTrace) {
                        ServerManager.logTrace("DC server cache entry " + ((null != resultBuffer) ? "found" : "not found"),
                            new LogData("hash", remoteCacheHash),
                            new LogData("filename", (null != filename) ? filename : "unkown"));
                    }
                }

                // if we we could not get the result from the cache, do the real conversion
                if (null == resultBuffer) {
                    // if no result is found inside the cache, perform a real conversion
                    final String jobType = (String) jobProperties.get(Properties.PROP_JOBTYPE);

                    resultProperties.clear();

                    if (isNotEmpty(jobType)) {
                        final boolean isPDFSource[] = { false };
                        final File tmpInputFile = createTempInputFile(params, isPDFSource, jobProperties);

                        if (null != tmpInputFile) {
                            final String featuresIdString = params.get("featuresid");
                            int featuresId = -1;

                            // if a 'featuresid' param is set, containing a valid combination (ORed)
                            // of feature ids, add this featuresId to the current job properties
                            if (null != featuresIdString) {
                                try {
                                    featuresId = Integer.parseInt(featuresIdString);
                                } catch (final NumberFormatException e) {
                                    ServerManager.logExcp(e);
                                }

                                if (featuresId > 0) {
                                    jobProperties.put(Properties.PROP_FEATURES_ID, Integer.valueOf(featuresId));
                                }
                            }

                            // autoscale
                            if (jobType.startsWith("html") && "true".equals(params.get("autoscale"))) {
                                jobProperties.put(Properties.PROP_AUTOSCALE, Boolean.TRUE);
                            }

                            // do the conversion with the given input data
                            jobProperties.put(Properties.PROP_INPUT_FILE, tmpInputFile);

                            // disallow further remote hash calls, since we want to really convert
                            jobProperties.put(Properties.PROP_REMOTE_CACHE_HASH, Properties.CACHE_NO_CACHEHASH);

                            // finally convert (either synchronously or asynchronously)
                            try (final InputStream resultStm = executeDocumentConverterConversion(jobType, jobProperties, resultProperties)) {
                                resultBuffer = ensureValidResultPropertiesAndGetBuffer(resultStm, resultProperties);
                            } catch (final IOException e) {
                                ServerManager.logExcp(e);
                            }

                            // remove the temp. input file
                            FileUtils.deleteQuietly(tmpInputFile);
                        }
                    }
                }
            } else if ("queryavailability".equals(method)) {
                // try to get the result based on the given remote cache hash,
                // but don't return result buffer
                final String remoteCacheHash = (null != cache) ? (String) jobProperties.get(Properties.PROP_REMOTE_CACHE_HASH) : null;

                if (isValidHash(remoteCacheHash)) {
                    try (final InputStream resultStm = ServerManager.getCachedResult(cache, remoteCacheHash, filename, resultProperties, true)) {
                        resultBuffer = ensureValidResultPropertiesAndGetBuffer(resultStm, resultProperties);

                        if (logTrace) {
                            ServerManager.logTrace("DC querying for remotehash server cache entry " + ((null != resultBuffer) ? "succeeded" : "failed"), new LogData("remotehash", remoteCacheHash), new LogData("filename", (null != filename) ? filename : "unkown"));
                        }
                    } catch (final IOException e) {
                        ServerManager.logExcp(e);
                    }

                    final Integer resultJobErrorCode = (Integer) resultProperties.get(Properties.PROP_RESULT_ERROR_CODE);
                    final String resultJobErrorData = (String) resultProperties.get(Properties.PROP_RESULT_ERROR_DATA);

                    resultProperties.clear();

                    resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, resultJobErrorCode);

                    if (null != resultJobErrorData) {
                        resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, resultJobErrorData);
                    }
                }
            } else if ("beginpageconversion".equals(method)) {
                final String jobType = (String) jobProperties.get(Properties.PROP_JOBTYPE);

                if ((null != jobType) && (jobType.length() > 0)) {
                    final boolean isPDFSource[] = { false };
                    final File tmpInputFile = createTempInputFile(params, isPDFSource, jobProperties);

                    if (null != tmpInputFile) {
                        // do the conversion with the given input data
                        jobProperties.put(Properties.PROP_INPUT_FILE, tmpInputFile);

                        final String jobId = m_manager.beginPageConversion(jobType, jobProperties, resultProperties);

                        FileUtils.deleteQuietly(tmpInputFile);

                        if (null != jobId) {
                            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.NONE.getErrorCode()));
                            m_request.getSession(true);
                        }
                    }
                }
            } else if ("getconversionpage".equals(method)) {
                final String jobId = (String) jobProperties.get(Properties.PROP_JOBID);
                final int pageNumber = jobProperties.containsKey(Properties.PROP_PAGE_NUMBER) ? ((Integer) jobProperties.get(Properties.PROP_PAGE_NUMBER)).intValue() : 1;

                if (isNotEmpty(jobId) && (pageNumber > 0)) {
                    try (final InputStream resultStm = m_manager.getConversionPage(jobId, pageNumber, jobProperties, resultProperties)) {
                        resultBuffer = ensureValidResultPropertiesAndGetBuffer(resultStm, resultProperties);
                    } catch (final Exception e) {
                        ServerManager.logExcp(e);
                    }

                }
            } else if ("endpageconversion".equals(method)) {
                final String jobId = (String) jobProperties.get(Properties.PROP_JOBID);

                if (isNotEmpty(jobId)) {
                    m_manager.endPageConversion(jobId, jobProperties, resultProperties);

                    resultProperties.put(Properties.PROP_RESULT_JOBID, jobId);
                    resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.NONE.getErrorCode()));

                    final HttpSession session = m_request.getSession(false);

                    if (null != session) {
                        session.invalidate();
                    }
                }
            }
        }

        if (!resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        }

        return setResponse(params.get("returntype"), resultProperties);
    }

    /**
     * @param params
     * @param resultProperties
     * @return
     */
    protected boolean convert(final Map<String, String> params, final HashMap<String, Object> resultProperties) {
        return implConvert(params, resultProperties, true);
    }

    /**
     * @param params
     * @param resultProperties
     * @param doSetResponse
     * @return
     */
    protected boolean implConvert(final Map<String, String> params, final HashMap<String, Object> resultProperties, boolean doSetResponse) {
        final HashMap<String, Object> jobProperties = new HashMap<>(12);
        final String targetFormat = params.get("targetformat");
        final boolean isPDFSource[] = { false };
        final File inputTempFile = createTempInputFile(params, isPDFSource, jobProperties);
        boolean zipArchive = false;

        if (null != inputTempFile) {
            String jobType = null;

            jobProperties.put(Properties.PROP_INPUT_FILE, inputTempFile);
            jobProperties.put(Properties.PROP_PRIORITY, JobPriority.fromString(params.get("priority")));

            // 'html' or e.g. 'htmlpage' possible
            if (targetFormat.startsWith("html")) {
                jobType = "html";
                zipArchive = !"htmlpage".equals(targetFormat);

                if ("true".equals(params.get("autoscale"))) {
                    jobProperties.put(Properties.PROP_AUTOSCALE, Boolean.TRUE);
                }
            } else if ("pdf".equals(targetFormat)) {
                jobType = "pdf";
                jobProperties.put(Properties.PROP_MIME_TYPE, "application/pdf");
            } else if ("odf".equals(targetFormat)) {
                jobType = "odf";
            } else if ("ooxml".equals(targetFormat)) {
                jobType = "ooxml";
            } else if ("image".equals(targetFormat) || "jpeg".equals(targetFormat) || "jpg".equals(targetFormat)) {
                jobType = "image";
                jobProperties.put(Properties.PROP_MIME_TYPE, "image/jpeg");
                zipArchive = true;
            } else if ("png".equals(targetFormat)) {
                jobType = "image";
                jobProperties.put(Properties.PROP_MIME_TYPE, "image/png");
                zipArchive = true;
            }

            if (zipArchive) {
                jobProperties.put(Properties.PROP_ZIP_ARCHIVE, Boolean.TRUE);
            }

            if ("true".equals(params.get("nocache"))) {
                jobProperties.put(Properties.PROP_CACHE_HASH, Properties.CACHE_NO_CACHEHASH);
            }

            if ("true".equals(params.get("cacheonly"))) {
                jobProperties.put(Properties.PROP_CACHE_ONLY, Boolean.TRUE);
            }

            if ("true".equals(params.get("async"))) {
                jobProperties.put(Properties.PROP_ASYNC, Boolean.TRUE);
            }

            if ("true".equals(params.get("hidechanges"))) {
                jobProperties.put(Properties.PROP_HIDE_CHANGES, Boolean.TRUE);
            }

            if ("true".equals(params.get("hidecomments"))) {
                jobProperties.put(Properties.PROP_HIDE_COMMENTS, Boolean.TRUE);
            }

            if ("true".equals(params.get("userrequest"))) {
                jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);
            }

            if (null != jobType) {
                try (final InputStream resultStm = executeDocumentConverterConversion(jobType, jobProperties, resultProperties)) {
                    if (null != resultStm) {
                        final String mimeType = (String) jobProperties.get(Properties.PROP_MIME_TYPE);

                        // set error code
                        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.NONE.getErrorCode()));

                        // set mime type and extension, if available
                        if (isNotEmpty(mimeType)) {
                            final String extension = getExtensionForMimeType(mimeType);

                            resultProperties.put(Properties.PROP_RESULT_MIME_TYPE, mimeType);

                            if (isNotEmpty(extension)) {
                                resultProperties.put(Properties.PROP_RESULT_EXTENSION, extension);
                            }
                        }

                        // set result buffer at result properties in every case
                        if (null == resultProperties.get(Properties.PROP_RESULT_BUFFER)) {
                            resultProperties.put(Properties.PROP_RESULT_BUFFER, IOUtils.toByteArray(resultStm));
                        }
                    }
                } catch (final IOException e) {
                    ServerManager.logExcp(e);
                }
            }

            FileUtils.deleteQuietly(inputTempFile);
        }

        if (!resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        }

        return doSetResponse ?
            setResponse(params.get("returntype"), resultProperties) :
                (0 == ((Integer) resultProperties.get(Properties.PROP_RESULT_ERROR_CODE)).intValue());
    }

    /**
     * @param params
     * @param resultProperties
     * @return
     */
    protected boolean getPreview(Map<String, String> params, HashMap<String, Object> resultProperties) {
        final HashMap<String, Object> jobProperties = new HashMap<>(12);
        final boolean isPDFSource[] = { false };
        final File inputTempFile = createTempInputFile(params, isPDFSource, jobProperties);

        if (null != inputTempFile) {
            jobProperties.put(Properties.PROP_INPUT_FILE, inputTempFile);
            jobProperties.put(Properties.PROP_MIME_TYPE, params.get("targetformat").equals("jpeg") ? "image/jpeg" : "image/png");
            jobProperties.put(Properties.PROP_PAGE_RANGE, PAGE_1);
            jobProperties.put(Properties.PROP_PRIORITY, JobPriority.fromString(params.get("priority")));

            if ("true".equals(params.get("nocache"))) {
                jobProperties.put(Properties.PROP_CACHE_HASH, Properties.CACHE_NO_CACHEHASH);
            }

            if ("true".equals(params.get("cacheonly"))) {
                jobProperties.put(Properties.PROP_CACHE_ONLY, Boolean.TRUE);
            }

            if ("true".equals(params.get("async"))) {
                jobProperties.put(Properties.PROP_ASYNC, Boolean.TRUE);
            }

            if ("true".equals(params.get("hidechanges"))) {
                jobProperties.put(Properties.PROP_HIDE_CHANGES, Boolean.TRUE);
            }

            if ("true".equals(params.get("hidecomments"))) {
                jobProperties.put(Properties.PROP_HIDE_COMMENTS, Boolean.TRUE);
            }

            if ("true".equals(params.get("userrequest"))) {
                jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);
            }

            try (final InputStream resultStm = executeDocumentConverterConversion("preview", jobProperties, resultProperties)) {
                if (null != resultStm) {
                    try {
                        final String mimeType = (String) jobProperties.get(Properties.PROP_MIME_TYPE);

                        // set error code
                        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.NONE.getErrorCode()));

                        // set mime type and extension, if available
                        if (isNotEmpty(mimeType)) {
                            final String extension = getExtensionForMimeType(mimeType);

                            resultProperties.put(Properties.PROP_RESULT_MIME_TYPE, mimeType);

                            if (isNotEmpty(extension)) {
                                resultProperties.put(Properties.PROP_RESULT_EXTENSION, extension);
                            }
                        }

                        // set result buffer at result properties in every case
                        if (null == resultProperties.get(Properties.PROP_RESULT_BUFFER)) {
                            resultProperties.put(Properties.PROP_RESULT_BUFFER, IOUtils.toByteArray(resultStm));
                        }
                    } catch (final IOException e) {
                        ServerManager.logExcp(e);
                    }
                }
            } catch (final IOException e) {
                ServerManager.logExcp(e);
            } finally {
                FileUtils.deleteQuietly(inputTempFile);
            }
        }

        return setResponse(params.get("returntype"), resultProperties);
    }

    /**
     * @param params
     * @param resultProperties
     * @return
     */
    protected boolean getThumbnail(Map<String, String> params, HashMap<String, Object> resultProperties) {
        return getPreview(params, resultProperties);
    }

    /**
     * @param params
     * @param resultProperties
     * @return
     */
    protected boolean beginConvert(Map<String, String> params, HashMap<String, Object> resultProperties) {
        final HashMap<String, Object> jobProperties = new HashMap<>(12);
        final String targetFormat = params.get("targetformat");
        final boolean isPDFSource[] = { false };
        final File inputTempFile = createTempInputFile(params, isPDFSource, jobProperties);
        String jobId = null;

        if (null != inputTempFile) {
            String jobType = null;

            jobProperties.put(Properties.PROP_INPUT_FILE, inputTempFile);

            if (isPDFSource[0]) {
                jobProperties.put(Properties.PROP_INPUT_TYPE, "pdf");
            }

            if ("image".equals(targetFormat) || "jpeg".equals(targetFormat) || "jpg".equals(targetFormat)) {
                jobType = "image";
                jobProperties.put(Properties.PROP_MIME_TYPE, "image/jpeg");
            } else if ("png".equals(targetFormat)) {
                jobType = "image";
                jobProperties.put(Properties.PROP_MIME_TYPE, "image/png");
            } else {
                jobType = "html";

                // autoscale
                if ("true".equals(params.get("autoscale"))) {
                    jobProperties.put(Properties.PROP_AUTOSCALE, Boolean.TRUE);
                }
            }

            jobProperties.put(Properties.PROP_PRIORITY, JobPriority.fromString(params.get("priority")));

            if ("true".equals(params.get("nocache"))) {
                jobProperties.put(Properties.PROP_CACHE_HASH, Properties.CACHE_NO_CACHEHASH);
            }

            if ("true".equals(params.get("cacheonly"))) {
                jobProperties.put(Properties.PROP_CACHE_ONLY, Boolean.TRUE);
            }

            if ("true".equals(params.get("async"))) {
                jobProperties.put(Properties.PROP_ASYNC, Boolean.TRUE);
            }

            if ("true".equals(params.get("hidechanges"))) {
                jobProperties.put(Properties.PROP_HIDE_CHANGES, Boolean.TRUE);
            }

            if ("true".equals(params.get("hidecomments"))) {
                jobProperties.put(Properties.PROP_HIDE_COMMENTS, Boolean.TRUE);
            }

            if ("true".equals(params.get("userrequest"))) {
                jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);
            }

            jobId = m_manager.beginPageConversion(jobType, jobProperties, resultProperties);
            FileUtils.deleteQuietly(inputTempFile);
        }

        if (!resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        } else if (null != jobId) {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.NONE.getErrorCode()));
            m_request.getSession(true);
        }

        return setResponse(FORMAT_JSON, resultProperties);
    }

    /**
     * @param params
     * @param resultProperties
     * @return
     */
    protected boolean getPage(Map<String, String> params, HashMap<String, Object> resultProperties) {
        final String jobId = params.get("jobid");
        final int pageNumber = Integer.valueOf(params.get("pagenumber")).intValue();
        final HashMap<String, Object> jobProperties = new HashMap<>(4);

        try (final InputStream resultStm = m_manager.getConversionPage(jobId, pageNumber, jobProperties, resultProperties)) {
            if (null != resultStm) {
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.NONE.getErrorCode()));

                // set result buffer at result properties in every case
                if (null == resultProperties.get(Properties.PROP_RESULT_BUFFER)) {
                    resultProperties.put(Properties.PROP_RESULT_BUFFER, IOUtils.toByteArray(resultStm));
                }
            }
            return setResponse((null != resultStm ? params.get("returntype") : Executor.FORMAT_JSON), resultProperties);
        } catch (final Exception e) {
            ServerManager.logExcp(e);
        }

        return false;
    }

    /**
     * @param params
     * @param resultProperties
     * @return
     */
    protected boolean endConvert(Map<String, String> params, HashMap<String, Object> resultProperties) {
        final String jobId = params.get("jobid");

        if (jobId != null) {
            final HashMap<String, Object> jobProperties = new HashMap<>(4);

            m_manager.endPageConversion(jobId, jobProperties, resultProperties);

            resultProperties.put(Properties.PROP_RESULT_JOBID, jobId);
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.NONE.getErrorCode()));

            // check for JSESSIONID cookie with correct path and delete appropriately
            final HttpSession session = m_request.getSession();

            if (null != session) {
                session.invalidate();
            }
        } else {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        }

        return setResponse(Executor.FORMAT_JSON, resultProperties);
    }

    /**
     * @param params
     * @param resultProperties
     * @return
     */
    protected boolean batchConvert(final HashMap<String, Object> resultProperties) {
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        return setResponse(Executor.FORMAT_JSON, resultProperties);
    }

    /**
     * @param jobType
     * @param jobProperties
     * @param resultProperties
     * @return
     */
    protected InputStream executeDocumentConverterConversion(@NonNull String jobType, @NonNull HashMap<String, Object> jobProperties, @NonNull HashMap<String, Object> resultProperties) {
        InputStream ret = null;
        final Boolean isAsyncConversion = (Boolean) jobProperties.get(Properties.PROP_ASYNC);

        if ((null != isAsyncConversion) && isAsyncConversion.booleanValue()) {
            m_manager.triggerAsyncConvert(jobType, jobProperties, resultProperties);
            resultProperties.put(Properties.PROP_RESULT_ASYNC, Boolean.TRUE);
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.NONE.getErrorCode()));
        } else {
            ret = m_manager.convert(jobType, jobProperties, resultProperties);
        }

        return ret;
    }

    /**
     * @param resultArray
     * @param returnType
     * @param resultProperties
     * @return
     */
    protected boolean setResponse(String returnType, Map<String, Object> resultProperties) {
        String mimeType = "";
        String extension = "";
        Boolean zipArchive = Boolean.FALSE;
        boolean asyncConversion = false;
        byte[] resultBuffer = null;
        boolean ret = false;

        try {
            if (resultProperties.containsKey(Properties.PROP_RESULT_MIME_TYPE)) {
                mimeType = (String) resultProperties.get(Properties.PROP_RESULT_MIME_TYPE);
            }

            if (resultProperties.containsKey(Properties.PROP_RESULT_EXTENSION)) {
                extension = (String) resultProperties.get(Properties.PROP_RESULT_EXTENSION);
            }

            if (resultProperties.containsKey(Properties.PROP_RESULT_ZIP_ARCHIVE)) {
                zipArchive = (Boolean) resultProperties.get(Properties.PROP_RESULT_ZIP_ARCHIVE);
            }

            if (resultProperties.containsKey(Properties.PROP_RESULT_BUFFER)) {
                resultBuffer = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER);
            }

            if (resultProperties.containsKey(Properties.PROP_RESULT_ASYNC)) {
                asyncConversion = ((Boolean) resultProperties.get(Properties.PROP_RESULT_ASYNC)).booleanValue();
            }

            if ("json".equals(returnType)) {
                final JSONObject jsonObject = new JSONObject();
                final JobErrorEx jobErrorEx = JobErrorEx.fromResultProperties(resultProperties);
                final String cacheHash = (String) resultProperties.get(Properties.PROP_RESULT_CACHE_HASH);
                final String inputFileHash = (String) resultProperties.get(Properties.PROP_RESULT_INPUTFILE_HASH);
                final String locale = (String) resultProperties.get(Properties.PROP_RESULT_LOCALE);
                final String jobIDStr = (String) resultProperties.get(Properties.PROP_RESULT_JOBID);
                final String pageJobType = (String) resultProperties.get(Properties.PROP_RESULT_SINGLE_PAGE_JOBTYPE);
                final Integer pageCount = (Integer) resultProperties.get(Properties.PROP_RESULT_PAGE_COUNT);
                final Integer originalPageCount = (Integer) resultProperties.get(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT);
                final Integer pageNumber = (Integer) resultProperties.get(Properties.PROP_RESULT_PAGE_NUMBER);
                final Integer shapeNumber = (Integer) resultProperties.get(Properties.PROP_RESULT_SHAPE_NUMBER);

                m_response.setContentType("application/json");
                m_response.setHeader("Cache-Control", "no-cache");

                try (final BufferedWriter bufferedWriter = new BufferedWriter(m_response.getWriter())) {
                    jsonObject.put("async", asyncConversion);
                    jsonObject.put("errorcode", Integer.valueOf(jobErrorEx.getErrorCode()));
                    jsonObject.put("errordata", jobErrorEx.getErrorData().toString());

                    if (!asyncConversion) {
                        if (isValidHash(cacheHash)) {
                            jsonObject.put("cachehash", cacheHash);
                        }

                        if (isValidHash(inputFileHash)) {
                            jsonObject.put("inputfilehash", inputFileHash);
                        }

                        if (isNotEmpty(locale)) {
                            jsonObject.put("locale", locale);
                        }

                        if (isNotEmpty(jobIDStr)) {
                            jsonObject.put("jobid", jobIDStr);
                        }

                        if (isNotEmpty(pageJobType)) {
                            jsonObject.put("pagejobtype", pageJobType);
                        }

                        if ((null != pageCount) && (pageCount.intValue() > 0)) {
                            jsonObject.put("pagecount", pageCount);
                        }

                        if ((null != originalPageCount) && (originalPageCount.intValue() > 0)) {
                            jsonObject.put("originalpagecount", originalPageCount);
                        }

                        if ((null != pageNumber) && (pageNumber.intValue() > 0)) {
                            jsonObject.put("pagenumber", pageNumber);
                        }

                        if ((null != shapeNumber) && (shapeNumber.intValue() > 0)) {
                            jsonObject.put("shapenumber", shapeNumber);
                        }

                        if (isNotEmpty(mimeType)) {
                            jsonObject.put("mimetype", mimeType);
                        }

                        if (isNotEmpty(extension)) {
                            jsonObject.put("extension", extension);
                        }

                        if (JobError.PASSWORD == jobErrorEx.getJobError()) {
                            jsonObject.put("passwordprotected", "true");
                        } else if (JobError.MAX_SOURCESIZE == jobErrorEx.getJobError()) {
                            jsonObject.put("maxsourcesize", "true");
                        } else if (JobError.UNSUPPORTED == jobErrorEx.getJobError()) {
                            jsonObject.put("unsupported", "true");
                        }

                        if (null != resultBuffer) {
                            final String base64Result = Base64.encodeBase64String(resultBuffer);

                            if (null != base64Result) {
                                implHandleResponseSize(
                                    "DC webservice returns JSON response with base64 encoded result string",
                                    base64Result.length());

                                jsonObject.put("result", new StringBuilder(base64Result.length() + 256).
                                    append("data:").
                                    append(zipArchive.booleanValue() ? "application/zip" : mimeType).
                                    append(";base64,").
                                    append(base64Result).toString());
                            }
                        }
                    }

                    jsonObject.write(bufferedWriter);
                    bufferedWriter.flush();

                    ret = true;
                }
            } else if ("binary".equals(returnType)) {
                JobErrorEx jobErrorEx = new JobErrorEx();

                if (asyncConversion) {
                    // we're already finished
                    ret = true;
                } else {
                    // ensure consistency with old style API
                    if (resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
                        jobErrorEx = JobErrorEx.fromResultProperties(resultProperties);

                        if (JobError.PASSWORD == jobErrorEx.getJobError()) {
                            resultProperties.put("PasswordProtected", Boolean.TRUE);
                        }
                    }

                    m_response.setContentType("application/octet-stream");
                    m_response.setHeader("Content-Disposition", "attachment;filename=Map.bin");

                    try (ObjectOutputStream objOutputStm = new ObjectOutputStream(new BufferedOutputStream(m_response.getOutputStream()))) {

                        objOutputStm.writeObject(resultProperties);
                        objOutputStm.flush();
                        ret = true;
                    }
                }
            } else if (FORMAT_FILE.equals(returnType) && (null != resultBuffer)) {
                m_response.setContentType(zipArchive.booleanValue() ? "application/zip" : mimeType);
                m_response.setContentLength(resultBuffer.length);
                m_response.setHeader("Content-Disposition", "attachment;filename=result." + (zipArchive.booleanValue() ? "zip" : extension));

                implHandleResponseSize(
                    "DC webservice returns file response with file size",
                    resultBuffer.length);

                try (OutputStream outputStm = m_response.getOutputStream()) {
                    IOUtils.write(resultBuffer, outputStm);
                    outputStm.flush();
                    m_response.flushBuffer();

                    ret = true;
                }
            } else if (asyncConversion) {
                ret = true;
            }
        } catch (OutOfMemoryError e) {
            final Integer oomErrorCode = Integer.valueOf(JobError.OUT_OF_MEMORY.getErrorCode());

            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, oomErrorCode);

            if ("json".equals(returnType)) {
                final JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("errorcode", oomErrorCode);
                    jsonObject.put("outOfMemory", true);
                } catch (final Exception e1) {
                    ServerManager.logExcp(e1);
                }
            }

            ServerManager.logError("DC webservice has no memory left to send response for the last request: " +
                Throwables.getRootCause(e));
        } catch (final Exception e) {
            ServerManager.logExcp(e);
        }

        return ret;
    }

    /**
     * @param properties
     */
    protected static void setJobProperties(Map<String, String> params, HashMap<String, Object> properties) {
        String curParam = null;

        // Method
        if (null != (curParam = params.get("method"))) {
            properties.put(Properties.PROP_REMOTE_METHOD, curParam);
        }

        // JobType
        if (null != (curParam = params.get("jobtype"))) {
            properties.put(Properties.PROP_JOBTYPE, curParam);
        }

        // JobId
        if (null != (curParam = params.get("jobid"))) {
            properties.put(Properties.PROP_JOBID, curParam);
        }

        // JobPriority
        if (null != (curParam = params.get("priority"))) {
            properties.put(Properties.PROP_PRIORITY, JobPriority.fromString(curParam));
        }

        // Locale
        if (null != (curParam = params.get("locale"))) {
            properties.put(Properties.PROP_LOCALE, curParam);
        }

        // CacheHash
        if (isValidHash(curParam = params.get("cachehash"))) {
            properties.put(Properties.PROP_CACHE_HASH, curParam);
        }

        // RemoteCacheHash
        if (isValidHash(curParam = params.get("remotecachehash"))) {
            properties.put(Properties.PROP_REMOTE_CACHE_HASH, curParam);
        }

        // FilterShortName
        if (null != (curParam = params.get("filtershortname"))) {
            properties.put(Properties.PROP_FILTER_SHORT_NAME, curParam);
        }

        // InputType
        if (null != (curParam = params.get("inputtype"))) {
            properties.put(Properties.PROP_INPUT_TYPE, curParam);
        }

        // InputUrl
        if (null != (curParam = params.get("inputurl"))) {
            try {
                // #DOCS-3981: ensure that no invalid hex values (e.g. %Xy)
                // get decoded as this will throw a not declared IllegalArgumentException.
                // In such cases, we replace the illegal hex value with a space char (%25).
                curParam = URLDecoder.decode(curParam.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "UTF-8");
            } catch (@SuppressWarnings("unused") Exception e) {
                ServerManager.logTrace("DC cannot decode given URL, using as is: " + curParam);
            }

            properties.put(Properties.PROP_INPUT_URL, curParam);
        }

        // PixelX
        if (null != (curParam = params.get("pixelx"))) {
            properties.put(Properties.PROP_PIXEL_X, Integer.valueOf(curParam));
        }

        // PixelX
        if (null != (curParam = params.get("pixely"))) {
            properties.put(Properties.PROP_PIXEL_Y, Integer.valueOf(curParam));
        }

        // PixelWidth
        if (null != (curParam = params.get("pixelwidth"))) {
            properties.put(Properties.PROP_PIXEL_WIDTH, Integer.valueOf(curParam));
        }

        // PixelHeight
        if (null != (curParam = params.get("pixelheight"))) {
            properties.put(Properties.PROP_PIXEL_HEIGHT, Integer.valueOf(curParam));
        }

        // MimeType
        if (null != (curParam = params.get("mimetype"))) {
            properties.put(Properties.PROP_MIME_TYPE, curParam);
        }

        // PageRange
        if (null != (curParam = params.get("pagerange"))) {
            properties.put(Properties.PROP_PAGE_RANGE, isValidPageRange(curParam) ? curParam : PAGE_1);
        }

        // PageNumber
        if (null != (curParam = params.get("pagenumber"))) {
            properties.put(Properties.PROP_PAGE_NUMBER, Integer.valueOf(curParam));
        }

        // ShapeNumber
        if (null != (curParam = params.get("shapenumber"))) {
            properties.put(Properties.PROP_SHAPE_NUMBER, Integer.valueOf(curParam));
        }

        // ZipArchive
        if (null != (curParam = params.get("ziparchive"))) {
            properties.put(Properties.PROP_ZIP_ARCHIVE, Boolean.valueOf(curParam));
        }

        // InfoFilename
        if (null != (curParam = params.get("infofilename"))) {
            try {
                // #DOCS-3981: ensure that no invalid hex values (e.g. %Xy)
                // get decoded as this will throw a not declared IllegalArgumentException.
                // In such cases, we replace the illegal hex value with a space char (%25).
                curParam = URLDecoder.decode(curParam.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "UTF-8");
            } catch (@SuppressWarnings("unused") Exception e) {
                ServerManager.logTrace("DC cannot decode given URL, using as is: " + curParam);
            }

            properties.put(Properties.PROP_INFO_FILENAME, curParam);
        }

        // CacheOnly
        if (null != (curParam = params.get("cacheonly"))) {
            properties.put(Properties.PROP_CACHE_ONLY, Boolean.valueOf(curParam));
        }

        // ImageResolution
        if (null != (curParam = params.get("imageresolution"))) {
            properties.put(Properties.PROP_IMAGE_RESOLUTION, Integer.valueOf(curParam));
        }

        // ImageScaleType
        if (null != (curParam = params.get("imagescaletype"))) {
            properties.put(Properties.PROP_IMAGE_SCALE_TYPE, curParam);
        }

        // Async
        if (null != (curParam = params.get("async"))) {
            properties.put(Properties.PROP_ASYNC, Boolean.valueOf(curParam));
        }

        // HideChanges
        if (null != (curParam = params.get("hidechanges"))) {
            properties.put(Properties.PROP_HIDE_CHANGES, Boolean.valueOf(curParam));
        }

        // HideComments
        if (null != (curParam = params.get("hidecomments"))) {
            properties.put(Properties.PROP_HIDE_COMMENTS, Boolean.valueOf(curParam));
        }

        // RemoteUrl
        if (null != (curParam = params.get("remoteurl"))) {
            properties.put(Properties.PROP_ENGINE_REMOTEURL, curParam);
        }

        // UserRequest
        if (null != (curParam = params.get("userrequest"))) {
            properties.put(Properties.PROP_USER_REQUEST, Boolean.valueOf(curParam));
        }

        // ShapeReplacements
        if (null != (curParam = params.get("shapereplacements"))) {
            try {
                properties.put(Properties.PROP_SHAPE_REPLACEMENTS, URLDecoder.decode(curParam, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                ServerManager.logTrace("DC cannot decode given shapereplacements: " + curParam);
            }
        }

        // ensure that a valid job priority is always set
        properties.putIfAbsent(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);

        ServerManager.ensureInputTypeSet(properties);
    }

    /**
     * @param mimeType
     * @return
     */
    protected static String getExtensionForMimeType(String mimeType) {
        String ret = null;

        if (isNotEmpty(mimeType)) {
            switch (mimeType) {
                case "application/pdf":
                    ret = "pdf";
                    break;

                case "image/png":
                    ret = "png";
                    break;

                case "image/jpg":
                case "image/jpeg":
                    ret = "jpg";
                    break;

                case "text/html":
                    ret = "html";
                    break;

                default: {
                    break;
                }
            }
        }

        return ret;
    }

    /**
     * @param resultStm
     * @param resultProperties
     * @return
     * @throws IOException
     */
    protected static byte[] ensureValidResultPropertiesAndGetBuffer(final InputStream resultStm, final Map<String, Object> resultProperties) throws IOException {
        byte[] ret = null;

        // ensure that the property PROP _RESULT_BUFFER is set in success case
        if (null != resultStm) {
            ret = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER);

            if (null == ret) {
                ret = IOUtils.toByteArray(resultStm);
                resultProperties.put(Properties.PROP_RESULT_BUFFER, ret);
            }
        }

        // ensure, that the property PROP_RESULT_ERROR_CODE is always set
        Integer resultErrorCode = (Integer) resultProperties.get(Properties.PROP_RESULT_ERROR_CODE);

        if (null == ret) {
            // set JobError.GENERAL as default, if no jobError is set by now
            if (null == resultErrorCode) {
                resultErrorCode = Integer.valueOf(JobError.GENERAL.getErrorCode());
            }
        } else if (ret.length < 1) {
            // set JobError.GENERAL as default, if no jobError is set by now
            if (null == resultErrorCode) {
                resultErrorCode = Integer.valueOf(JobError.NO_CONTENT.getErrorCode());
            }

            ret = null;
        } else {
            resultErrorCode = Integer.valueOf(JobError.NONE.getErrorCode());
        }

        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, resultErrorCode);

        return ret;
    }

    /**
     * @param msg
     * @param size
     */
    private void implHandleResponseSize(@NonNull final String msg, long size) {
        final long sizeMB = size >> 20;

        if (size >= RESPONSE_SIZE_WARN_THRESHOLD_BYTES) {
            ServerManager.logWarn(new StringBuilder(256).
                append("DC webservice is returning large request response with content of size: ").
                append(size).append(" Bytes (").
                append(sizeMB).append(" MegaBytes)").toString());

        }

        if (ServerManager.isLogTrace()) {
            ServerManager.logTrace(new StringBuilder(256).
                append(msg).append(": ").
                append(size).append(" Bytes (").
                append(sizeMB).append(" MegaBytes)").toString());
        }

    }

    // - Members ---------------------------------------------------------------

    /**
     * The conversion service manager interface
     */
    static private Map<String, Map<String, String[]>> m_paramDictionary = new HashMap<>(8);

    // initialize static m_dictionary member
    static {
        Map<String, String[]> curMap = null;
        final String[] arbitraryString = new String[0];
        final String[] defaultFalseTrueString = { ":false", "true" };
        final String[] defaultPriorityString = { "background", ":low", "medium", "high", "instant" };
        final String[] defaultFileJsonString = { ":file", "json" };
        final String[] defaultJsonBinaryFileString = { ":json", "binary", FORMAT_FILE };
        final String[] defaultNativeString = { ":native", "serialized" };
        final String[] defaultSerializedString = { ":serialized", "native" };

        // fill 'rconvert' map
        m_paramDictionary.put("rconvert", curMap = new HashMap<>(32));
        curMap.put("method", arbitraryString);
        curMap.put("jobtype", arbitraryString);
        curMap.put("jobid", arbitraryString);
        curMap.put("priority", defaultPriorityString);
        curMap.put("locale", arbitraryString);
        curMap.put("cachehash", arbitraryString);
        curMap.put("remotecachehash", arbitraryString);
        curMap.put("filtershortname", arbitraryString);
        curMap.put("inputtype", arbitraryString);
        curMap.put("inputurl", arbitraryString);
        curMap.put("pixelwidth", arbitraryString);
        curMap.put("pixelheight", arbitraryString);
        curMap.put("mimetype", arbitraryString);
        curMap.put("pagerange", arbitraryString);
        curMap.put("pagenumber", arbitraryString);
        curMap.put("shapenumber", arbitraryString);
        curMap.put("ziparchive", arbitraryString);
        curMap.put("infofilename", arbitraryString);
        curMap.put("featuresid", arbitraryString);
        curMap.put("nocache", defaultFalseTrueString);
        curMap.put("cacheonly", defaultFalseTrueString);
        curMap.put("async", defaultFalseTrueString);
        curMap.put("imageresolution", arbitraryString);
        curMap.put("imagescaletype", arbitraryString);
        curMap.put("remoteurl", arbitraryString);
        curMap.put("url", arbitraryString);
        curMap.put("returntype", defaultJsonBinaryFileString);
        curMap.put("multipartfileformat", defaultSerializedString);
        curMap.put("autoscale", defaultFalseTrueString);
        curMap.put("hidechanges", defaultFalseTrueString);
        curMap.put("hidecomments", defaultFalseTrueString);
        curMap.put("userrequest", defaultFalseTrueString);
        curMap.put("shapereplacements", arbitraryString);

        // fill 'convert' map
        m_paramDictionary.put("convert", curMap = new HashMap<>(16));
        curMap.put("url", arbitraryString);
        curMap.put("targetformat", new String[] { ":pdf", "html", "htmlpage", "odf", "ooxml", "jpeg", "png" });
        curMap.put("firstpageonly", defaultFalseTrueString);
        curMap.put("returntype", defaultFileJsonString);
        curMap.put("nocache", defaultFalseTrueString);
        curMap.put("cacheonly", defaultFalseTrueString);
        curMap.put("async", defaultFalseTrueString);
        curMap.put("priority", defaultPriorityString);
        curMap.put("multipartfileformat", defaultNativeString);
        curMap.put("autoscale", defaultFalseTrueString);
        curMap.put("hidechanges", defaultFalseTrueString);
        curMap.put("hidecomments", defaultFalseTrueString);
        curMap.put("userrequest", defaultFalseTrueString);
        curMap.put("shapereplacements", arbitraryString);

        // fill 'getpreview' map
        m_paramDictionary.put("getpreview", curMap = new HashMap<>(16));
        curMap.put("url", arbitraryString);
        curMap.put("targetformat", new String[] { ":jpeg", "png" });
        curMap.put("returntype", defaultFileJsonString);
        curMap.put("nocache", defaultFalseTrueString);
        curMap.put("cacheonly", defaultFalseTrueString);
        curMap.put("async", defaultFalseTrueString);
        curMap.put("priority", defaultPriorityString);
        curMap.put("multipartfileformat", defaultNativeString);
        curMap.put("hidechanges", defaultFalseTrueString);
        curMap.put("hidecomments", defaultFalseTrueString);
        curMap.put("userrequest", defaultFalseTrueString);

        // fill 'getthumbnail' map
        m_paramDictionary.put("getthumbnail", curMap = new HashMap<>(16));
        curMap.put("url", arbitraryString);
        curMap.put("targetformat", new String[] { ":jpeg", "png" });
        curMap.put("returntype", defaultFileJsonString);
        curMap.put("nocache", defaultFalseTrueString);
        curMap.put("cacheonly", defaultFalseTrueString);
        curMap.put("async", defaultFalseTrueString);
        curMap.put("priority", defaultPriorityString);
        curMap.put("multipartfileformat", defaultNativeString);
        curMap.put("hidechanges", defaultFalseTrueString);
        curMap.put("hidecomments", defaultFalseTrueString);
        curMap.put("userrequest", defaultFalseTrueString);

        // fill 'beginconvert' map
        m_paramDictionary.put("beginconvert", curMap = new HashMap<>(16));
        curMap.put("url", arbitraryString);
        curMap.put("targetformat", new String[] { ":html", "jpeg", "png" });
        curMap.put("nocache", defaultFalseTrueString);
        curMap.put("cacheonly", defaultFalseTrueString);
        curMap.put("async", defaultFalseTrueString);
        curMap.put("priority", defaultPriorityString);
        curMap.put("multipartfileformat", defaultNativeString);
        curMap.put("autoscale", defaultFalseTrueString);
        curMap.put("hidechanges", defaultFalseTrueString);
        curMap.put("hidecomments", defaultFalseTrueString);
        curMap.put("userrequest", defaultFalseTrueString);

        // fill 'getpage' map
        m_paramDictionary.put("getpage", curMap = new HashMap<>(16));
        curMap.put("jobid", arbitraryString);
        curMap.put("pagenumber", arbitraryString);
        curMap.put("returntype", defaultFileJsonString);
        curMap.put("nocache", defaultFalseTrueString);
        curMap.put("cacheonly", defaultFalseTrueString);
        curMap.put("async", defaultFalseTrueString);
        curMap.put("multipartfileformat", defaultNativeString);

        // fill 'endconvert' map
        m_paramDictionary.put("endconvert", curMap = new HashMap<>(8));
        curMap.put("jobid", arbitraryString);
        curMap.put("nocache", defaultFalseTrueString);
        curMap.put("cacheonly", defaultFalseTrueString);
        curMap.put("async", defaultFalseTrueString);
        curMap.put("multipartfileformat", defaultNativeString);

        // fill 'batchconvert' map
        m_paramDictionary.put("batchconvert", curMap = new HashMap<>(16));
        curMap.putAll(m_paramDictionary.get("convert"));
    }
}
