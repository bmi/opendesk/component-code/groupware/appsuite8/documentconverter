/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.nio.file.Files;
import java.security.InvalidParameterException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.base.Throwables;
import com.openexchange.documentconverter.DataSourceFile;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.TransferObject;
import com.openexchange.documentconverter.impl.Cache;
import com.openexchange.documentconverter.impl.ServerManager;
import com.openexchange.documentconverter.impl.api.DataSourceFileInputStream;

/**
 * {@link JobExecutor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public abstract class Executor {

    final public static String FORMAT_JSON = "json";

    final public static String FORMAT_FILE = "file";

    final private static Pattern ALLOWED_HASH_CHARS_PATTERN = Pattern.compile("^[a-zA-Z0-9\\_\\-\\.\\,\\@\\#]+$");

    final private static Pattern ALLOWED_HASH_START_CHARS_PATTERN = Pattern.compile("^[a-zA-Z0-9]+");

    final private static Pattern ALLOWED_PAGERANGE_CHARS_PATTERN = Pattern.compile("^[0-9\\-\\,]+$");

    final private static int MAX_FORM_DATA_PART_COUNT = 32;

    /**
     * Initializes a new {@link JobExecutor}.
     *
     * @param request
     */
    public Executor(ServerManager manager, DocumentConverterServlet servlet, HttpServletRequest request, HttpServletResponse response) throws InvalidParameterException {
        super();

        m_manager = manager;
        m_documentConverterServlet = servlet;
        m_request = request;
        m_response = response;

        if (null != m_request) {
            final Enumeration<String> params = m_request.getParameterNames();
            final String contentType = (null != m_request) ? m_request.getContentType() : null;

            if ((null != contentType) && contentType.startsWith("multipart/form-data")) {
                extractMultipartRequestData();
            }

            if (null != params) {
                while (params.hasMoreElements()) {
                    final String curParam = params.nextElement();

                    if (null == m_requestParams) {
                        m_requestParams = new HashMap<>();
                    }

                    if (!StringUtils.isEmpty(curParam)) {
                        m_requestParams.put(curParam.startsWith("?") ? curParam.substring(1) : curParam, m_request.getParameter(curParam));
                    }
                }
            }
        }
    }

    // - abstract methods ------------------------------------------------------

    /**
     * @return
     */
    protected abstract Map<String, Map<String, String[]>> getParamDictionary();

    /**
     * @param params The current parameters to execute.
     * @param results The results of the execution.
     * @return true, if the request could be successfully executed
     */
    protected abstract boolean doExecute(Map<String, String> params, HashMap<String, Object> results);

    // -- public methods -------------------------------------------------------

    /**
     * @param jobProperties Input/Output parameter containing initial job properties (input) as well as additional result parameters
     *            (output).
     * @return
     */
    public boolean execute(HashMap<String, Object> results) {
        final Map<String, String> params = initParams();
        int responseStatus = HttpServletResponse.SC_BAD_REQUEST;

        if (null != params) {
            if (doExecute(params, results)) {
                responseStatus = HttpServletResponse.SC_OK;
            } else {
                responseStatus = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            }
        }

        return (HttpServletResponse.SC_OK == responseStatus);
    }

    /**
     * @return
     */
    public Map<String, String> getRequestParams() {
        return m_requestParams;
    }

    /**
     * @return
     */
    public Map<String, String> getParams() {
        return m_params;
    }

    /**
     * @param url The URL the document to convert should be read from. Valid types are: dataURL, fileURL or httpURL.
     * @param documentType The type of document ("odf", "ooxml", "pdf", "img", "any") to convert [output].
     * @return
     */
    protected File createTempInputFile(Map<String, String> params, boolean isPDFSource[], Map<String, Object> jobProperties) {
        File ret = null;
        String url = (null != params) ? params.get("url") : null;

        if (null == url) {
            // we expect a File object nowadays, which previousy needs to be set within the #extractFormData method
            final Serializable contentSerializable = (null != m_transferObject) ? m_transferObject.getSerialObject() : null;

            if (null != contentSerializable) {
                if (contentSerializable instanceof File) {
                    ret = (File) contentSerializable;
                    m_transferObject.setSerialObject(null);

                    if (isPDFSource.length > 0) {
                        final String serialMimeType = m_transferObject.getMimeType();

                        // check for PDF mime type
                        isPDFSource[0] = "application/pdf".equals(serialMimeType);
                    }
                } else if (contentSerializable instanceof DataSourceFile) {
                    final byte[] data = ((DataSourceFile) contentSerializable).getDataSinkBuffer();
                    m_transferObject.setSerialObject(null);

                    if ((null != data) && (data.length > 0)) {
                        ret = implCreateTempFile();

                        if (null != ret) {
                            try {
                                FileUtils.writeByteArrayToFile(ret, data);
                            } catch (final IOException e) {
                                FileUtils.deleteQuietly(ret);
                                ret = null;
                                ServerManager.logExcp(e);
                            }
                        }
                    }
                }
            }

            if ((null == ret) && !jobProperties.containsKey(Properties.PROP_REMOTE_CACHE_HASH) && ServerManager.isLogDebug()) {
                ServerManager.logDebug("DC WebService neither received a valid input buffer with data nor a hash value to retrieve data with");
            }
        } else {
            String infoFileName= (String) jobProperties.get(Properties.PROP_INFO_FILENAME);
            String httpURL = url = url.trim();

            if (url.indexOf("data:") == 0) {
                final String base64SearchPattern = "base64,";
                int pos = url.indexOf(base64SearchPattern);

                if ((isPDFSource.length > 0) && (-1 != pos) && (pos > 5)) {
                    // check for PDF mime type
                    isPDFSource[0] = url.substring(5, 5 + StringUtils.length("application/pdf")).equals("application/pdf");
                }

                if ((-1 != pos) && ((pos += base64SearchPattern.length()) < (url.length() - 1))) {
                    final byte[] data = Base64.decodeBase64(url.substring(pos));

                    if ((null != data) && (data.length > 0)) {
                        ret = implCreateTempFile();

                        if (null != ret) {
                            try {
                                FileUtils.writeByteArrayToFile(ret, data);
                            } catch (final IOException e) {
                                FileUtils.deleteQuietly(ret);
                                ret = null;
                                ServerManager.logExcp(e);
                            } finally {
                                // relaese the base64 encoded data buffer ASAP, since
                                // we already saved its content within a temp. file
                                if (null != params) {
                                    params.remove("url");
                                }
                            }
                        }
                    }
                }
            } else if (httpURL.startsWith("http://") || httpURL.startsWith("https://")) {
                final File tempFile = implCreateTempFile();
                final MutableWrapper<String> extensionWrapper = new MutableWrapper<>(null);

                if (null == (ret = ServerManager.resolveURL(httpURL, tempFile, extensionWrapper))) {
                    FileUtils.deleteQuietly(tempFile);
                } else if ("pdf".equals(extensionWrapper.get()) && (isPDFSource.length > 0)) {
                    isPDFSource[0] = true;
                }
            } else if (m_manager.getServerConfig().ALLOW_LOCAL_FILE_URLS && (url.indexOf("file:///") == 0)) {
                final String fileName = url.substring(7);
                final File localFile = new File(fileName);

                if (fileName.toLowerCase().trim().endsWith(".pdf") && (isPDFSource.length > 0)) {
                    isPDFSource[0] = true;
                }

                if (localFile.canRead()) {
                    ret = implCreateTempFile();

                    if (null != ret) {
                        try {
                            Files.copy(localFile.toPath(), FileUtils.openOutputStream(ret));

                            if (null == infoFileName) {
                                infoFileName = fileName;
                            }
                        } catch (final IOException e) {
                            FileUtils.deleteQuietly(ret);
                            ret = null;
                            ServerManager.logExcp(e);
                        }
                    }
                }
            }

            if ((null == ret) && ServerManager.isLogDebug()) {
                ServerManager.logDebug("DC WebService didn't receive a valid soure URL or could not write source content to temp. file");
            } else if ((null != infoFileName) && (infoFileName.length() > 0)) {
                jobProperties.put(Properties.PROP_INFO_FILENAME, infoFileName);
            } else {
                jobProperties.remove(Properties.PROP_INFO_FILENAME);
            }
        }

        return ret;
    }

    /**
     * @param hash
     * @return
     */
    protected static boolean isValidHash(final String hash) {
        final boolean ret = ((null != hash) &&
            ALLOWED_HASH_CHARS_PATTERN.matcher(hash).matches() &&
            ALLOWED_HASH_START_CHARS_PATTERN.matcher(hash).find() &&
            Cache.isValidHash(hash));

        if (!ret && (null != hash)) {
            ServerManager.logError("DC rejected service request due to invalid pattern of hash parameter: " + hash);
        }

        return ret;
    }

    /**
     * @param pageRange
     * @return
     */
    protected static boolean isValidPageRange(final String pageRange) {
        return ((null != pageRange) &&
            ALLOWED_PAGERANGE_CHARS_PATTERN.matcher(pageRange).matches());
    }

    // - implementation

    /**
     * @return The filled jobParams Map or null if any of the parameters is missing or wrong
     */
    private Map<String, String> initParams() {
        if ((null == m_params) && (null != m_request)) {
            final Map<String, Map<String, String[]>> paramDictionary = getParamDictionary();
            String action = getRequestParameter("action");
            action = (null != action) ? action.toLowerCase() : "";

            if ((action.length() > 0) && paramDictionary.containsKey(action)) {
                if (fillMap(paramDictionary.get(action), m_params = new HashMap<>())) {
                    m_params.put("action", action);
                } else {
                    m_params = null;
                }
            }
        }

        return m_params;
    }

    /**
     * @paramMap The map of valid parameter names and possible values
     * @param curParams The current map to fill with valid parameters.
     * @return true in case all given request parameters are valid, false otherwise.
     */
    private boolean fillMap(Map<String, String[]> paramMap, Map<String, String> curParams) {
        boolean success = true;
        final String keys[] = paramMap.keySet().toArray(new String[paramMap.size()]);

        for (final String curKey : keys) {
            final String value = implGetCheckedParameter(curKey, paramMap.get(curKey));

            if (null != value) {
                if (value.length() > 0) {
                    curParams.put(curKey, value);
                }
            } else {
                success = false;
                break;
            }
        }

        return success;
    }

    /**
     *
     */
    private void extractMultipartRequestData() throws InvalidParameterException {
        extractFormData(m_request, m_transferObject = new TransferObject<>());

        final String queryString = m_transferObject.getQuery();

        // extract request parameters from the 'query' form data field
        if (null != queryString) {
            final StringTokenizer queryTokenizer = new StringTokenizer(queryString, "&");
            String curQuery = null;

            while (queryTokenizer.hasMoreTokens()) {
                if (null != (curQuery = queryTokenizer.nextToken())) {
                    final int assignPos = curQuery.indexOf('=');

                    if ((assignPos > 0) && (assignPos < (curQuery.length() - 1))) {
                        if (null == m_requestParams) {
                            m_requestParams = new HashMap<>();
                        }

                        m_requestParams.put(curQuery.substring(0, assignPos), curQuery.substring(assignPos + 1));
                    }
                }
            }
        }
    }

    /**
     * @param servlet
     * @param request
     * @param transferObj
     */
    private void extractFormData(HttpServletRequest request, TransferObject<? extends Serializable> transferObj) throws InvalidParameterException {
        if ((null != request) && (null != transferObj) && ServletFileUpload.isMultipartContent(request)) {
            final ServletFileUpload fileUpload = new ServletFileUpload();
            File tmpFile = null;

            transferObj.clear();

            try {
                final FileItemIterator itemIter = fileUpload.getItemIterator(request);
                int curPartNumber = 0;

                while (itemIter.hasNext()) {
                    final FileItemStream itemStm = itemIter.next();
                    final String itemName = itemStm.getFieldName();

                    if (null != itemName) {
                        if (MAX_FORM_DATA_PART_COUNT == curPartNumber++) {
                            throw new InvalidParameterException("multipart/form-data request exceeds limit of " + MAX_FORM_DATA_PART_COUNT + " parts");
                        }

                        try (InputStream itemInputStm = itemStm.openStream()) {
                            if (itemStm.isFormField()) {
                                final String itemValue = Streams.asString(itemInputStm);

                                if (null != itemValue) {
                                    if (null == transferObj.getParamMap()) {
                                        transferObj.setParamMap(new HashMap<String, String>());
                                    }

                                    transferObj.getParamMap().put(itemName, itemValue);
                                }
                            } else {
                                Object serialObject = null;

                                if ("sourcefile".equals(itemName)) {
                                    // copy whole content of input stream into tmp. file and
                                    // set tmp. file as serialization  object at TransferObject
                                    FileUtils.copyInputStreamToFile(itemInputStm, tmpFile = implCreateTempFile());
                                    serialObject = tmpFile;
                                } else {
                                    final byte[] itemInputBuffer = IOUtils.toByteArray(itemInputStm);

                                    try (final InputStream bufferInputStm = new ByteArrayInputStream(itemInputBuffer);
                                         final DataSourceFileInputStream dataSourceFileInputStm = new DataSourceFileInputStream(bufferInputStm)) {
                                         final Serializable contentSerializable = (Serializable) dataSourceFileInputStm.readObject();

                                        // several types of data serialization
                                        if (contentSerializable instanceof byte[]) {
                                            // set temp. file, created from read byte array object as content
                                            FileUtils.writeByteArrayToFile(tmpFile = implCreateTempFile(), (byte[]) contentSerializable);
                                            serialObject = tmpFile;
                                        } else if (contentSerializable instanceof Long) {
                                            // set temp. file, created from input stream after just reading one Long object as content (compat. reading)
                                            FileUtils.copyInputStreamToFile(dataSourceFileInputStm, tmpFile = implCreateTempFile());
                                            serialObject = tmpFile;
                                        } else if (!(contentSerializable instanceof File)) {
                                            // set read object as content
                                            serialObject = contentSerializable;
                                        } else {
                                            throw new InvalidParameterException("File type object not allowed as source data in request!");
                                        }
                                    } catch (@SuppressWarnings("unused") StreamCorruptedException e) {
                                        // detection for invalid Java object serialization =>
                                        // compatibility fallback to use whole Item InputStream content
                                        try (final InputStream bufferInputStm = new ByteArrayInputStream(itemInputBuffer)) {
                                            FileUtils.copyInputStreamToFile(bufferInputStm, tmpFile = implCreateTempFile());
                                            serialObject = tmpFile;
                                        }
                                    }
                                }

                                transferObj.setSerialObject(serialObject);
                                transferObj.setFilename(itemStm.getName());
                                transferObj.setMimeType(itemStm.getContentType());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                FileUtils.deleteQuietly(tmpFile);
                throw new InvalidParameterException(Throwables.getRootCause(e).getMessage());
            }
        }
    }

    /**
     * @param paramName
     * @return
     */
    private String getRequestParameter(String paramName) {
        return (null != m_requestParams) ? m_requestParams.get(paramName) : m_request.getParameter(paramName);
    }

    /**
     * @param paramName The param name for which to retrieve a valid value
     * @param values The array of valid values for this parameter name. :value for a default value.
     * @return
     */
    private String implGetCheckedParameter(String paramName, String[] values) {
        String retValue = null;
        String value = getRequestParameter(paramName);

        // 1. Check all valid values against the parameter value.
        // 2. If no values are given, the parameter is returned as user value.
        // 3. If no parameter value exists, check for a possible default value.
        if (null != value) {
            if ((null != values) && (values.length > 0)) {
                value = value.toLowerCase();

                for (String curValue : values) {
                    // remove possible default tag/char at pos 0
                    if (curValue.charAt(0) == ':') {
                        curValue = curValue.substring(1);
                    }

                    if (curValue.equals(value)) {
                        retValue = value;
                        break;
                    }
                }
            } else {
                // every value is accepted as user value
                retValue = value;
            }
        } else {
            // check for a default value within the given values (denoted by ':...')
            String defaultValue = null;

            if ((null != values) && (values.length > 0)) {
                for (final String curVal : values) {
                    if (curVal.charAt(0) == ':') {
                        defaultValue = curVal.substring(1);
                        break;
                    }
                }
            }
            else {
                // a value listed as valid String param but not actually set
                // in request is treated as an empty string here
                defaultValue = "";
            }

            if (null != defaultValue) {
                retValue = defaultValue;
            }
        }

        return retValue;
    }

    /**
     * @return
     * @throws IOException
     */
    private File implCreateTempFile() {
        return m_manager.createTempFile("oxcs");
    }

    // - Members ---------------------------------------------------------------

    /**
     * The Documntconverter manager
     */
    protected ServerManager m_manager = null;

    /**
     *  The HttpServlet
     */
    protected DocumentConverterServlet m_documentConverterServlet = null;

    /**
     * The HTTP servlet request this JobExecutor is working on
     */
    protected HttpServletRequest m_request = null;

    /**
     * The HTTP servlet response this JobExecutor should fill
     */
    protected HttpServletResponse m_response = null;

    /**
     * The map of given request parameters
     */
    protected Map<String, String> m_requestParams = null;

    /**
     * The uploaded binary data object
     */
    protected TransferObject<Serializable> m_transferObject = null;

    /**
     * The map of valid job parameters or null if any of the needed paremeters is missing or has a wrong value
     */
    private Map<String, String> m_params = null;
}
